<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'phpDocumentor' => array($vendorDir . '/phpdocumentor/reflection-docblock/src'),
    'dflydev\\markdown' => array($vendorDir . '/dflydev/markdown/src'),
    'Zizaco\\FactoryMuff' => array($vendorDir . '/zizaco/factory-muff/src'),
    'Whoops' => array($vendorDir . '/filp/whoops/src'),
    'Way\\Generators' => array($vendorDir . '/way/generators/src'),
    'Way\\Database' => array($vendorDir . '/way/database/src'),
    'System' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Symfony\\Component\\Security\\Core\\' => array($vendorDir . '/symfony/security-core'),
    'Symfony\\Component\\Routing\\' => array($vendorDir . '/symfony/routing'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\HttpKernel\\' => array($vendorDir . '/symfony/http-kernel'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\Filesystem\\' => array($vendorDir . '/symfony/filesystem'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Symfony\\Component\\DomCrawler\\' => array($vendorDir . '/symfony/dom-crawler'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\CssSelector\\' => array($vendorDir . '/symfony/css-selector'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Symfony\\Component\\ClassLoader\\' => array($vendorDir . '/symfony/class-loader'),
    'Symfony\\Component\\BrowserKit\\' => array($vendorDir . '/symfony/browser-kit'),
    'Stack' => array($vendorDir . '/stack/builder/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log'),
    'Predis' => array($vendorDir . '/predis/predis/lib'),
    'Patchwork' => array($vendorDir . '/patchwork/utf8/class'),
    'PHPParser' => array($vendorDir . '/nikic/php-parser/lib'),
    'OpenBoleto\\' => array($vendorDir . '/kriansa/openboleto/src'),
    'Normalizer' => array($vendorDir . '/patchwork/utf8/class'),
    'Net' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'Menu' => array($vendorDir . '/vespakoen/menu/src'),
    'Math' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'LeroyMerlin\\LaraSniffer\\' => array($vendorDir . '/leroy-merlin-br/larasniffer/src'),
    'LaravelBook\\Ardent' => array($vendorDir . '/laravelbook/ardent/src'),
    'Krucas\\Notification' => array($vendorDir . '/edvinaskrucas/notification/src'),
    'Jeremeamia\\SuperClosure' => array($vendorDir . '/jeremeamia/SuperClosure/src'),
    'JasonNZ\\LaravelGrunt' => array($vendorDir . '/jason-morton-nz/laravel-grunt/src'),
    'Intervention\\Image\\' => array($vendorDir . '/intervention/image/src'),
    'Illuminate' => array($vendorDir . '/laravel/framework/src'),
    'HtmlObject' => array($vendorDir . '/anahkiasen/html-object/src'),
    'Hashids' => array($vendorDir . '/hashids/hashids/lib'),
    'File' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'Faker\\PHPUnit' => array($vendorDir . '/fzaninotto/faker/test'),
    'Faker' => array($vendorDir . '/fzaninotto/faker/src'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Doctrine\\Common\\Cache\\' => array($vendorDir . '/doctrine/cache/lib'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib'),
    'Doctrine\\Common\\' => array($vendorDir . '/doctrine/common/lib'),
    'Cviebrock\\EloquentSluggable' => array($vendorDir . '/cviebrock/eloquent-sluggable/src'),
    'Crypt' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'Clockwork' => array($vendorDir . '/itsgoingd/clockwork'),
    'ClassPreloader' => array($vendorDir . '/classpreloader/classpreloader/src'),
    'Carbon' => array($vendorDir . '/nesbot/carbon/src'),
    'Boris' => array($vendorDir . '/d11wtq/boris/lib'),
    'Bllim\\Datatables' => array($vendorDir . '/bllim/datatables/src'),
    'Barryvdh\\LaravelIdeHelper' => array($vendorDir . '/barryvdh/laravel-ide-helper/src'),
    'Barryvdh\\DomPDF' => array($vendorDir . '/barryvdh/laravel-dompdf/src'),
);
