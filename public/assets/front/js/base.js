$(function(){

	/* plugin de scroll */
	$('#scrollbar1').tinyscrollbar();
	$('#scrollbar2').tinyscrollbar();
	$('#scrollbar3').tinyscrollbar();
	$('#scrollbar4').tinyscrollbar();

	/* plugin da galeria 
	$(function(){
		$('#carousel').infiniteCarousel({
			imagePath: 'assets/front/imagens/'
		});
	});


	/* cpo de login */
	$('.btnLogin').click(function(e){
		e.preventDefault();

		if($(this).hasClass('aberto')){
			$('.boxLogin').fadeOut();
			$(this).removeClass('aberto');
		}
		else{
			$('.boxLogin').fadeIn();
			$(this).addClass('aberto');
		}
	});

	/* chama o scroll quando clica nos filtros */
	$('.cposBusca > a').click(function(e){
		e.preventDefault();

		//$('.cposBusca > a').removeClass('atv');
		$('.scrollbarPadrao, .scrollbarPadraoM').slideUp('fast');

		var index = $(this).closest('li').index()+1;

		if($(this).hasClass('atv')){
			$(this).closest('li').children('.scrollbarPadrao, .scrollbarPadraoM').slideUp('fast');
			$(this).removeClass('atv');
		}
		else{
			$('.cposBusca > a').removeClass('atv');
			$(this).addClass('atv');
			$(this).closest('li').children('.scrollbarPadrao, .scrollbarPadraoM').slideDown('fast',function(){
				$('#scrollbar'+index).tinyscrollbar();
				
			});
		}
		
	});


	/* add e remove a setinha do menu */
	$('.menu ul li').mouseenter(function(){
		if($(this).children('a').hasClass('atv')){
			return false
		}
		else{
			$(this).append('<span class="seta"></span>');
		}
		
	});

	$('.menu ul').delegate('li','mouseleave', function(){

		if($(this).children('a').hasClass('atv')){
			return false
		}
		else{
			$(this).children('span').remove();
		}
	});


	/* galeria banners */
	$(window).load(function() {
		var qtdBanners = 0;
		var lBanners = 0;
		
		function largura(){	
			qtdBanners = $('#banners ul li').length;
			lBanners = $('#banners ul li').width();

			$('#banners ul').css('width',qtdBanners*lBanners);
		};
		

		largura();

	});

	


	/* página de detalhe do empreendimento */
	var qtdImgsDetalhe = $('ul.lista li').length; // coloca a largura na ul.bullets
	$('ul.bullets').css('width',qtdImgsDetalhe*11+(qtdImgsDetalhe*10));// coloca a largura na ul.bullets
	var larguraImgs = $('ul.bullets').width();// coloca a largura na ul.bullets
	$('ul.bullets').css('margin-left',-larguraImgs/2);// coloca a largura na ul.bullets

	for(var i=0; i<qtdImgsDetalhe; i++){
		$('ul.bullets').append('<li><a href="#"></a></li>');
	}
	$('ul.bullets li').eq(0).children('a').addClass('atv');

	/* coloca largura na ul.lista*/
	$('ul.lista').css('width',qtdImgsDetalhe*458);

	$('.bullets li').delegate('a','click', function(e){
		e.preventDefault();

		var thisIndex = $(this).parent().index();

		$('ul.lista').animate({'margin-left':-thisIndex*458},500);

		$('.bullets li a').removeClass('atv');
		$(this).addClass('atv');
	});


	/* galeria txt Detalhes */
	var qtdTxtDetalhe = $('.detalhes > ul > li').length;
	$('.detalhes > ul').css('width',qtdTxtDetalhe*545+(qtdTxtDetalhe*65));

	$('ul.links li').delegate('a','click', function(e){
		e.preventDefault();

		var thisIndex = $(this).parent().index();

		$('.detalhes > ul').animate({'margin-left':-thisIndex*545 - (thisIndex*65)},500);

		$('ul.links li a').removeClass('atv');
		$(this).addClass('atv');

	});

});