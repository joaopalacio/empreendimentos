# jQuery.tinyscroller

Simple smooth scroller with events and API.

## demos

* [Basic](http://takazudo.github.com/jQuery.tinyscroller/demos/basic.html)
* [Live](http://takazudo.github.com/jQuery.tinyscroller/demos/live.html)
* [Live with two configs](http://takazudo.github.com/jQuery.tinyscroller/demos/live2.html)
* [Events](http://takazudo.github.com/jQuery.tinyscroller/demos/events.html)
* [Events with API call](http://takazudo.github.com/jQuery.tinyscroller/demos/events2.html)
* [API call](http://takazudo.github.com/jQuery.tinyscroller/demos/apicall.html)
* [API call - stop](http://takazudo.github.com/jQuery.tinyscroller/demos/stop.html)
* [API call - stop(chain)](http://takazudo.github.com/jQuery.tinyscroller/demos/stopchain.html)
* [Deferred](http://takazudo.github.com/jQuery.tinyscroller/demos/deferred.html)
* [Options](http://takazudo.github.com/jQuery.tinyscroller/demos/options.html)
* [Options with live](http://takazudo.github.com/jQuery.tinyscroller/demos/options2.html)
* [adjustEndY](http://takazudo.github.com/jQuery.tinyscroller/demos/adjustendy.html)

## Usage

```javascript
$.Tinyscroller().live();
```
For more info, see demos

## Depends

jQuery 1.7.2  

## Browsers

IE6+ and other new browsers.  

## License

Copyright (c) 2012 "Takazudo" Takeshi Takatsudo  
Licensed under the MIT license.

## Misc

This scirpt was developed with following things.  

 * [CoffeeScript][coffeescript]
 * [grunt][grunt]

[coffeescript]: http://coffeescript.org/ "CoffeeScript"
[grunt]: https://github.com/cowboy/grunt "grunt"
