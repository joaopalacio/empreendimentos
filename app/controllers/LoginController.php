<?php

class LoginController extends Controller {

	public function logar(){
		$email 	  = Input::get('email');
		$password = Input::get('senha');
		if (Auth::attempt(array('email' => $email, 'password' => $password)))
		{
		    return 1;
		}
	}
}