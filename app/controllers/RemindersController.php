<?php

class RemindersController extends Controller {


	public function request()
	{
	  $credentials = array('cpf' => Input::get('cpf'));
	 
	  return Password::remind($credentials, function($message, $user)
		{
				$message->subject('Esqueci minha senha.');
		});
	}

	public function reset($token)
	{
	  return View::make('password.reset')->with('token', $token);
	}

	public function update()
	{
	  	$credentials = array('cpf' => Input::get('cpf'), 'password' => Input::get('password'), 'password_confirmation' => Input::get('password_confirmation'), 'token' => Input::get('token'));

		$response = Password::reset($credentials, function($user, $password)
		{
		  $user->password = Hash::make($password);
		  $user->save();
		});

		switch ($response)
		{
		  case Password::INVALID_PASSWORD:
		  case Password::INVALID_TOKEN:
		  case Password::INVALID_USER:
		    return Redirect::back()->with('error', Lang::get($response));

		  case Password::PASSWORD_RESET:
		    return Redirect::to('/');
		}
	}
}