<?php

class HomeController extends BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.master';

    public function __construct(){
        //COMPOSER
        View::composer('includes.form_busca', function($view)
        {
            $view->with('tipos',['apartamento', 'loft', 'cobertura', 'studio', 'flat', 'casa padrão', 'sobrado', 'casa de condomínio', 'casa de vila']);
            $view->with('estagios',['pré-venda','venda','construindo','entregue']);            
            $view->with('cidades', Cidade::all());
        });
        //COMPOSER
        View::composer('includes.footer', function($view)
        {       
            $view->with('settings', Configuracao::first());
        });    

        View::composer('layouts.master',function($view){
            $url = (Request::segment(1))."/";
            $page = Page::whereUrl($url)->first();        
            if (isset($page))
                $view->with('page',$page);
        }); 
    }

    /**
    *	Show Index do site
    *	@return Response
    */
	public function index()
	{
        $model = Empreendimento::with('imoveis','estagios','uploads','bairro')->where('destaque','!=',"")->orderBy(DB::raw('RAND()'))->get();        
		$this->layout->content = View::make('pages.home')->with(['model'=>$model]);
	}

    /**
    *	Show Sobre do site
    *	@return Response
    */
	public function sobre()
	{
        $model = Sobre::all()->take(1);
		$this->layout->content = View::make('pages.sobre')->with('model',$model);
	}

    /**
    *	Show Contato do site
    *	@return Response
    */	public function contato()
	{
		$this->layout->content = View::make('pages.contato')->with('data',Configuracao::first());
	}

    /**
    *	Show empreendimentos do site
    *	@return Response
    */
	public function empreendimentos()
	{
        $model = Empreendimento::with('imoveis','estagios','uploads','bairro.cidades','bairro')->where('destaque','!=',"")->orderBy(DB::raw('RAND()'))->get();        
        $this->layout->content = View::make('pages.home')->with(['model'=>$model]);
	}


    public function empreendimentoDetalhe($cidade,$empreendimentos){
        $model = Empreendimento::with('imoveis','estagios','uploads','bairro','bairro.cidades')->where('url-clean','=',$empreendimentos)->first();         
        $this->layout->content = View::make('pages.detalhe')->with('data',$model);;
    }

    public function photos($arquivo,$width,$height){
        // create/resize image from file
        $image = Image::make($arquivo)->resize($width, $height);

        // create response and add formated image
        $response = Response::make($image->encode('png'));

        // set content-type
        $response->header('Content-Type', 'image/png');

        // et voila
        return $response;

    }

    //POST FORM
    public function PostSobre(){
        $model           = new Contato;
        $model->nome     = Input::get('nome');
        $model->email    = Input::get('email');
        $model->telefone = Input::get('telefone');
        $model->mensagem = Input::get('mensagem');
        $model->save();
        return 1;
    }

}