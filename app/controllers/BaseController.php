<?php



function linhaProcessada1($self, $numLn, $vlinha) {

    {
        if($vlinha["registro"] == $self::DETALHE && $vlinha["segmento"] == "T") {
            echo UtilApp::formatNumberTXT($vlinha['valor_titulo']);
            echo "nosso_numero = ".$vlinha['referencia_sacado']."<br/>";
            echo "valor_pagamento = ".UtilApp::formatNumberTXT($vlinha['valor_pagamento']);
            echo "acrescimos = ".UtilApp::formatNumberTXT($vlinha['acrescimos']); //multa e mora
            echo "data_pagamento = ".$vlinha['data_pagamento'];
            $boleto = Boleto::where('nosso_numero',"=",$vlinha['referencia_sacado'])->get();
            if(isset($boleto)){
            	$boleto->status = "pago";
            	//$boleto->save();
            }
        }
    }
    echo "<br/>\n";
  
}



class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
		$this->beforeFilter(function()
		{
		    Event::fire('clockwork.controller.start');
		});

		$this->afterFilter(function()
		{
		    Event::fire('clockwork.controller.end');
		});		
	}
}