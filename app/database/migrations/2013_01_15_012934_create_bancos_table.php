<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBancosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bancos',function($table){
			$table->increments('id');
			$table->string('banco')->nullable();
						
			$table->string('cnpj')->nullable();
			$table->string('cedente')->nullable();			
			$table->string('endereco')->nullable();
			$table->string('cep')->nullable();
			$table->string('cidade')->nullable();
			$table->string('uf')->nullable();

			$table->string('agencia')->nullable();
			$table->string('carteira')->nullable();
			$table->string('conta')->nullable();
			$table->string('contaDv')->nullable();
			$table->string('agenciaDv')->nullable();

			$table->string('identificacao')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bancos');
	}

}
