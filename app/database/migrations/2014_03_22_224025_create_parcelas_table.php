<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateParcelasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parcelas', function(Blueprint $table) {
			$table->increments('id');
            $table->integer('contrato_id')->unsigned()->nullable();
            $table->foreign('contrato_id')->references('id')->on('contratos')->onDelete('cascade'); 

            $table->integer('numero')->default(0);

            $table->date('vencimento')->nullable();
            $table->decimal('valor',20,2)->default(0.00);

            $table->enum('status',['aguardando','paga','cancelada'])->default('aguardando');            			

            $table->string('description')->nullable();
            $table->date('pagmento')->nullable();
            $table->decimal('multa',20,2)->nullable();
            $table->decimal('mora',20,2)->nullable();
            $table->decimal('desconto',20,2)->nullable();
            $table->decimal('pago',20,2)->nullable();

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('parcelas');
	}

}
