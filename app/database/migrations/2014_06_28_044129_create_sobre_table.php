<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSobreTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sobre', function(Blueprint $table) {
			$table->increments('id');
			$table->text('title1')->nullable();
			$table->text('descr1')->nullable();
			$table->text('title2')->nullable();
			$table->text('descr2')->nullable();
			$table->text('title3')->nullable();
			$table->text('descr3')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sobre');
	}

}
