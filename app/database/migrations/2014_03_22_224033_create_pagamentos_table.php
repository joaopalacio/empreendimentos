<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagamentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pagamentos', function(Blueprint $table) {
			$table->increments('id');
            //A Qual cidade este empreendimento pertence
            $table->integer('boleto_id')->unsigned()->index();
            $table->foreign('boleto_id')->references('id')->on('boletos')->onDelete('cascade');
            $table->decimal('valor');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pagamentos');
	}

}
