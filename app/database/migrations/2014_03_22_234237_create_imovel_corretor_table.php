<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImovelCorretorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imovel_corretor', function(Blueprint $table) {
			$table->increments('id');
            //Corretor deste Imovel
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); 			
            //O Imovel
            $table->integer('imovel_id')->unsigned()->index();
            $table->foreign('imovel_id')->references('id')->on('imoveis')->onDelete('cascade'); 			
            //Descontos
            $table->integer('desconto')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imovel_corretor');
	}

}
