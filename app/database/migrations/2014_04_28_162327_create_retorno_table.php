<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRetornoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('retorno', function(Blueprint $table) {
			$table->increments('id_fn');
			$table->integer('id_ps')->nullable();
			$table->date('vencimento_fn')->nullable();
			$table->date('pagamento_fn')->nullable();
			$table->decimal('valor_fn',10,2)->nullable();
			$table->decimal('valor_pago_fn',10,2)->nullable();
			$table->decimal('multa_fn',10,2)->nullable();
			$table->decimal('juros_fn',10,2)->nullable();
			$table->string('status_fn',10,2)->nullable();
			$table->date('data_fn')->nullable();
			$table->time('hora_fn')->nullable();
			$table->string('retorno_fn')->nullable();
			$table->integer('competencia_fn')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('retorno');
	}

}
