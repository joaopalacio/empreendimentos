<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropostasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('propostas', function(Blueprint $table) {
			$table->increments('id');

            //Dados proposta 		
            $table->decimal('desconto',20,2)->default(0.00);
            $table->decimal('entrada',20,2)->default(0.00);
            $table->decimal('chaves',20,2)->default(0.00);
            $table->decimal('fgts',20,2)->default(0.00);
            $table->decimal('porcent',20,2)->default(0.00);
            $table->integer('prazo')->default(0);//Prazo maximo de financiamento em meses.
            //A Qual corretor pertence esses dados
            $table->integer('reserva_id')->unsigned()->index();
            $table->foreign('reserva_id')->references('id')->on('reserva')->onDelete('cascade');	

            $table->decimal('valor_final',20,2)->nullable();
            
            $table->enum('status',['Aguardando','Recusado','Aprovado'])->default('Aguardando');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('propostas');
	}

}
