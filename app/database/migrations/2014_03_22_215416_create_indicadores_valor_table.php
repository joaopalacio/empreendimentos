<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIndicadoresValorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('indicadores_valor', function(Blueprint $table) {
			$table->increments('id');
			$table->string('base',8);
			$table->decimal('amount',20,2)->default(0.00);
			//A qual indicador pertence esses valores no mes base (MM/YYYY)
            $table->integer('indicador_id')->unsigned()->index();
            $table->foreign('indicador_id')->references('id')->on('indicadores')->onDelete('cascade');			
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('indicadores_valor');
	}

}
