<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBoletosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('boletos', function(Blueprint $table) {
			$table->increments('id');
			$table->string('boleto');
            //A Qual cidade este empreendimento pertence
            $table->integer('parcela_id')->unsigned()->index();
            $table->foreign('parcela_id')->references('id')->on('parcelas')->onDelete('cascade');
            $table->string('nosso_numero');
			$table->enum('banco',['itau','bradesco','caixa','santander']);
			$table->decimal('valor')->default('0.00');
			$table->decimal('pago')->default('0.00')->nullable();
			$table->date('vencimento');
			$table->enum('status',['pendente','atrasado','cancelado','pago']);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('boletos');
	}

}
