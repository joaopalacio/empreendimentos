<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRegrasIntermediariasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('regras_intermediarias', function(Blueprint $table) {
			$table->increments('id');
			$table->string('mes');
			$table->decimal('valor',20,2)->default(0.00);
			$table->timestamps();
            //A Qual imovel pertence esses dados
            $table->integer('regras_id')->unsigned()->index();
            $table->foreign('regras_id')->references('id')->on('regras_propostas')->onDelete('cascade'); 			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('regras_intermediarias');
	}

}
