<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIntermediariasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('intermediarias', function(Blueprint $table) {
			$table->increments('id');
            //A Qual Proposta pertence esses dados
            $table->integer('proposta_id')->unsigned()->nullable();
            $table->foreign('proposta_id')->references('id')->on('propostas')->onDelete('cascade'); 
            $table->string('description')->nullable();
            $table->integer('numero')->default(0);
            $table->date('vencimento');
            $table->decimal('valor',20,2)->default(0.00);

            $table->enum('status',['aguardando','paga','cancelada'])->default('aguardando');     			
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('intermediarias');
	}

}
