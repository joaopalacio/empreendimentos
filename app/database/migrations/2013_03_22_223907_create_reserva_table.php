<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reserva', function(Blueprint $table) {
			$table->increments('id');
			
			$table->enum('status',['Aguardando Cliente','Aguardando Aprovação','Recusado','Aprovado'])->default('Aguardando Cliente');
			$table->text('observacao')->nullable();	

            //A Qual imovel pertence esses dados
            $table->integer('imovel_id')->unsigned()->index();
            $table->foreign('imovel_id')->references('id')->on('imoveis')->onDelete('cascade'); 
            //A Qual corretor pertence esses dados
            $table->integer('corretor_id')->unsigned()->index();
            $table->foreign('corretor_id')->references('id')->on('users')->onDelete('cascade');     
            //A Qual Cliente pertence esses dados
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users'); 
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reserva');
	}

}
