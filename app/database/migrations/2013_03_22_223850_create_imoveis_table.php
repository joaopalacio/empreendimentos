<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImoveisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imoveis', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nome')->nullable();
            //A Qual empreendimento pertence
            $table->integer('empreendimento_id')->unsigned()->index();
            $table->foreign('empreendimento_id')->references('id')->on('empreendimentos')->onDelete('cascade');		
			$table->enum('status',['Disponível','Reserva','Vendido'])->default('Disponível');            
            //Informacoes do Imovel
			$table->enum('tipo',array('apartamento', 'loft', 'cobertura', 'studio', 'flat', 'casa padrão', 'sobrado', 'casa de condomínio', 'casa de vila'));
			$table->text('descricao');
			$table->integer('quartos');
			$table->integer('suites');
			$table->integer('vagas');
			$table->decimal('area_privativa',10,2)->default('0.00');
			$table->decimal('area_total',10,2)->default('0.00');
			$table->decimal('area_comum',10,2)->default('0.00');
			$table->string('chave_unica')->nullable();		
			$table->string('grupo')->nullable();										
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imoveis');
	}

}
