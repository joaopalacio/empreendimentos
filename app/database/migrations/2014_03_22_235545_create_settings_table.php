<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings', function(Blueprint $table) {
			$table->increments('id');
			$table->string('endereco')->nullable();
			$table->string('cep')->nullable();
			$table->string('cidade')->nullable();
			$table->string('estado')->nullable();
			$table->string('telefone')->nullable();
			$table->string('email')->nullable();	
			$table->string('facebook')->nullable();
			$table->string('youtube')->nullable();
			$table->string('googleplus')->nullable();
			$table->string('twitter')->nullable();
			$table->string('linkedin')->nullable();					
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');
	}

}
