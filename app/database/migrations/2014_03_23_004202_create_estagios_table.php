<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstagiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('estagios', function(Blueprint $table) {
			$table->increments('id');
			$table->enum('estagio',['aprovações', 'preparações iniciais', 'fundações', 'estrutura', 'alvenaria', 'acabamento']);
			$table->decimal('porcent',20,2)->default(0.00);
            //A Qual empreendimento pertence esses dados
            $table->integer('empreendimento_id')->unsigned()->index();
            $table->foreign('empreendimento_id')->references('id')->on('empreendimentos')->onDelete('cascade');   			
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estagios');
	}

}
