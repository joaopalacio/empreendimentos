<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRegrasPropostasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('regras_propostas', function(Blueprint $table) {
			$table->increments('id');
            //A Qual imovel pertence esses dados
            $table->integer('imovel_id')->unsigned()->index();
            $table->foreign('imovel_id')->references('id')->on('imoveis')->onDelete('cascade'); 
            $table->decimal('desconto_padrao',20,2)->default(0.00);
            $table->decimal('valor_imovel',20,2)->default(0.00);
            $table->decimal('entrada_minima',20,2)->default(0.00);
            $table->decimal('chaves_maxima',20,2)->default(0.00);
            $table->decimal('porcent_financiamento',20,2)->default(0.00);
            $table->integer('prazo')->default(0);//Prazo maximo de financiamento em meses.
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('regras_intermediarias');
		Schema::drop('propostas');
	}

}
