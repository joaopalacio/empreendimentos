<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->string('email')->index()->unique();				
			$table->string('password');		
			$table->string('name')->nullable();
			$table->string('cpf')->nullable();			
            //Nivel de Acesso do Usuario (cliente, corretor, Admin)
            $table->integer('role_id')->unsigned()->index();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');    	
			$table->integer('corretor_id')->unsigned()->nullable();
			$table->foreign('corretor_id')->references('id')->on('users')->onDelete('cascade'); 	
			$table->enum('status_acesso',['bloqueado','liberado','negado'])->default('liberado'); 	            		
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
