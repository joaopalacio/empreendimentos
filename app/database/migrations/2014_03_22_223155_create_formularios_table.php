<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFormulariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('formularios', function(Blueprint $table) {
		$table->increments('id');
            $table->enum('conjuge',['Sim','Não'])->default('Não');                        
            //A Qual proposta pertence esses dados
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');            
            //Dados do Usuario
            $table->string('nome');
            $table->enum('sexo',array('Masculino','Feminino'))->nullable();
            $table->enum('tipo',array('Proposta','Conjuge'))->default('Proposta');
            $table->string('pai')->nullable();
            $table->string('mae')->nullable();
            $table->date('nascimento')->nullable();
            $table->string('naturalidade')->nullable();
            $table->string('civil')->nullable();
            $table->date('casamento')->nullable();
            $table->enum('separacao',array('','com_parcial','separacao','universal'))->nullable();
            $table->string('rg')->nullable();
            $table->string('org_exp')->nullable();
            $table->string('data_exp')->nullable();
            $table->string('cpf');
            $table->string('profissao')->nullable();
            $table->string('carteira_trabalho')->nullable();
            $table->string('pis')->nullable();            
            $table->decimal('renda')->default(0.00)->nullable();
            $table->string('cartorio')->nullable();            
            //Residencial
            $table->string('endereco')->nullable();
            $table->string('bairro')->nullable();
            $table->string('cep')->nullable();
            $table->string('municipio')->nullable();
            $table->string('uf')->nullable();
            $table->string('telefone')->nullable();
            $table->string('celular')->nullable();
            //comercial
            $table->string('endereco_com')->nullable();
            $table->string('bairro_com')->nullable();
            $table->string('cep_com')->nullable();
            $table->string('municipio_com')->nullable();
            $table->string('uf_com')->nullable();
            $table->string('telefone_com')->nullable();
            $table->string('empresa')->nullable();
            
		$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('formularios');
	}

}
