<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmpreendimentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('empreendimentos', function(Blueprint $table) {
			$table->increments('id');
            //A Qual cidade este empreendimento pertence
            $table->integer('cidade_id')->unsigned()->index();
            $table->foreign('cidade_id')->references('id')->on('cidades')->onDelete('cascade');		
            $table->integer('bairro_id')->unsigned()->index();
            $table->foreign('bairro_id')->references('id')->on('bairros')->onDelete('cascade');		
            $table->integer('banco_id')->unsigned()->nullable();
            $table->foreign('banco_id')->references('id')->on('bancos')->onDelete('cascade');		            		
			$table->string('nome');
			$table->enum('destaque',array('destaque','banner','normal'))->nullable();	
			$table->text("description")->nullable();
			$table->string('url-clean')->unique()->nullable();
			$table->string('endereco')->nullable();
			$table->string('gmaps')->nullable();
			$table->string('span')->nullable();					
			$table->enum('status_venda',['Pré-venda','Lançamento','Em construção','Entregue','Ultimas unidades'])->nullable();               
			$table->string('caption')->nullable();
			$table->enum('tipo_destaque',[0,'imovelP','imovelG','imovelDestaque'])->default('imovelP')->nullable();
			$table->integer('destaque_home')->default(0);		
			$table->date('previsao_entrega')->nullable();	         	
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empreendimentos');
	}

}
