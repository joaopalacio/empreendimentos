<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('RoleTableSeeder');
		$this->call('CidadesTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('IndicadorTableSeeder');
		$this->call('ConfiguracaoTableSeeder');
		$this->call('PageTableSeeder');
		$this->call('EmpreendimentoTableSeeder');
		$this->call('ImovelTableSeeder');
	}

}
