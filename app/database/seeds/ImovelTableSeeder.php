<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ImovelTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create('pt_br');	
		$items = ['apartamento', 'loft', 'cobertura', 'studio', 'flat', 'casa padrão', 'sobrado', 'casa de condomínio', 'casa de vila'];
		foreach(range(1, 30) as $index)
		{
			$hashids = new Hashids\Hashids(Config::get('app.key'));
			$hash = $hashids->encrypt($index+Config::get('params.plusKey'));

			$Empreendimento = Empreendimento::orderBy(DB::raw('RAND()'))->first();			
			$array = [
				'empreendimento_id' =>$Empreendimento->id,
				'tipo'              =>$items[array_rand($items)],
				'descricao'         =>$faker->text,
				'quartos'           =>rand(0,10),
				'suites'            =>rand(0,10),
				'vagas'             =>rand(0,10),
				'area_privativa'    =>rand(0,200),
				'area_total'        =>rand(0,200),
				'area_comum'        =>rand(0,200),
				//'localizacao'       =>$faker->randomNumber,
				'chave_unica'       =>$hash
			];
			$id = DB::table('imoveis')->insertGetId($array);

			$Role = Role::where('name','=','Corretor')->first();

			foreach (User::where('role_id','=',$Role->id)->get() as $key => $value) {
				$model = Imovel::find($id);
				$model->corretores()->attach($value);
			}        	
			$regra_proposta                        = new RegrasProposta;
			$regra_proposta->imovel_id             = $id;
			$regra_proposta->desconto_padrao       = rand(0,100);
			$regra_proposta->valor_imovel          = 500000.00;
			$regra_proposta->entrada_minima        = rand(1,(int)($regra_proposta->valor_imovel /100*20));
			$regra_proposta->chaves_maxima         = rand(1,(int)($regra_proposta->valor_imovel /100*40));
			$regra_proposta->porcent_financiamento = rand(20,30);
			$regra_proposta->prazo                 = rand(10,360);
			$regra_proposta->save();

			$intermediaria = 
			[
				['mes'=>'1','valor'=>'1000.00'],
				['mes'=>'2','valor'=>'1000.00'],
				['mes'=>'3','valor'=>'1000.00'],
				['mes'=>'5','valor'=>'8000.00'],
				['mes'=>'10','valor'=>'3000.00']
			];
			foreach ($intermediaria as $key => $value) {
				$intermediaria = new RegraIntermediaria;
				$intermediaria->mes = $value['mes'];
				$intermediaria->valor = $value['valor'];
				$intermediaria->regras_id = $regra_proposta->id;
				$intermediaria->save();
			}			
		}
	}

}