<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class IndicadorTableSeeder extends Seeder {

	public function run()
	{

		Indicador::create(['name'=>'INPC']);
		Indicador::create(['name'=>'IGPM']);
		Indicador::create(['name'=>'Taxa de Multa']);
	
	}

}