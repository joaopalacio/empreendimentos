<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CidadesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create('pt_BR');

		foreach(range(1, 10) as $index)
		{
			$model = Cidade::create([
						'cidade'=>$faker->city
			]);	
			$id	= $model->id;	
			foreach(range(1, 4) as $sub_index)
			{	
				if(isset($id))
				Bairro::create([
					'cidade_id'=>$id,
					'bairros'=>$faker->city
				]);				
			}
		}
	}

}