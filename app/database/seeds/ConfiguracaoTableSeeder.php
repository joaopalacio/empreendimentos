<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ConfiguracaoTableSeeder extends Seeder {

	public function run()
	{

		$model = Configuracao::first();
		if (isset($model)) $model->delete();
		$faker = Faker::create('pt_BR');
		Configuracao::create([
			'endereco'=>$faker->streetAddress,
			'cep'=>$faker->postcode,
			'cidade'=>$faker->city,
			'estado'=>$faker->stateAbbr,
			'telefone'=>$faker->phoneNumber,
			'email'=>$faker->email,
		]);
	}

}