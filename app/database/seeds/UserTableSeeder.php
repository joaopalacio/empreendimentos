<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create('pt_BR');
		
		User::create([
			'role_id'=>Role::where('name','=','Administrador')->first()->id,
			'email'=> 'admin@admin.com.br', 
			'name'=>$faker->name,
			'cpf'=>'111.111.111-11',
			'password'=>Hash::make('123mudar')
		]);
		foreach(range(1, 2) as $index)
		{

			User::create([
				'role_id'=>Role::where('name','=','Corretor')->first()->id,
				'email'=> $faker->email, 
				'name'=>$faker->name,
				'cpf'=>'111.111.111-1'.$index,
				'password'=>Hash::make('123mudar')
			]);			
		}
		/*$Role = Role::where('name','=','Corretor')->first();
		$corretor = User::where('role_id','=',$Role->id)->first();		
		foreach(range(1, 10) as $index)
		{
			User::create([
				'role_id'=>Role::where('name','=','Cliente')->first()->id,
				'email'=> $faker->email, 
				'name'=>$faker->name,
				'cpf'=>'222.111.111-1'.$index,
				'corretor_id'=>$corretor->id,
				'password'=>Hash::make('123mudar')
			]);
		}*/
	}

}
