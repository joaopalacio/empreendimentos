<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class RoleTableSeeder extends Seeder {

	public function run()
	{
		DB::table('imovel_corretor')->delete();	
		DB::table('regras_intermediarias')->delete();
		DB::table('regras_propostas')->delete();		
		DB::table('propostas')->delete();
		DB::table('imoveis')->delete();
		DB::table('estagios')->delete();
		DB::table('empreendimentos')->delete();		
		DB::table('indicadores')->delete();	
		DB::table('bairros')->delete();
		DB::table('cidades')->delete();
		DB::table('users')->delete();


		Role::create(['name'=>'Administrador']);
		Role::create(['name'=>'Corretor']);
		Role::create(['name'=>'Cliente']);
	}

}