<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class EmpreendimentoTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create('pt_br');	
		foreach(range(1, 3) as $index)
		{
			$bairro = Bairro::orderBy(DB::raw('RAND()'))->first();			
			$array = [
				'nome'=>$faker->catchPhrase,
				'destaque'=>1,
				'cidade_id'=>$bairro->cidade_id,
				'bairro_id'=>$bairro->id,
				'endereco'=>$faker->address,
				'gmaps'=>$faker->latitude.", ".$faker->longitude
			];
			$id = DB::table('empreendimentos')->insertGetId($array);
        	foreach (['aprovações', 'preparações iniciais', 'fundações', 'estrutura', 'alvenaria', 'acabamento'] as $key => $value) {
        		$estagio = new Estagio;
        		$estagio->empreendimento_id = $id;
        		$estagio->porcent = 0.0;
        		$estagio->estagio = $value;
        		$estagio->save();
        	}			
		}
	}

}