<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PageTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		Page::truncate();
		Page::create([
			'title'=>'Home',
			'url'=>'/',
		]);
		Page::create([
			'title'=>'Contato',
			'url'=>'contato/',
		]);
		Page::create([
			'title'=>'Sobre',
			'url'=>'sobre/',
		]);

	}

}