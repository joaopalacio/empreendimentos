<?php
use LaravelBook\Ardent\Ardent;
/**
 * Pagamento
 *
 * @property integer $id
 * @property integer $boleto_id
 * @property float $valor
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Boleto $boleto
 * @method static \Illuminate\Database\Query\Builder|\Pagamento whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Pagamento whereBoletoId($value)
 * @method static \Illuminate\Database\Query\Builder|\Pagamento whereValor($value)
 * @method static \Illuminate\Database\Query\Builder|\Pagamento whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Pagamento whereUpdatedAt($value)
 */
class Pagamento extends Ardent implements ModelInterface{
	protected $fillable = ['valor'];
	protected $table = "pagamentos";

	public static $relationsData  = 
	[				
		'boleto' => array(self::BELONGS_TO, 'Boleto'),		
  	];	
}