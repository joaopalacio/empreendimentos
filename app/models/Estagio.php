<?php
use LaravelBook\Ardent\Ardent;
/**
 * Estagio
 *
 * @property integer $id
 * @property string $estagio
 * @property float $porcent
 * @property integer $empreendimento_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Ardent\Collection|\Empreendimento[] $empreendimento
 * @method static \Illuminate\Database\Query\Builder|\Estagio whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Estagio whereEstagio($value)
 * @method static \Illuminate\Database\Query\Builder|\Estagio wherePorcent($value)
 * @method static \Illuminate\Database\Query\Builder|\Estagio whereEmpreendimentoId($value)
 * @method static \Illuminate\Database\Query\Builder|\Estagio whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Estagio whereUpdatedAt($value)
 */
class Estagio extends Ardent implements ModelInterface{

	protected $fillable = ['estagio','porcent'];
	protected $table = "estagios";


	public $autoHydrateEntityFromInput = true;
   	public $autoPurgeRedundantAttributes = true;

	public static $rules 		  = 
	[
		//'bairros'  					  => 'required|between:3,50',
	];
		
	public static $customMessages = 
	[
		'required'                    => '- Este campo é :attribute obrigatorio.',
		'between'                     => '- Preencha o campo Bairro com mais de 3 caracteres',    	
	];     



	public static $relationsData  = 
	[
		'empreendimentos'   => array(self::BELONGS_TO, 'Empreendimento'),
  	];	


  	public  $settings      = 
  	[
  			'model'=>'Empreendimento',
  			'title'=>'Lista de estagios do Andamento do Empreendimento',
			'formTitle'   =>'Andamento do Empreendimento',
			'index.url'   =>'estagios.index' ,
  	]; 

  	public  $btnsColumns   = 
  	[
		[
			'link'=>'imoveis.index',
			'text'=>'<i class="fa  fa-home"></i>',
			'array'=>
			[
				'class'=>'btn btn-warning btn-xs tooltips',
				'data-original-title'=>'Tooltip on top',
				'data-placement'=>"top",
				'data-toggle'=>"tooltip"
			]
		],

	];



}