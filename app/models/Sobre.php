<?php
use LaravelBook\Ardent\Ardent;
class Sobre  extends Ardent implements ModelInterface{
	protected $table = "sobre";
	protected $fillable = ['title1','title2','title3','descr1','descr2','descr3'];
}