<?php

use LaravelBook\Ardent\Ardent;

class Banner extends Ardent implements ModelInterface
{
    protected $fillable = ['empreendimento_id','image'];
    protected $table = "banners";

    public $autoHydrateEntityFromInput   = true;
    public $autoPurgeRedundantAttributes = true;
    public static $rules                 =
    [
        'image'             => 'required'
    ];
        
    public static $customMessages =
    [
        'required'                    => '- Este campo é obrigatorio.',
        'between'                     => '- Preencha o campo :attributes com mais de 4 caracteres', 
    ];  

   
    public static $relationsData  =
    [
        'empreendimento'   => array(self::BELONGS_TO, 'Empreendimento'),
    ];

    public $tableColumns  = ['id','Image','Empreendimento'];

      public $editColumns   =
      [
        ['id'=>'nome','name'=>'nome','campo'=>'Nome do Empreendimento','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'EX: Decor Empreendimento'],
        ['id'=>'cidade_id','lista'=>[],'value'=>'','name'=>'cidade_id','campo'=>'Cidade','minlength'=>'','type'=>'select','required'=>'','class'=>'form-control m-bot15 change_bairros','placeholder'=>'EX: Rua Correia Dias, 120 - 04104000, Sao paulo / SP'],
        ['id'=>'bairro_id','lista'=>[],'value'=>'','name'=>'bairro_id','campo'=>'Bairro','minlength'=>'','type'=>'select','required'=>'','class'=>'form-control m-bot15','placeholder'=>'EX: Rua Correia Dias, 120 - 04104000, Sao paulo / SP'],                
        ['id'=>'endereco','name'=>'endereco','campo'=>'Endereco Completo','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'EX: Rua Correia Dias, 120 - 04104000'],        
        ['id'=>'gmaps','name'=>'gmaps','campo'=>'Latitude e Longitude','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'EX: -45.60940,32.09289'],    
        ['id'=>'description','name'=>'description','campo'=>'Descrição','minlength'=>'','type'=>'textarea','required'=>'','class'=>'','placeholder'=>'EX: Descrição do Empreendimento a ser anunciado'],                
        ['id'=>'tipo_destaque','lista'=>['0'=>'Escolha um tipo de chamada para a home do site','imovelP'=>'Chamada Pequena','imovelG'=>'Chamada Grande','imovelDestaque'=>'Chamada Destaque'],'value'=>'','name'=>'tipo_destaque','campo'=>'Tipo de Chamada','minlength'=>'','type'=>'select','required'=>'','class'=>'form-control m-bot15',],                        
        ['id'=>'caption','name'=>'caption','campo'=>'Subchamada para Home','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'EX: Pequena descrição de 100 caracteres para home '],        
        ['id'=>'previsao_entrega','name'=>'previsao_entrega','campo'=>'Previsão de Enterega','minlength'=>'','type'=>'text','required'=>'','class'=>'datepickermes data','placeholder'=>'EX:mm/YYYY'],        
      ];


      public $settings =
      [
              'model'=>'Empreendimento',
              'title'=>'Lista de Empreendimentos cadastradas',
              'thead'=>[
                    ['class'=>'numeric','name'=>'id'],
                    ['class'=>'','name'=>'Empreendimento'],
                    ['class'=>'','name'=>'Endereço'],    
                    ['class'=>'','name'=>'Status'],                            
                    ['class'=>'','name'=>'Destaque'],                            
                    ['class'=>'numeric','name'=>'']
               ],
              'modal'=>[
                'title'=>'Deseja Excluir o registro',
                'description'=>'<b>Tem certeza que deseja Excluir o registro?</b> </br>Caso este registro esteja atrelado a outros como empreendimentos,imoveis os mesmos tambem serao deletados.',
                'btns'=>[]
            ],
            'formTitle'   =>'Formulario de Cidades',
            'photos.url'  =>'banners.photos',
            'upload.url'  =>'banners.upload',            
            'crud.url'    =>'banners.crud',
            'destroy.url' =>'banners.destroy',
      ]; 

      public $btnsColumns   =
      [
        [
            'link'=>'empreendimentos.photos',
            'text'=>'<i class="fa  fa-camera"></i>',
            'array'=>
            [
                'class'=>'btn btn-default btn-xs tooltips',
                'data-original-title'=>'Adicionar imagens',
                'data-placement'=>"top",
                'data-toggle'=>"tooltip"
            ]
        ],            
        [
            'link'=>'imoveis.index',
            'text'=>'<i class="fa  fa-map-marker"></i>',
            'array'=>
            [
                'class'=>'btn btn-info btn-xs tooltips',
                'data-original-title'=>'Imoveis deste Empreendimento',
                'data-placement'=>"top",
                'data-toggle'=>"tooltip"
            ]
        ],
        [
            'link'=>'estagio.index',
            'text'=>'<i class="fa fa-bar-chart-o"></i>',
            'array'=>
            [
                'class'=>'btn btn-warning btn-xs tooltips',
                'data-original-title'=>'Estagio da Obra',
                'data-placement'=>"top",
                'data-toggle'=>"tooltip"
            ]
        ],        
        [
            'link'=>'empreendimentos.edit',
            'text'=>'<i class="fa fa-pencil"></i>',
            'array'=>
            [
                'class'=>'btn btn-primary btn-xs tooltips',
                'data-original-title'=>'Editar Registro',
                'data-placement'=>"top",
                'data-toggle'=>"tooltip"
            ]
        ],
        [
            'link'=>'#myModal',
            'modal'=>'true',
            'text'=>'<i class="fa fa-trash-o"></i>',
            'data-url'=>'empreendimentos.destroy',
            'array'=>
            [
                'class'=>'btn btn-danger btn-xs tooltips delModal',
                'data-original-title'=>'Excluir registro',
                'data-placement'=>"top",
                'data-toggle'=>"modal"
            ]
        ],

    ];

    public function getSelect()
    {
        return Cidade::all()->toArray();
    }
}
