<?php
use LaravelBook\Ardent\Ardent;
/**
 * IndicadorValor
 *
 * @property integer $id
 * @property string $base
 * @property float $amount
 * @property integer $indicador_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Indicador $indicador
 * @method static \Illuminate\Database\Query\Builder|\IndicadorValor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\IndicadorValor whereBase($value)
 * @method static \Illuminate\Database\Query\Builder|\IndicadorValor whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\IndicadorValor whereIndicadorId($value)
 * @method static \Illuminate\Database\Query\Builder|\IndicadorValor whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\IndicadorValor whereUpdatedAt($value)
 */
class IndicadorValor extends Ardent implements ModelInterface{
	protected $fillable = ['indicador_id','base','amount'];
	protected $table = "indicadores_valor";

 	public $autoHydrateEntityFromInput = true;
   	public $autoPurgeRedundantAttributes = true;

	public static $rules 		  = 
	[
		'base'  					  => array('regex:/^(0[1-9]|1[0-2])\/(19|2[0-1])\d{2}$/'),
		'amount'					=> 'required'
	];
		
	public static $customMessages = 
	[
		'regex' => '- Preencha o mes base no formato MM/YYYY.',  	
		'required'=>'- Preencha todos os campoas'
	];     
		
		
	public static $relationsData  = 
	[
		'indicador'                     => array(self::BELONGS_TO, 'Indicador'),
  	];

  	public  $tableColumns  = ['id','base','amount'];

  	public  $editColumns   = 
  	[
		['id' =>'Indicador','name'=>'base','campo'=>'Mes Base','minlength'=>'','type'=>'text','required'=>'true','class'=>'mask_monthYear','placeholder'=>'00/0000'],
		['id' =>'Indicador','name'=>'amount','campo'=>'Valor Indicador','minlength'=>'','type'=>'text','required'=>'true','class'=>'mask_percent','placeholder'=>'0.00']
  	];


  	public  $settings      = 
  	[
  			'model'=>'Valores',
  			'title'=>'Lista de Valores cadastradas',
			'thead'=>[
				['class'=>'numeric','name'=>'id'],
				['class'=>'','name'=>'Mes Base'],
				['class'=>'','name'=>'Valor'],				
				['class'=>'numeric','name'=>'']
			],
  			'modal'=>[
				'title'=>'Deseja Excluir o registro',
				'description'=>'<b>Tem certeza que deseja Excluir o registro?</b> </br>Caso este registro esteja atrelado a outros como empreendimentos,imoveis os mesmos tambem serao deletados.',
				'btns'=>[]
			],
			'formTitle'   =>'Formulario de Indicadores',
			'index.url'   =>'valores.index' ,
			'insert.url'  =>'valores.create' ,
			'store.url'   =>'valores.store',
			'edit.url'    =>'valores.edit',
			'update.url'  =>'valores.update',
			'crud.url'    =>'valores.crud',
			'destroy.url' =>'valores.destroy',
  	]; 

  	public  $btnsColumns   = 
  	[

		[
			'link'=>'#myModal',
			'modal'=>'true',
			'text'=>'<i class="fa fa-trash-o"></i>',
			'data-url'=>'valores.destroy',
			'array'=>
			[
				'class'=>'btn btn-danger btn-xs tooltips delModal',
				'data-original-title'=>'Excluir',
				'data-placement'=>"top",
				'data-toggle'=>"modal"
			]
		],

	];


}