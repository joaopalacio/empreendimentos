<?php
use LaravelBook\Ardent\Ardent;
/**
 * Faq
 *
 * @property integer $id
 * @property string $titulo
 * @property string $resposta
 * @property string $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Faq whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Faq whereTitulo($value)
 * @method static \Illuminate\Database\Query\Builder|\Faq whereResposta($value)
 * @method static \Illuminate\Database\Query\Builder|\Faq whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Faq whereUpdatedAt($value)
 * @property string $url-clean
 * @method static \Illuminate\Database\Query\Builder|\Faq whereUrlClean($value)
 */
class Faq extends Ardent implements ModelInterface{
	protected $fillable = ['titulo','resposta','type'];
	protected $table = "faq";


 	public $autoHydrateEntityFromInput = true;
   	public $autoPurgeRedundantAttributes = true;

	public static $rules 		  = 
	[
		'titulo'  					  => 'required|between:4,20',
		'resposta'					  =>'required|between:1,300'
	];
		
	public static $customMessages = 
	[
		'required'                    => '- Este campo é obrigatorio.',
		'between'                     => '- Preencha o campo :attributes com mais de 4 caracteres',    	
	];     
		
	public static $sluggable      = 
	[
		'build_from'                  => 'titulo',
		'save_to'                     => 'url-clean',
	];	


  	public  $tableColumns  = ['id','titulo','resposta'];

  	public  $editColumns   = 
  	[
		['id'=>'titulo','name'=>'titulo','campo'=>'Titulo','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>''],
		['id'=>'resposta','name'=>'resposta','campo'=>'Descrição','minlength'=>'','type'=>'textarea','required'=>'','class'=>'','placeholder'=>''],
  	];


  	public  $settings      = 
  	[
  			'model'=>'FAQ',
  			'title'=>'Lista de FAQ cadastradas',
			'thead'=>[
				['class'=>'numeric','name'=>'id'],
				['class'=>'','name'=>'Titulo'],
				['class'=>'','name'=>'Descricao'],
				['class'=>'numeric','name'=>''],
			],
  			'modal'=>[
				'title'=>'Deseja Excluir o registro',
				'description'=>'<b>Tem certeza que deseja Excluir o registro?</b> </br>Caso este registro esteja atrelado a outros como empreendimentos,imoveis os mesmos tambem serao deletados.',
				'btns'=>[]
			],
			'formTitle'   =>'Formulario de FAQ',
			'index.url'   =>'faq.index' ,
			'insert.url'  =>'faq.create' ,
			'store.url'   =>'faq.store',
			'edit.url'    =>'faq.edit',
			'update.url'  =>'faq.update',
			'crud.url'    =>'faq.crud',
			'destroy.url' =>'faq.destroy',
  	]; 

  	public  $btnsColumns   = 
  	[

		[
			'link'=>'faq.edit',
			'text'=>'<i class="fa fa-pencil"></i>',
			'array'=>
			[
				'class'=>'btn btn-primary btn-xs tooltips',
				'data-original-title'=>'Editar registro',
				'data-placement'=>"top",
				'data-toggle'=>"tooltip"
			]
		],
		[
			'link'=>'#myModal',
			'modal'=>'true',
			'text'=>'<i class="fa fa-trash-o"></i>',
			'data-url'=>'faq.destroy',
			'array'=>
			[
				'class'=>'btn btn-danger btn-xs tooltips delModal',
				'data-original-title'=>'Excluir Registro',
				'data-placement'=>"top",
				'data-toggle'=>"modal"
			]
		],

	];


}