<?php
use LaravelBook\Ardent\Ardent;
/**
 * Formulario
 *
 * @property integer $id
 * @property integer $proposta_id
 * @property string $nome
 * @property string $sexo
 * @property string $tipo
 * @property string $pai
 * @property string $mae
 * @property string $nascimento
 * @property string $naturalidade
 * @property string $civil
 * @property string $casamento
 * @property string $separacao
 * @property string $rg
 * @property string $org_exp
 * @property string $data_exp
 * @property string $cpf
 * @property string $profissao
 * @property string $carteira_trabalho
 * @property string $pis
 * @property float $renda
 * @property string $cartorio
 * @property string $endereco
 * @property string $bairro
 * @property string $cep
 * @property string $municipio
 * @property string $uf
 * @property string $telefone
 * @property string $celular
 * @property string $endereco_com
 * @property string $bairro_com
 * @property string $cep_com
 * @property string $municipio_com
 * @property string $uf_com
 * @property string $telefone_com
 * @property string $empresa
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Proposta $proposta
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario wherePropostaId($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereNome($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereSexo($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereTipo($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario wherePai($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereMae($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereNascimento($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereNaturalidade($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereCivil($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereCasamento($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereSeparacao($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereRg($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereOrgExp($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereDataExp($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereCpf($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereProfissao($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereCarteiraTrabalho($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario wherePis($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereRenda($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereCartorio($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereEndereco($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereBairro($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereCep($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereMunicipio($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereUf($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereTelefone($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereCelular($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereEnderecoCom($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereBairroCom($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereCepCom($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereMunicipioCom($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereUfCom($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereTelefoneCom($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereEmpresa($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Formulario whereUpdatedAt($value)
 */
class Formulario extends Ardent implements ModelInterface{
	protected $fillable = ['nome','sexo','tipo','pai','mae','nascimento','naturalidade','civil','casamento','separacao','rg','org_exp','data_exp','cpf','profissao','carteira_trabalho','pis','renda','cartorio','endereco','bairro','cep','municipio','uf','telefone','celular','endereco_com','bairro_com','cep_com','municipio_com','uf_com','telefone_com','empresa'];
	protected $table = "formularios";


 	public $autoHydrateEntityFromInput = true;
   	public $autoPurgeRedundantAttributes = true;

	public static $rules 		  = 
	[
		'nome' => 'required|between:4,200',
		'cpf'    => 'required|regex:/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/i',
		'sexo'    => 'required',
		'pai'    => 'required',
		'mae'    => 'required',
		'nascimento'    => 'required|date',
		'naturalidade'    => 'required',
		'civil'    => 'required',
		'rg'    => 'required',
		'org_exp'    => 'required',
		'data_exp'    => 'required',
		'org_exp'    => 'required',
		'cep'    => 'required',
		'cep_com'    => 'required',
		'endereco'    => 'required',
		'endereco_com'    => 'required',


	];
		
	public static $customMessages = 
	[
		'required'  => '- O campo <b>:attribute</b> é obrigatorio.',
		'cpf.regex' =>	' - Preencha o <b>CPF</b> corretamente no formato (000.000.000-00)',
		'cpf.unique' =>	' - <b>CPF</b> já cadastrado favor verificar com a construtora',
		'email.unique' =>	' - <b>Email</b> já cadastrado favor verificar com a construtora',
		'email'     =>	'- Preencha o <b>email</b> corretamente.'
	];   


	public static $relationsData  = 
	[
		'user_id'   => array(self::BELONGS_TO, 'User'),
  	];	
 	public  $editColumns   = 
  	[
  		'Cliente' => 
  		[
	 		['id'=>'nome','name'=>'nome','campo'=>'Nome do Corretor','minlength'=>'','type'=>'text','class'=>'','placeholder'=>'Sr. Jose Almeida'],
			['id'=>'cpf','name'=>'cpf','campo'=>'CPF do Cliente','minlength'=>'','type'=>'text','class'=>'cpf','placeholder'=>''], 
			['id'=>'sexo','name'=>'sexo','campo'=>'Sexo','minlength'=>'','type'=>'select','required'=>'1','lista'=>[''=>'Escolha o sexo','Masculino'=>'Masculino','Feminino'=>'Feminino'],'class'=>'mask_cpf','placeholder'=>''],  
			['id'=>'pai','name'=>'pai','campo'=>'Nome do Pai','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'mae','name'=>'mae','campo'=>'Nome da Mãe','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'nascimento','name'=>'nascimento','campo'=>'Data Nascimento','minlength'=>'','type'=>'text','class'=>'data','placeholder'=>''], 
			['id'=>'naturalidade','name'=>'naturalidade','campo'=>'Naturalidade','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'civil','name'=>'civil','campo'=>'Estado Civil','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'casamento','name'=>'casamento','campo'=>'Data Casamento','minlength'=>'','type'=>'text','class'=>'data','placeholder'=>''], 
			['id'=>'separacao','name'=>'separacao','campo'=>'Separação','minlength'=>'','lista'=>[''=>'Escolha o tipo de Separação','com_parcial'=>'Parcial de bens','separacao'=>'Separação Total','universal'=>'Separação Universal'],'type'=>'select','class'=>'','placeholder'=>''], 
			['id'=>'rg','name'=>'rg','campo'=>'RG','minlength'=>'','type'=>'text','class'=>'rg','placeholder'=>''], 	
			['id'=>'org_exp','name'=>'org_exp','campo'=>'Orgão Exp','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'data_exp','name'=>'data_exp','campo'=>'Data de Expedição','minlength'=>'','type'=>'text','class'=>'data','placeholder'=>''], 
			['id'=>'profissao','name'=>'profissao','campo'=>'Profissão','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'carteira_trabalho','name'=>'carteira_trabalho','campo'=>'Carteira de Trabalho','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'pis','name'=>'pis','campo'=>'PIS','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'renda','name'=>'renda','campo'=>'Renda','minlength'=>'','type'=>'text','class'=>'money','placeholder'=>''], 
			['id'=>'cartorio','name'=>'cartorio','campo'=>'Cartorio','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'cep','name'=>'cep','campo'=>'CEP','minlength'=>'','type'=>'text','class'=>'cep','placeholder'=>''], 
			['id'=>'endereco','name'=>'endereco','campo'=>'Endereço','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'bairro','name'=>'bairro','campo'=>'Bairro','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'municipio','name'=>'municipio','campo'=>'Municipio','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'uf','name'=>'uf','campo'=>'Estado','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'telefone','name'=>'telefone','campo'=>'Telefone','minlength'=>'','type'=>'text','class'=>'telefone','placeholder'=>''], 
			['id'=>'celular','name'=>'celular','campo'=>'Celular','minlength'=>'','type'=>'text','class'=>'celular','placeholder'=>''], 
			['id'=>'cep_com','name'=>'cep_com','campo'=>'CEP Comercial','minlength'=>'','type'=>'text','class'=>'cep_com','placeholder'=>''], 
			['id'=>'endereco_com','name'=>'endereco_com','campo'=>'Endereco Comercial','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'bairro_com','name'=>'bairro_com','campo'=>'Bairro Comercial','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'municipio_com','name'=>'municipio_com','campo'=>'Municipio Comercial','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'uf_com','name'=>'uf_com','campo'=>'UF Comercial','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
			['id'=>'telefone_com','name'=>'telefone_com','campo'=>'Telefone Comercial','minlength'=>'telefone','type'=>'text','class'=>'telefone','placeholder'=>''], 
			['id'=>'empresa','name'=>'empresa','campo'=>'Empresa','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''], 
  		]

  	];


  	public  $settings      = 
  	[
  		'Cliente' => 
  		[  	
  			'model'=>'Cliente',
  			'title'=>'Lista de Clientes cadastrados',
			'thead'=>[
				['class'=>'numeric','name'=>'id'],
				['class'=>'','name'=>'Cliente'],
				['class'=>'','name'=>'Email'],
				['class'=>'','name'=>'CPF'],								
				['class'=>'numeric','name'=>'']
			],
  			'modal'=>[
				'title'=>'Deseja Excluir o registro',
				'description'=>'<b>Tem certeza que deseja Excluir o registro?</b> </br>Caso este registro esteja atrelado a outros como empreendimentos,imoveis os mesmos tambem serao deletados.',
				'btns'=>[]
			],
			'formTitle'   =>'Formulario de Cliente',
			'index.url'   =>'clientes.index' ,
			'insert.url'  =>'clientes.create' ,
			'store.url'   =>'formulario.store',
			'update.url'  =>'formulario.store',
			'crud.url'    =>'clientes.crud',
			'destroy.url' =>'clientes.destroy',
		]
  	]; 



}