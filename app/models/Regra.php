<?php
use LaravelBook\Ardent\Ardent;
/**
 * Regra
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Regra whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Regra whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Regra whereUpdatedAt($value) 
 */
class Regra extends Ardent implements ModelInterface{
	protected $fillable = [];
}