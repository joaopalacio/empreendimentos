<?php
use LaravelBook\Ardent\Ardent;
/**
 * Intermediaria
 *
 * @property integer $id
 * @property integer $proposta_id
 * @property integer $numero
 * @property string $vencimento
 * @property float $valor
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Proposta $proposta
 * @method static \Illuminate\Database\Query\Builder|\Intermediaria whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Intermediaria wherePropostaId($value)
 * @method static \Illuminate\Database\Query\Builder|\Intermediaria whereNumero($value)
 * @method static \Illuminate\Database\Query\Builder|\Intermediaria whereVencimento($value)
 * @method static \Illuminate\Database\Query\Builder|\Intermediaria whereValor($value)
 * @method static \Illuminate\Database\Query\Builder|\Intermediaria whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Intermediaria whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Intermediaria whereUpdatedAt($value)
 */
class Intermediaria extends Ardent implements ModelInterface{
	protected $fillable = ['numero','valor','vencimento','status'];
	protected $table = "intermediarias";

	public static $relationsData  = 
	[				
		'proposta' => array(self::BELONGS_TO, 'Proposta','foreignKey'=>'proposta_id'),	
  	];	
}