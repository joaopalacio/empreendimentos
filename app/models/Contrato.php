<?php
use LaravelBook\Ardent\Ardent;
class Contrato extends Ardent implements ModelInterface{
	protected $fillable = ['proposta_id','user_id'];
	protected $table = "contratos";
	
	public static $relationsData  = 
	[
		'proposta'                 => array(self::BELONGS_TO, 'Proposta','proposta_id'),
		'cliente'                 => array(self::BELONGS_TO, 'User','user_id'),
		'parcelas'  => array(self::HAS_MANY, 'Parcela', 'contrato_id'),	
  	];

   public  $tableColumns  = ['empreendimento','cliente','imovel','corretor'];

    public  $editColumns   = 
    [
     ];


  	public $settings =
  	[
  	    
  	];		
}