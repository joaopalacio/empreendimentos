<?php
use LaravelBook\Ardent\Ardent;
/**
 * Configuracao
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Configuracao whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Configuracao whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Configuracao whereUpdatedAt($value)
 */
class Configuracao extends Ardent implements ModelInterface{
	protected $fillable = ['endereco','email','telefone','cep','cidade','estado','facebook','twitter','youtube','googleplus','linkedin'];
	protected $table = "settings";
	


 	public $autoHydrateEntityFromInput = true;
   	public $autoPurgeRedundantAttributes = true;


  	public  $editColumns   = 
  	[
  		['id'=>'cep','name'=>'cep','campo'=>'CEP','minlength'=>'','type'=>'text','required'=>'','class'=>'cep','placeholder'=>'000000-000'],
		['id'=>'endereco','name'=>'endereco','campo'=>'Endereço','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'Digite o Endereço'],		
		['id'=>'cidade','name'=>'cidade','campo'=>'Cidade','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'Digite a Cidade da Construtora'],	
		['id'=>'estado','name'=>'estado','campo'=>'Estado','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'Digite o Estado da Construtora'],
		['id'=>'telefone','name'=>'telefone','campo'=>'Telefone','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'(99) 9999-9999'],		
		['id'=>'email','name'=>'email','campo'=>'Email','minlength'=>'','type'=>'email','class'=>'','placeholder'=>'Digite um email para contato'],		
		['id'=>'facebook','name'=>'facebook','campo'=>'Facebook url','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'Ex: https://www.facebook.com/fanpage'],		
		['id'=>'twiiter','name'=>'twitter','campo'=>'Twitter url','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'Ex: https://www.twitter.com/username'],		
		['id'=>'googleplus','name'=>'googleplus','campo'=>'Google+ url','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'Ex: https://plus.google.com/username'],		
		['id'=>'youtube','name'=>'youtube','campo'=>'Youtube Channel url','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'Ex: https://youtube.com/channel/username'],		
		['id'=>'linkedin','name'=>'linkedin','campo'=>'Linkedin url','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'Ex: https://br.linkedin.com/empresa'],		

  	];


  	public  $settings      = 
  	[
  			'model'=>'Configuracao',
  			'title'=>'Lista de Paginas cadastradas',
			'formTitle'   =>'Formulario de Paginas',
			'index.url'   =>'settings.index' ,
			'update.url'  =>'settings.update',
			'crud.url'    =>'settings.crud',
  	]; 




}