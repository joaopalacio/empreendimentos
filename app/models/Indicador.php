<?php
use LaravelBook\Ardent\Ardent;
/**
 * Indicador
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Ardent\Collection|\IndicadorValor[] $valores
 * @method static \Illuminate\Database\Query\Builder|\Indicador whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Indicador whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Indicador whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Indicador whereUpdatedAt($value)
 */
class Indicador extends Ardent implements ModelInterface{
	protected $table = "indicadores";
	protected $fillable = ['name'];


 	public $autoHydrateEntityFromInput = true;
   	public $autoPurgeRedundantAttributes = true;

	public static $rules 		  = 
	[
		'name'  					  => 'required',
	];
		
	public static $customMessages = 
	[
		'required'                    => '- Este campo é obrigatorio.'
	];     
		
		
	public static $relationsData  = 
	[
		'valores'                     => array(self::HAS_MANY, 'IndicadorValor', 'indicador_id'),
  	];

  	public  $tableColumns  = ['name'];

  	public  $editColumns   = 
  	[
		['id'=>'Indicador','name'=>'name','campo'=>'Nome da Indicador','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'Nome da Indicador']
  	];


  	public  $settings      = 
  	[
  			'model'=>'Indicador',
  			'title'=>'Lista de Indicadores cadastradas',
			'thead'=>[
				['class'=>'','name'=>'Indicador'],
				['class'=>'numeric','name'=>'']
			],
  			'modal'=>[
				'title'=>'Deseja Excluir o registro',
				'description'=>'<b>Tem certeza que deseja Excluir o registro?</b> </br>Caso este registro esteja atrelado a outros como empreendimentos,imoveis os mesmos tambem serao deletados.',
				'btns'=>[]
			],
			'formTitle'   =>'Formulario de Indicadores',
			'index.url'   =>'indicadores.index' ,
			'insert.url'  =>'indicadores.create' ,
			'children.url'=>'indicadores_valor.index',
			'store.url'   =>'indicadores.store',
			'edit.url'    =>'indicadores.edit',
			'update.url'  =>'indicadores.update',
			'crud.url'    =>'indicadores.crud',
			'destroy.url' =>'indicadores.destroy',
  	]; 

  	public  $btnsColumns   = 
  	[
		[
			'link'=>'valores.index',
			'text'=>'<i class="fa fa-superscript"></i>',
			'array'=>
			[
				'class'=>'btn btn-warning btn-xs tooltips',
				'data-original-title'=>'Valores base de cada mes',
				'data-placement'=>"top",
				'data-toggle'=>"tooltip"
			]
		],
		[
			'link'=>'indicadores.edit',
			'text'=>'<i class="fa fa-pencil"></i>',
			'array'=>
			[
				'class'=>'btn btn-primary btn-xs tooltips',
				'data-original-title'=>'Editar',
				'data-placement'=>"top",
				'data-toggle'=>"tooltip"
			]
		],
/*		[
			'link'=>'#myModal',
			'modal'=>'true',
			'text'=>'<i class="fa fa-trash-o"></i>',
			'data-url'=>'indicadores.destroy',
			'array'=>
			[
				'class'=>'btn btn-danger btn-xs tooltips delModal',
				'data-original-title'=>'Excluir',
				'data-placement'=>"top",
				'data-toggle'=>"modal"
			]
		],
*/
	];


}