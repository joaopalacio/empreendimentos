<?php

use LaravelBook\Ardent\Ardent;

/**
 * Empreendimento
 *
 * @property integer $id
 * @property string $nome
 * @property string $destaque
 * @property integer $cidade_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Cidade $cidade
 * @property-read \Illuminate\Database\Ardent\Collection|\Imovel[] $imoveis
 * @method static \Illuminate\Database\Query\Builder|\Empreendimento whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Empreendimento whereNome($value)
 * @method static \Illuminate\Database\Query\Builder|\Empreendimento whereDestaque($value)
 * @method static \Illuminate\Database\Query\Builder|\Empreendimento whereCidadeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Empreendimento whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Empreendimento whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Ardent\Collection|\Estagio[] $estagios
 * @property string $description
 * @property string $url-clean
 * @property string $endereco
 * @property string $gmaps
 * @property string $span
 * @method static \Illuminate\Database\Query\Builder|\Empreendimento whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Empreendimento whereUrlClean($value)
 * @method static \Illuminate\Database\Query\Builder|\Empreendimento whereEndereco($value)
 * @method static \Illuminate\Database\Query\Builder|\Empreendimento whereGmaps($value)
 * @method static \Illuminate\Database\Query\Builder|\Empreendimento whereSpan($value)
 * @property integer $bairro_id
 * @method static \Illuminate\Database\Query\Builder|\Empreendimento whereBairroId($value)
 * @property-read \Bairro $bairro
 */
class Empreendimento extends Ardent implements ModelInterface
{
    protected $fillable = ['nome','destaque','description','endereco','gmaps','banco_id','span','url-clean','cidade_id','bairro_id','caption','destaque_home','tipo_destaque','previsao_entrega'];
    protected $table = "empreendimentos";

    public $autoHydrateEntityFromInput   = true;
    public $autoPurgeRedundantAttributes = true;
    public static $rules                 =
    [
        'nome' => 'required',
        'banco_id' => 'required',
        'cidade_id' => 'required',
        'bairro_id' => 'required',
        'endereco' => 'required',
        'tipo_destaque' => 'required',
        'caption' => 'required',
        'previsao_entrega' => 'required'
    ];
        
    public static $customMessages =
    [
        'required'                    => '- O campo :attribute é obrigatorio.',
    ];  

    public static $sluggable      =
    [
        'build_from'                  => 'nome',
        'save_to'                     => 'url-clean',
    ];    
        
    public static $relationsData  =
    [
        'imoveis'  => array(self::HAS_MANY, 'Imovel', 'empreendimento_id'),
        'estagios' => array(self::HAS_MANY, 'Estagio', 'empreendimento_id'),
        'uploads'  => array(self::MORPH_MANY, 'UploadImage','name' => 'uses', 'table' => 'images'),                              
        'bairro'   => array(self::BELONGS_TO, 'Bairro'),
        'banco'   => array(self::BELONGS_TO, 'Banco'),
        'cidades'   => array(self::BELONGS_TO, 'Cidade')
      ];

    public function delete()
    {
        // delete all related 
        $this->imoveis()->delete();
        $this->estagios()->delete();
        $this->uploads()->delete();
        // as suggested by Dirk in comment,
        // it's an uglier alternative, but faster
        // Photo::where("user_id", $this->id)->delete()
        return parent::delete();
    }
      public $tableColumns  = ['id','nome','endereco','dropdown'=>[],'checkbox'=>['destaque','destaque']];

      public $editColumns   =
      [
        ['id'=>'nome','name'=>'nome','campo'=>'Nome do Empreendimento','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'EX: Decor Empreendimento'],
        ['id'=>'banco_id','lista'=>[],'value'=>'','name'=>'banco_id','campo'=>'Banco financiador','minlength'=>'','type'=>'select','required'=>'','class'=>'form-control m-bot15','placeholder'=>''],
        ['id'=>'cidade_id','lista'=>[],'value'=>'','name'=>'cidade_id','campo'=>'Cidade','minlength'=>'','type'=>'select','required'=>'','class'=>'form-control m-bot15 change_bairros','placeholder'=>'EX: Rua Correia Dias, 120 - 04104000, Sao paulo / SP'],
        ['id'=>'bairro_id','lista'=>[],'value'=>'','name'=>'bairro_id','campo'=>'Bairro','minlength'=>'','type'=>'select','required'=>'','class'=>'form-control m-bot15','placeholder'=>'EX: Rua Correia Dias, 120 - 04104000, Sao paulo / SP'],                
        ['id'=>'endereco','name'=>'endereco','campo'=>'Endereco Completo','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'EX: Rua Correia Dias, 120 - 04104000'],        
        ['id'=>'gmaps','name'=>'gmaps','campo'=>'Latitude e Longitude','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'EX: -45.60940,32.09289'],    
        ['id'=>'description','name'=>'description','campo'=>'Descrição','minlength'=>'','type'=>'textarea','required'=>'','class'=>'','placeholder'=>'EX: Descrição do Empreendimento a ser anunciado'],                
        ['id'=>'tipo_destaque','lista'=>['0'=>'Escolha um tipo de chamada para a home do site','imovelP'=>'Chamada Pequena','imovelG'=>'Chamada Grande','imovelDestaque'=>'Chamada Destaque'],'value'=>'','name'=>'tipo_destaque','campo'=>'Tipo de Chamada','minlength'=>'','type'=>'select','required'=>'','class'=>'form-control m-bot15',],                        
        ['id'=>'caption','name'=>'caption','campo'=>'Subchamada para Home','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'EX: Pequena descrição de 100 caracteres para home '],        
        ['id'=>'previsao_entrega','name'=>'previsao_entrega','campo'=>'Previsão de Enterega','minlength'=>'','type'=>'text','required'=>'','class'=>'datepickermes data','placeholder'=>'EX:mm/YYYY'],        
      ];


      public $settings =
      [
              'model'=>'Empreendimento',
              'title'=>'Lista de Empreendimentos cadastradas',
              'thead'=>[
                    ['class'=>'numeric','name'=>'id'],
                    ['class'=>'','name'=>'Empreendimento'],
                    ['class'=>'','name'=>'Endereço'],    
                    ['class'=>'','name'=>'Status'],                            
                    ['class'=>'','name'=>'Destaque'],                            
                    ['class'=>'numeric','name'=>'']
               ],
              'modal'=>[
                'title'=>'Deseja Excluir o registro',
                'description'=>'<b>Tem certeza que deseja Excluir o registro?</b> </br>Caso este registro esteja atrelado a outros como empreendimentos,imoveis os mesmos tambem serao deletados.',
                'btns'=>[]
            ],
            'formTitle'   =>'Formulario de Cidades',
            'index.url'   =>'empreendimentos.index' ,
            'insert.url'  =>'empreendimentos.create' ,
            'children.url'=>'imoveis.index',
            'store.url'   =>'empreendimentos.store',
            'edit.url'    =>'empreendimentos.edit',
            'update.url'  =>'empreendimentos.update',
            'photos.url'  =>'empreendimentos.photos',
            'upload.url'  =>'empreendimentos.upload',            
            'crud.url'    =>'empreendimentos.crud',
            'destroy.url' =>'empreendimentos.destroy',
      ]; 

      public $btnsColumns   =
      [
        [
            'link'=>'empreendimentos.photos',
            'text'=>'<i class="fa  fa-camera"></i>',
            'array'=>
            [
                'class'=>'btn btn-default btn-xs tooltips',
                'data-original-title'=>'Adicionar imagens',
                'data-placement'=>"top",
                'data-toggle'=>"tooltip"
            ]
        ],            
        [
            'link'=>'imoveis.index',
            'text'=>'<i class="fa  fa-map-marker"></i>',
            'array'=>
            [
                'class'=>'btn btn-info btn-xs tooltips',
                'data-original-title'=>'Imoveis deste Empreendimento',
                'data-placement'=>"top",
                'data-toggle'=>"tooltip"
            ]
        ],
        [
            'link'=>'estagio.index',
            'text'=>'<i class="fa fa-bar-chart-o"></i>',
            'array'=>
            [
                'class'=>'btn btn-warning btn-xs tooltips',
                'data-original-title'=>'Estagio da Obra',
                'data-placement'=>"top",
                'data-toggle'=>"tooltip"
            ]
        ],        
        [
            'link'=>'empreendimentos.edit',
            'text'=>'<i class="fa fa-pencil"></i>',
            'array'=>
            [
                'class'=>'btn btn-primary btn-xs tooltips',
                'data-original-title'=>'Editar Registro',
                'data-placement'=>"top",
                'data-toggle'=>"tooltip"
            ]
        ],
        [
            'link'=>'#myModal',
            'modal'=>'true',
            'text'=>'<i class="fa fa-trash-o"></i>',
            'data-url'=>'empreendimentos.destroy',
            'array'=>
            [
                'class'=>'btn btn-danger btn-xs tooltips delModal',
                'data-original-title'=>'Excluir registro',
                'data-placement'=>"top",
                'data-toggle'=>"modal"
            ]
        ],

    ];

    public function getSelect()
    {
        return Cidade::all()->toArray();
    }

     public function beforeSave() {
        $this->{'url-clean'} = $this->toAscii($this->nome);
        if($this->isDirty('previsao_entrega')) {
          $this->previsao_entrega = Carbon\Carbon::createFromFormat('d/m/Y', $this->previsao_entrega)->toDateTimeString();
        }
        return true;
      }
    

    private function toAscii($str) {
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", '-', $clean);

        return $clean;
    }
}
