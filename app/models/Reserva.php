<?php
use LaravelBook\Ardent\Ardent;
/**
 * Reserva
 *
 * @property integer $id
 * @property integer $imovel_id
 * @property integer $corretor_id
 * @property integer $user_id
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Imovel $imovel
 * @property-read \User $cliente
 * @property-read \User $corretor
 * @method static \Illuminate\Database\Query\Builder|\Reserva whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Reserva whereImovelId($value)
 * @method static \Illuminate\Database\Query\Builder|\Reserva whereCorretorId($value)
 * @method static \Illuminate\Database\Query\Builder|\Reserva whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Reserva whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Reserva whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Reserva whereUpdatedAt($value)
 */
class Reserva extends Ardent implements ModelInterface{
	protected $fillable = ['status'];
	protected $table = "reserva";

	public static $relationsData  = 
	[		
		'imovel'         => array(self::BELONGS_TO, 'Imovel','foreignKey' => 'imovel_id' ),		
		'cliente'         => array(self::BELONGS_TO, 'User','foreignKey' => 'user_id'),
		'corretor'         => array(self::BELONGS_TO, 'User','foreignKey' => 'corretor_id' )										
  	];	

	public  $tableColumns  = ['id','empreendimento','imovel','cliente','ucfirst'=>'status'];

  	public  $editColumns   = 
  	[
		['id'=>'localizacao','name'=>'localizacao','campo'=>'Localização','minlength'=>'','type'=>'text','required'=>'true','class'=>'','placeholder'=>''],		
											
  	];


  	public  $settings      = 
  	[
  			'model'=>'Imoveis',
  			'parents'=>'empreendimentos',
  			'title'=>'Lista de Reservas cadastradas',
			'thead'=>[
				['class'=>'numeric','name'=>'id'],
				['class'=>'','name'=>'Empreendimento'],
				['class'=>'numeric','name'=>'Imovel'],
				['class'=>'numeric','name'=>'Cliente'],
				['class'=>'numeric','name'=>'Status'],
				['class'=>'numeric','name'=>'Proposta'],
				
			],
  			'modal'=>[
				'title'=>'Escolha o Cliente para essa reserva.',
				'description'=>'<b>Escolha o cliente que já possui as fichas cadastrais completas?</b> </br>
				<div class="form-group ">
            			<label for="cname" class="control-label col-lg-2">Cliente</label>
            			<div class="col-lg-10">
                			<select class="form-control" id="cliente" minlength="" placeholder="" name="cliente">
                				<option value="">Escolha o cliente</option>
                			</select>            
                		</div>
       			</div>',
				'btns'=>[]
			],
			'formTitle'   =>'Formulario de Bairros',
			'index.url'   =>'reservas.index' ,
			'index.admin.url'   =>'reservas.admin.index' ,
			'insert.url'  =>'reservas.create' ,
			'store.url'   =>'imoveis.store',
			'photos.url'  =>'imoveis.photos',
			'upload.url'  =>'imoveis.upload',
			'crud.url'    =>'reservas.crud',
			'destroy.url' =>'ajax.corretores.set.clientes',
  	]; 
  	public  $btnsColumns   = 
  	[	
		[
			'link'=>'#myModal',
			'modal'=>'true',
			'text'=>'<i class="fa fa-users"></i>',
			'data-url'=>'reservas.cliente',
			'array'=>
			[
				'class'=>'btn btn-success btn-xs tooltips delModal',
				'data-original-title'=>'Escolha o cliente da reserva',
				'data-placement'=>"top",
				'data-toggle'=>"modal"
			]
		],
	];

	public function scopeminhasReservas($query){
		return $query->whereCorretorId(Auth::user()->id);
	}

}
	