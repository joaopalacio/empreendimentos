<?php
use LaravelBook\Ardent\Ardent;
/**
 * Contato
 *
 * @property integer $id
 * @property string $nome
 * @property string $email
 * @property string $telefone
 * @property string $mensagem
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Contato whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Contato whereNome($value)
 * @method static \Illuminate\Database\Query\Builder|\Contato whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Contato whereTelefone($value)
 * @method static \Illuminate\Database\Query\Builder|\Contato whereMensagem($value)
 * @method static \Illuminate\Database\Query\Builder|\Contato whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Contato whereUpdatedAt($value)
 */
class Contato extends Ardent implements ModelInterface{
	protected $fillable = ['nome','email','telefone','mensagem'];
	protected $table = "contato";
}