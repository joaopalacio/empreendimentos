<?php
use LaravelBook\Ardent\Ardent;
/**
 * RegraIntermediaria
 *
 * @property integer $id
 * @property string $mes
 * @property float $valor
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $regras_id
 * @property integer $regras_proposta_id
 * @property-read \RegrasProposta $regras_proposta
 * @method static \Illuminate\Database\Query\Builder|\RegraIntermediaria whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\RegraIntermediaria whereMes($value)
 * @method static \Illuminate\Database\Query\Builder|\RegraIntermediaria whereValor($value)
 * @method static \Illuminate\Database\Query\Builder|\RegraIntermediaria whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\RegraIntermediaria whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\RegraIntermediaria whereRegrasId($value)
 * @method static \Illuminate\Database\Query\Builder|\RegraIntermediaria whereRegrasPropostaId($value)
 */
class RegraIntermediaria extends Ardent implements ModelInterface{
	protected $fillable = ['mes','valor'];
	protected $table = "regras_intermediarias";
	
	public static $relationsData  = 
	[		
		'regras_proposta' => array(self::BELONGS_TO, 'RegrasProposta', 'foreignKey' => 'regras_id' )		
  	];				
}