<?php
use LaravelBook\Ardent\Ardent;
/**
 * Proposta
 *
 * @property integer $id
 * @property float $desconto
 * @property float $entrada
 * @property float $chaves
 * @property float $porcent
 * @property integer $prazo
 * @property integer $user_id
 * @property integer $corretor_id
 * @property integer $imovel_id
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \User $cliente
 * @property-read \User $corretor
 * @property-read \Imovel $imovel
 * @method static \Illuminate\Database\Query\Builder|\Proposta whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Proposta whereDesconto($value)
 * @method static \Illuminate\Database\Query\Builder|\Proposta whereEntrada($value)
 * @method static \Illuminate\Database\Query\Builder|\Proposta whereChaves($value)
 * @method static \Illuminate\Database\Query\Builder|\Proposta wherePorcent($value)
 * @method static \Illuminate\Database\Query\Builder|\Proposta wherePrazo($value)
 * @method static \Illuminate\Database\Query\Builder|\Proposta whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Proposta whereCorretorId($value)
 * @method static \Illuminate\Database\Query\Builder|\Proposta whereImovelId($value)
 * @method static \Illuminate\Database\Query\Builder|\Proposta whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Proposta whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Proposta whereUpdatedAt($value)
 */
class Proposta extends Ardent implements ModelInterface{
	protected $fillable = ['desconto','entrada','chaves','porcent','prazo','status'];
	protected $table = "propostas";

	public static $relationsData  = 
	[		
		'reserva' => array(self::BELONGS_TO, 'Reserva','foreignKey'=>'reserva_id'),	
    'intermediarias' => array(self::HAS_MANY, 'Intermediaria'), 
  ];		



  public  $tableColumns  = ['id','empreendimento','imovel','cliente','ucfirst'=>'status'];

    public  $editColumns   = 
    [
      ['id'=>'localizacao','name'=>'localizacao','campo'=>'Localização','minlength'=>'','type'=>'text','required'=>'true','class'=>'','placeholder'=>''],                       
    ];


  	public $settings =
  	[
  	    'corretor' =>
  	    [
  	        'views'=>
  	        [
  	            'cadastro.form'=>'propostas.corretor.crud',
                'lista.table'=>'propostas.corretor.index',
  	        ]
  	    ],
        'admin'=>
        [
          'views'=>
          [
            'lista.table'=>'propostas.admin.index',
            'documento'=>'propostas.admin.documento'
          ]
        ]
  	];
}