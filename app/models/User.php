<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use LaravelBook\Ardent\Ardent;
/**
 * User
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property integer $role_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Role $role
 * @property-read \Illuminate\Database\Ardent\Collection|\Proposta[] $propostas_enviadas
 * @property-read \Illuminate\Database\Ardent\Collection|\Proposta[] $propostas_solicitada
 * @property-read \Illuminate\Database\Ardent\Collection|\Reserva[] $reservas_enviadas
 * @property-read \Illuminate\Database\Ardent\Collection|\Reserva[] $reservas_solicitada
 * @method static \Illuminate\Database\Query\Builder|\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereUpdatedAt($value)
 * @property string $name
 * @property string $cpf
 * @method static \Illuminate\Database\Query\Builder|\User whereName($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereCpf($value) 
 * @method static \User corretores() 
 */


class User extends Ardent implements UserInterface, RemindableInterface,ModelInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

 	public $autoHydrateEntityFromInput = true;
   	public $autoPurgeRedundantAttributes = true;

	public static $rules 		  = 
	[
		'name' => 'required|between:4,200',
		'cpf'    => 'required|regex:/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/i|unique:users',
		'email'  =>' email|required|unique:users'		
	];
		
	public static $customMessages = 
	[
		'required'  => '- O campo <b>:attribute</b> é obrigatorio.',
		'cpf.regex' =>	' - Preencha o <b>CPF</b> corretamente no formato (000.000.000-00)',
		'cpf.unique' =>	' - <b>CPF</b> já cadastrado favor verificar com a construtora',
		'email.unique' =>	' - <b>Email</b> já cadastrado favor verificar com a construtora',
		'email'     =>	'- Preencha o <b>email</b> corretamente.'
	];     
		
		
	public static $relationsData  = 
	[
		'role'       => array(self::BELONGS_TO, 'Role'),
		'propostas'  => array(self::HAS_MANY, 'Proposta', 'user_id'),
		'reservas'   => array(self::HAS_MANY, 'Reserva', 'user_id'),
		'formulario' => array(self::HAS_MANY, 'Formulario', 'user_id'),		
  	];

  	public  $tableColumns  = ['id','name','email','cpf'];

  	public  $editColumns   = 
  	[
  		'Cliente' => 
  		[
	 		['id'=>'name','name'=>'name','campo'=>'Nome do Cliente','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'Sr. Jose Almeida'],
			['id'=>'email','name'=>'email','campo'=>'Email do Cliente','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'cliente@cliente.com.br'],
			['id'=>'cpf','name'=>'cpf','campo'=>'CPF do Cliente','minlength'=>'','type'=>'text','required'=>'','class'=>'mask_cpf cpf','placeholder'=>'000.000.000-00'], 		
  		]

  	];


  	public  $settings      = 
  	[
  		'Cliente' => 
  		[  	
  			'model'=>'Cliente',
  			'title'=>'Lista de Clientes cadastrados',
			'thead'=>[
				['class'=>'numeric','name'=>'id'],
				['class'=>'','name'=>'Cliente'],
				['class'=>'','name'=>'Email'],
				['class'=>'','name'=>'CPF'],								
				['class'=>'numeric','name'=>'']
			],
  			'modal'=>[
				'title'=>'Deseja Excluir o registro',
				'description'=>'<b>Tem certeza que deseja Excluir o registro?</b> </br>Caso este registro esteja atrelado a outros como empreendimentos,imoveis os mesmos tambem serao deletados.',
				'btns'=>[]
			],
			'formTitle'   =>'Formulario de Cliente',
			'index.url'   =>'clientes.index' ,
			'insert.url'  =>'clientes.create' ,
			'store.url'   =>'clientes.store',
			'edit.url'    =>'clientes.edit',
			'update.url'  =>'clientes.update',
			'crud.url'    =>'clientes.crud',
			'destroy.url' =>'clientes.destroy',
		]
  	]; 

  	public  $btnsColumns   = 
  	[
  		'Cliente' => 
  		[  
			[
				'link'=>'clientes.ficha',
				'text'=>'<i class="fa fa-male"></i>',
				'array'=>
				[
					'class'=>'btn btn-warning btn-xs tooltips',
					'data-original-title'=>'Ficha do Cliente',
					'data-placement'=>"top",
					'data-toggle'=>"tooltip"
				]
			],   		
			[
				'link'=>'clientes.ficha.conjuge',
				'route_format'=>'Sim',
				'text'=>'<i class="fa fa-female"></i>',
				'array'=>
				[
					'class'=>'btn btn-default btn-xs tooltips',
					'data-original-title'=>'Ficha do Cônjuge',
					'data-placement'=>"top",
					'data-toggle'=>"tooltip"
				]
			],  			
			[
				'link'=>'clientes.edit',
				'text'=>'<i class="fa fa-pencil"></i>',
				'array'=>
				[
					'class'=>'btn btn-primary btn-xs tooltips',
					'data-original-title'=>'Editar registro',
					'data-placement'=>"top",
					'data-toggle'=>"tooltip"
				]
			],
			[
				'link'=>'#myModal',
				'modal'=>'true',
				'text'=>'<i class="fa fa-trash-o"></i>',
				'data-url'=>'clientes.destroy',
				'array'=>
				[
					'class'=>'btn btn-danger btn-xs tooltips delModal',
					'data-original-title'=>'Deletar Registro',
					'data-placement'=>"top",
					'data-toggle'=>"modal"
				]
			],
		]

	];



	public function scopemeusClientes($query){
		$roles = Role::where('name','=','Cliente')->first();
		return $query
					->where('role_id','=',$roles->id)
					->where('corretor_id','=',Auth::user()->id);
	}

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}


	public function getRememberToken(){

	}

	public function setRememberToken($value){

	}

	public function getRememberTokenName(){

	}

}