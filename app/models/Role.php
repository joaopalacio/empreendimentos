<?php
use LaravelBook\Ardent\Ardent;
/**
 * Role
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Ardent\Collection|\User[] $usuarios
 * @method static \Illuminate\Database\Query\Builder|\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Role whereUpdatedAt($value)
 */
class Role extends Ardent implements ModelInterface{
	protected $fillable = ['name'];
	protected $table = "roles";

	public static $relationsData  = 
	[		
		'usuarios' => array(self::HAS_MANY, 'User','role_id'),												
  	];		
}