<?php
use LaravelBook\Ardent\Ardent;
/**
 * Imovel
 *
 * @property integer $id
 * @property integer $empreendimento_id
 * @property string $tipo
 * @property string $descricao
 * @property integer $quartos
 * @property integer $suites
 * @property integer $vagas
 * @property float $area_privativa
 * @property float $area_total
 * @property float $area_comum
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Imovel whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Imovel whereEmpreendimentoId($value)
 * @method static \Illuminate\Database\Query\Builder|\Imovel whereTipo($value)
 * @method static \Illuminate\Database\Query\Builder|\Imovel whereDescricao($value)
 * @method static \Illuminate\Database\Query\Builder|\Imovel whereQuartos($value)
 * @method static \Illuminate\Database\Query\Builder|\Imovel whereSuites($value)
 * @method static \Illuminate\Database\Query\Builder|\Imovel whereVagas($value)
 * @method static \Illuminate\Database\Query\Builder|\Imovel whereAreaPrivativa($value)
 * @method static \Illuminate\Database\Query\Builder|\Imovel whereAreaTotal($value)
 * @method static \Illuminate\Database\Query\Builder|\Imovel whereAreaComum($value)
 * @method static \Illuminate\Database\Query\Builder|\Imovel whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Imovel whereUpdatedAt($value)
 * @property-read \Empreendimento $empreendimento
 * @property-read \Illuminate\Database\Eloquent\Collection|\Proposta[] $propostas
 * @property-read \Illuminate\Database\Eloquent\Collection|\Reserva[] $reservas
 * @property-read \Illuminate\Database\Eloquent\Collection|\User[] $corretores
 */
class Imovel extends Ardent implements ModelInterface{
	protected $fillable = ['tipo','descricao','quartos','suites','vagas','area_privativa','area_total','nome','area_comum'];
	protected $table = "imoveis";

 	public $autoHydrateEntityFromInput = true;
   	public $autoPurgeRedundantAttributes = true;

	public static $rules 		  = 
	[
		'tipo'  					  => 'required',
		'descricao'  					  => 'required',
		'quartos'  					  => 'required|numeric',
		'suites'  					  => 'required|numeric',
		'vagas'  					  => 'required|numeric',
		'area_privativa'  					  => 'required|numeric',
		'area_total'  					  => 'required|numeric',
		'nome'  					  => 'required',
		'area_comum'  					  => 'required|numeric',
	];
		
	public static $customMessages = 
	[
		'required'                    => '- O campo :attribute obrigatorio.',
		'numeric'                    => '- O campo :attribute deve ser um numero. (Ex: 100.00)',
	];     

		
	public static $relationsData  = 
	[
		'reservas'       => array(self::HAS_MANY, 'Reserva', 'imovel_id'),
		'regras'         => array(self::HAS_MANY, 'RegrasProposta', ),		
		'propostas'      => array(self::HAS_MANY, 'Proposta', 'imovel_id'),		
		'corretores'     => array(self::BELONGS_TO_MANY, 'User','pivotKeys'=>['user_id','imovel_id'] ,'table' => 'imovel_corretor'),	
		'uploads' 		 => array(self::MORPH_MANY, 'UploadImage','name' => 'uses', 'table' => 'images'),						
		'empreendimento' => array(self::BELONGS_TO, 'Empreendimento'),		
  	];

    public function delete()
    {
        // delete all related         
        $this->reservas()->delete();
        $this->propostas()->delete();
        $this->corretores()->delete();
        $this->uploads()->delete();
        // as suggested by Dirk in comment,
        // it's an uglier alternative, but faster
        // Photo::where("user_id", $this->id)->delete()
        return parent::delete();
    }

  	public  $tableColumns  = ['id','tipo','quartos','suites','vagas','area_privativa','area_total','area_comum'];

  	public  $editColumns   = 
  	[
		['id'=>'nome','name'=>'nome','campo'=>'Descrição','minlength'=>'','type'=>'text','class'=>'','placeholder'=>'Ex: Apartamento Planta Tipo 1 (2 quartos)'],	
		['id'=>'quantidade','name'=>'quantidade','campo'=>'Quantidade deste tipo','minlength'=>'','type'=>'spinner','required'=>'true','class'=>'','placeholder'=>''],  	
		['id'=>'tipo','lista'=>[''=>'Selecione o tipo de imovel','apartamento'=>'Apartamento', 'loft'=>'Loft', 'cobertura'=>'Cobertura', 'studio'=>'Studio', 'flat'=>'Flat', 'casa padrão'=>'Casa Padrão', 'sobrado'=>'Sobrado', 'casa de condomínio'=>'Casa de Codomínio', 'casa de vila'=> 'Casa de Vila'],'name'=>'tipo','campo'=>'Tipo','minlength'=>'','type'=>'select','required'=>'true','class'=>'','placeholder'=>''],
		['id'=>'quartos','name'=>'quartos','campo'=>'Quartos','minlength'=>'','type'=>'spinner','required'=>'true','class'=>'','placeholder'=>''],
		['id'=>'suites','name'=>'suites','campo'=>'Suítes','minlength'=>'','type'=>'spinner','required'=>'true','class'=>'','placeholder'=>''],
		['id'=>'vagas','name'=>'vagas','campo'=>'Vagas','minlength'=>'','type'=>'spinner','required'=>'true','class'=>'','placeholder'=>''],
		['id'=>'area_privativa','name'=>'area_privativa','campo'=>'Área Privativa (m2)','minlength'=>'','type'=>'text','required'=>'true','class'=>'','placeholder'=>''],
		['id'=>'area_total','name'=>'area_total','campo'=>'Área Total (m2)','minlength'=>'','type'=>'text','required'=>'true','class'=>'','placeholder'=>''],	
		['id'=>'descricao','name'=>'descricao','campo'=>'Descrição','minlength'=>'','type'=>'textarea','required'=>'true','class'=>'','placeholder'=>''],		
		['id'=>'area_comum','name'=>'area_comum','campo'=>'Área Comum (m2)','minlength'=>'','type'=>'text','required'=>'true','class'=>'','placeholder'=>''],	
		['fieldSet' =>
			[
				['id'=>'desconto_padrao','name'=>'desconto_padrao','campo'=>'Desconto Padrão','minlength'=>'','type'=>'text','required'=>'true','class'=>'percent','placeholder'=>'0.00%'],
				['id'=>'valor_imovel','name'=>'valor_imovel','campo'=>'Valor do Imovel','minlength'=>'','type'=>'text','required'=>'true','class'=>'money','placeholder'=>''],	
				['id'=>'entrada_minima','name'=>'entrada_minima','campo'=>'Entrada Mínima','minlength'=>'','type'=>'text','required'=>'true','class'=>'money','placeholder'=>''],	
				['id'=>'chaves_maxima','name'=>'chaves_maxima','campo'=>'Chaves Máxima','minlength'=>'','type'=>'text','required'=>'true','class'=>'money','placeholder'=>''],	
				['id'=>'porcent_financiamento','name'=>'porcent_financiamento','campo'=>'Procentagem financiamento','minlength'=>'','type'=>'text','required'=>'true','class'=>'percent','placeholder'=>'0.00%'],
				['id'=>'prazo','name'=>'prazo','campo'=>'Prazo maximo financiamento (meses)','minlength'=>'','type'=>'text','required'=>'true','class'=>'','placeholder'=>''],	
				['id'=>'intermediarias','lista'=>[0,60],'name'=>'intermediarias','campo'=>'Intermediárias','minlength'=>'','type'=>'selectRange','required'=>'true','class'=>'','placeholder'=>''],	
				['id'=>'lista','name'=>'lista','campo'=>'Lista das Intermediárias','minlength'=>'','type'=>'div'],	
				['id'=>'corretores','name'=>'corretores','campo'=>'Corretores autorizados','values'=>'','minlength'=>'','type'=>'multi'],	
			]
		]											
  	];


  	public  $settings      = 
  	[
  			'model'=>'Imoveis',
  			'parents'=>'empreendimentos',
  			'title'=>'Lista de Imoveis cadastradas',
			'thead'=>[
				['class'=>'numeric','name'=>'id'],
				['class'=>'','name'=>'Tipo'],
				['class'=>'numeric','name'=>'Quartos'],
				['class'=>'numeric','name'=>'Suites'],
				['class'=>'numeric','name'=>'Vagas'],
				['class'=>'numeric','name'=>'Área Privativa'],
				['class'=>'numeric','name'=>'Área Total'],
				['class'=>'numeric','name'=>'Área Comum'],
				['class'=>'numeric','name'=>''],

			],
  			'modal'=>[
				'title'=>'Deseja Excluir o registro',
				'description'=>'<b>Tem certeza que deseja Excluir o registro?</b> </br>Caso este registro esteja atrelado a outros como empreendimentos,imoveis os mesmos tambem serao deletados.',
				'btns'=>[]
			],
			'formTitle'   =>'Formulario de Imoveis',
			'index.reservas.url'   =>'imoveis.index-reservas' ,
			'index.url'   =>'imoveis.index' ,
			'insert.url'  =>'imoveis.create' ,
			'store.url'   =>'imoveis.store',
			'photos.url'  =>'imoveis.photos',
			'upload.url'  =>'imoveis.upload',
			'crud.url'    =>'imoveis.crud',
			'destroy.url' =>'imoveis.destroy',
  	]; 
  	public  $btnsColumns   = 
  	[
		[
			'link'=>'imoveis.photos',
			'text'=>'<i class="fa  fa-camera"></i>',
			'sub'=>'empreendimento_id',
			'array'=>
			[
				'class'=>'btn btn-primary btn-xs tooltips',
				'data-original-title'=>'Adicionar imagens',
				'data-placement'=>"top",
				'data-toggle'=>"tooltip"
			]
		],  	
		[
			'link'=>'#myModal',
			'modal'=>'true',
			'text'=>'<i class="fa fa-trash-o"></i>',
			'data-url'=>'bairros.destroy',
			'array'=>
			[
				'class'=>'btn btn-danger btn-xs tooltips delModal',
				'data-original-title'=>'Tooltip on top',
				'data-placement'=>"top",
				'data-toggle'=>"modal"
			]
		],

	];


}
	