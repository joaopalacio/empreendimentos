<?php
use LaravelBook\Ardent\Ardent;
/**
 * Banco
 *
 * @property integer $id
 * @property string $Banco
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Ardent\Collection|\Empreendimento[] $empreendimentos
 * @method static \Illuminate\Database\Query\Builder|\Banco whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Banco whereBanco($value)
 * @method static \Illuminate\Database\Query\Builder|\Banco whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Banco whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Banco whereUrlClean($value)
 */
class Banco extends Ardent implements ModelInterface{
	protected $table = "bancos";
	protected $fillable = ['banco','cnpj','cedente','endereco','cep','cidade','uf','agencia','agenciaDv','conta','contaDV','carteira'];

	public $forceEntityHydrationFromInput = true;
	public $autoPurgeRedundantAttributes = true;

	public static $rules 		  = 
	[
		'banco'  					  => 'required|unique:bancos|between:2,20',
	];
		
	public static $customMessages = 
	[
		'required'                    => '- Este campo é obrigatorio.',
		'between'                     => '- Preencha o campo Banco com mais de 4 caracteres',    	
	];     
		
		
	public static $relationsData  = 
	[

  	];

  	public  $tableColumns  = ['banco','carteira','conta'];

  	public  $editColumns   = 
  	[
  		['id'=>'banco','name'=>'banco','lista'=>[''=>'Selecione o Banco','itau'=>'Banco Itau','santander'=>'Banco Santander','nossa_caixa'=>'Nossa Caixa Federal'],'campo'=>'Nome da Banco','minlength'=>'','type'=>'select','required'=>'','class'=>'','placeholder'=>''],
  		['id'=>'carteira','name'=>'carteira','campo'=>'Carteira','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>''],  		
  		['id'=>'cedente','name'=>'cedente','campo'=>'Cedente','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>''],
  		['id'=>'cnpj','name'=>'cnpj','campo'=>'CNPJ','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>''],
  		['id'=>'endereco','name'=>'endereco','campo'=>'Endereço Cedente','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>''],
  		['id'=>'cep','name'=>'cep','campo'=>'Cep Cedente','minlength'=>'','type'=>'text','required'=>'','class'=>'cep','placeholder'=>''],
  		['id'=>'cidade','name'=>'endereco','campo'=>'Endereço Cedente','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>''],  		
  		['id'=>'agencia','name'=>'agencia','campo'=>'Numero Agencia','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>''],
  		['id'=>'agenciaDv','name'=>'agenciaDv','campo'=>'Digito Agencia','minlength'=>'','type'=>'text','class'=>'','placeholder'=>''],
  		['id'=>'conta','name'=>'conta','campo'=>'Numero da Conta','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>''],
  		['id'=>'contaDv','name'=>'contaDv','campo'=>'Digito da Conta','minlength'=>'','type'=>'text','class'=>'','placeholder'=>'']
  	];


  	public  $settings      = 
  	[
  			'model'=>'Banco',
  			'title'=>'Lista de Bancos cadastradas',
			'thead'=>[
				
				['class'=>'','name'=>'Banco'],
				['class'=>'','name'=>'Carteira'],
				['class'=>'','name'=>'Conta'],
				['class'=>'numeric','name'=>'']
			],
  			'modal'=>[
				'title'=>'Deseja Excluir o registro',
				'description'=>'<b>Tem certeza que deseja Excluir o registro?</b> </br>Caso este registro esteja atrelado a outros como empreendimentos,imoveis os mesmos tambem serao deletados.',
				'btns'=>[]
			],
			'formTitle'   =>'Formulario de Bancos',
			'index.url'   =>'bancos.index' ,
			'insert.url'  =>'bancos.create' ,
			'store.url'   =>'bancos.store',
			'edit.url'    =>'bancos.edit',
			'update.url'  =>'bancos.update',
			'crud.url'    =>'bancos.crud',
			'destroy.url' =>'bancos.destroy',
  	]; 

  	public  $btnsColumns   = 
  	[		
		[
			'link'=>'bancos.edit',
			'text'=>'<i class="fa fa-pencil"></i>',
			'array'=>
			[
				'class'=>'btn btn-primary btn-xs tooltips',
				'data-original-title'=>'Editar registro',
				'data-placement'=>"top",
				'data-toggle'=>"tooltip"
			]
		],
		[
			'link'=>'#myModal',
			'modal'=>'true',
			'text'=>'<i class="fa fa-trash-o"></i>',
			'data-url'=>'bancos.destroy',
			'array'=>
			[
				'class'=>'btn btn-danger btn-xs tooltips delModal',
				'data-original-title'=>'Excluir registro',
				'data-placement'=>"top",
				'data-toggle'=>"modal"
			]
		],

	];


}