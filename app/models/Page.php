<?php
use LaravelBook\Ardent\Ardent;
/**
 * Page
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $keywords
 * @property string $description
 * @property string $og_image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Page whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereKeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereOgImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereUpdatedAt($value)
 */
class Page extends Ardent implements ModelInterface{
	protected $fillable = ['title','url','keywords','description','og_image'];
	protected $table = "pages";


 	public $autoHydrateEntityFromInput = true;
   	public $autoPurgeRedundantAttributes = true;

	public static $rules 		  = 
	[
		'title'  					  => 'required|between:4,20',
		'url'					  	  => 'required|between:1,600'
	];
		
	public static $customMessages = 
	[
		'required'                    => '- Este campo :attributes é obrigatorio.',
		'between'                     => '- Preencha o campo :attributes com mais de 4 caracteres',    	
	];     


  	public  $tableColumns  = ['id','title','url'];

  	public  $editColumns   = 
  	[
		['id'=>'title','name'=>'title','campo'=>'Titulo','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>''],
		['id'=>'url','name'=>'url','campo'=>'Url','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>''],
		['id'=>'keywords','name'=>'keywords','campo'=>'Palavra-chaves','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>''],	
		['id'=>'description','name'=>'description','campo'=>'Descrição','minlength'=>'','type'=>'textarea','required'=>'','class'=>'','placeholder'=>''],
  	];


  	public  $settings      = 
  	[
  			'model'=>'Paginas',
  			'title'=>'Lista de Paginas cadastradas',
			'thead'=>[
				['class' =>'numeric','name'=>'id'],
				['class' =>'','name'=>'Title'],				
				['class' =>'','name'=>'Url'],
				['class' =>'numeric','name'=>''],
			],
  			'modal'=>[
				'title'=>'Deseja Excluir o registro',
				'description'=>'<b>Tem certeza que deseja Excluir o registro?</b> </br>Caso este registro esteja atrelado a outros como empreendimentos,imoveis os mesmos tambem serao deletados.',
				'btns'=>[]
			],
			'formTitle'   =>'Formulario de Paginas',
			'index.url'   =>'site.index' ,
			'insert.url'  =>'site.create' ,
			'store.url'   =>'site.store',
			'edit.url'    =>'site.edit',
			'update.url'  =>'site.update',
			'crud.url'    =>'site.crud',
			'destroy.url' =>'site.destroy',
  	]; 

  	public  $btnsColumns   = 
  	[

		[
			'link'=>'site.edit',
			'text'=>'<i class="fa fa-pencil"></i>',
			'array'=>
			[
				'class'=>'btn btn-primary btn-xs tooltips',
				'data-original-title'=>'Tooltip on top',
				'data-placement'=>"top",
				'data-toggle'=>"tooltip"
			]
		],

	];




}