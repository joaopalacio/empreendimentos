<?php
use LaravelBook\Ardent\Ardent;
/**
 * Boleto
 *
 * @property integer $id
 * @property string $boleto
 * @property integer $proposta_id
 * @property string $banco
 * @property float $valor
 * @property string $vencimento
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Proposta $proposta
 * @method static \Illuminate\Database\Query\Builder|\Boleto whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Boleto whereBoleto($value)
 * @method static \Illuminate\Database\Query\Builder|\Boleto wherePropostaId($value)
 * @method static \Illuminate\Database\Query\Builder|\Boleto whereBanco($value)
 * @method static \Illuminate\Database\Query\Builder|\Boleto whereValor($value)
 * @method static \Illuminate\Database\Query\Builder|\Boleto whereVencimento($value)
 * @method static \Illuminate\Database\Query\Builder|\Boleto whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Boleto whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Boleto whereUpdatedAt($value)
 */
class Boleto extends Ardent implements ModelInterface{
	
	protected $fillable = ['boleto','banco','valor','vencimento','status'];
	protected $table = "boletos";

	public static $relationsData  = 
	[
		'proposta'                 => array(self::BELONGS_TO, 'Proposta'),
  	];
}