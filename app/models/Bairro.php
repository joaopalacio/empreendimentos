<?php
use LaravelBook\Ardent\Ardent;
/**
 * Bairro
 *
 * @property integer $id
 * @property string $bairros
 * @property string $url-clean
 * @property integer $cidade_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Bairro whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Bairro whereBairros($value)
 * @method static \Illuminate\Database\Query\Builder|\Bairro whereUrlClean($value)
 * @method static \Illuminate\Database\Query\Builder|\Bairro whereCidadeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Bairro whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Bairro whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Ardent\Collection|\Empreendimento[] $empreendimentos
 */
class Bairro extends Ardent implements ModelInterface{
	protected $table = "bairros";
	protected $fillable = ['cidade_id','bairro'];


 	public $autoHydrateEntityFromInput = true;
   	public $autoPurgeRedundantAttributes = true;

	public static $rules 		  = 
	[
		'bairros'  					  => 'required|between:3,50',
	];
		
	public static $customMessages = 
	[
		'required'                    => '- Este campo é :attribute obrigatorio.',
		'between'                     => '- Preencha o campo Bairro com mais de 3 caracteres',    	
	];     
		
	public static $sluggable      = 
	[
		'build_from'                  => 'bairros',
		'save_to'                     => 'url-clean',
	];	
		
	public static $relationsData  = 
	[
		'empreendimentos'             => array(self::HAS_MANY, 'Empreendimento', 'bairro_id'),
		'cidades'		              => array(self::BELONGS_TO, 'Cidade'),		
  	];

  	public  $tableColumns  = ['id','bairros'];

  	public  $editColumns   = 
  	[
		['id'=>'bairros','name'=>'bairros','campo'=>'Nome do Bairro','minlength'=>'','type'=>'text','required'=>'true','class'=>'','placeholder'=>''],
  	];


  	public  $settings      = 
  	[
  			'model'=>'Bairro',
  			'parents'=>'cidade',
  			'title'=>'Lista de Bairros cadastradas',
			'thead'=>[
				['class'=>'numeric','name'=>'id'],
				['class'=>'','name'=>'Bairro'],
				['class'=>'numeric','name'=>'']
			],
  			'modal'=>[
				'title'=>'Deseja Excluir o registro',
				'description'=>'<b>Tem certeza que deseja Excluir o registro?</b> </br>Caso este registro esteja atrelado a outros como empreendimentos,imoveis os mesmos tambem serao deletados.',
				'btns'=>[]
			],
			'formTitle'   =>'Formulario de Bairros',
			'index.url'   =>'bairros.index' ,
			'insert.url'  =>'bairros.create' ,
			'store.url'   =>'bairros.store',
			'edit.url'    =>'bairros.edit',
			'update.url'  =>'bairros.update',
			'crud.url'    =>'bairros.crud',
			'destroy.url' =>'bairros.destroy',
  	]; 
  	public  $btnsColumns   = 
  	[
		[
			'link'=>'#myModal',
			'modal'=>'true',
			'text'=>'<i class="fa fa-trash-o"></i>',
			'data-url'=>'bairros.destroy',
			'array'=>
			[
				'class'=>'btn btn-danger btn-xs tooltips delModal',
				'data-original-title'=>'Excluir registro',
				'data-placement'=>"top",
				'data-toggle'=>"modal"
			]
		],

	];


}
	