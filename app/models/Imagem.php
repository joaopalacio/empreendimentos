<?php
use LaravelBook\Ardent\Ardent;
/**
 * Imagem
 *
 * @property integer $id
 * @property integer $uses_id
 * @property string $uses_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $media
 * @method static \Illuminate\Database\Query\Builder|\Imagem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Imagem whereUsesId($value)
 * @method static \Illuminate\Database\Query\Builder|\Imagem whereUsesType($value)
 * @method static \Illuminate\Database\Query\Builder|\Imagem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Imagem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Imagem whereMedia($value)
 */
class UploadImage extends Ardent implements ModelInterface{
	protected $fillable = ['url'];
	protected $table = "images";
	
	public static $relationsData  = 
	[
		'uses'     	 => array(self::MORPH_TO),					
  	];    
}