<?php
use LaravelBook\Ardent\Ardent;
/**
 * Parcela
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $proposta_id
 * @property integer $numero
 * @property string $vencimento
 * @property float $valor
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Proposta $proposta
 * @property-read \User $cliente
 * @method static \Illuminate\Database\Query\Builder|\Parcela whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Parcela whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Parcela wherePropostaId($value)
 * @method static \Illuminate\Database\Query\Builder|\Parcela whereNumero($value)
 * @method static \Illuminate\Database\Query\Builder|\Parcela whereVencimento($value)
 * @method static \Illuminate\Database\Query\Builder|\Parcela whereValor($value)
 * @method static \Illuminate\Database\Query\Builder|\Parcela whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Parcela whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Parcela whereUpdatedAt($value)
 */
class Parcela extends Ardent implements ModelInterface{
	protected $fillable = ['numero','vencimento','valor','status'];
	protected $table = "parcelas";
 
	public static $relationsData  = 
	[				
		'contrato' => array(self::BELONGS_TO, 'Contrato','id'),	
		'boletos'  => array(self::HAS_MANY, 'Boleto', 'parcela_id'),		
  	];			
}