<?php
use LaravelBook\Ardent\Ardent;
/**
 * RegrasProposta
 *
 * @property integer $id
 * @property integer $imovel_id
 * @property float $desconto_padrao
 * @property float $valor_imovel
 * @property float $entrada_minima
 * @property float $chaves_maxima
 * @property float $porcent_financiamento
 * @property integer $prazo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\RegrasProposta whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\RegrasProposta whereImovelId($value)
 * @method static \Illuminate\Database\Query\Builder|\RegrasProposta whereDescontoPadrao($value)
 * @method static \Illuminate\Database\Query\Builder|\RegrasProposta whereValorImovel($value)
 * @method static \Illuminate\Database\Query\Builder|\RegrasProposta whereEntradaMinima($value)
 * @method static \Illuminate\Database\Query\Builder|\RegrasProposta whereChavesMaxima($value)
 * @method static \Illuminate\Database\Query\Builder|\RegrasProposta wherePorcentFinanciamento($value)
 * @method static \Illuminate\Database\Query\Builder|\RegrasProposta wherePrazo($value)
 * @method static \Illuminate\Database\Query\Builder|\RegrasProposta whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\RegrasProposta whereUpdatedAt($value)
 * @property-read \imovel $imovel
 * @property-read \Illuminate\Database\Ardent\Collection|\RegraIntermediaria[] $intermediarias
 */
class RegrasProposta extends Ardent implements ModelInterface{
	protected $fillable = ['desconto_padrao','valor_imovel','entrada_minima','chaves_maxima','porcent_financiamento','prazo'];
	protected $table = "regras_propostas";


	public static $relationsData  = 
	[		
		'intermediarias' => array(self::HAS_MANY, 'RegraIntermediaria', 'foreignKey' => 'regras_id' ),	
		'imovel'         => array(self::BELONGS_TO, 'Imovel'),									
  	];	


}