<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use LaravelBook\Ardent\Ardent;
/**
 * Corretor
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property integer $role_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Role $role
 * @property-read \Illuminate\Database\Ardent\Collection|\Proposta[] $propostas_enviadas
 * @property-read \Illuminate\Database\Ardent\Collection|\Proposta[] $propostas_solicitada
 * @property-read \Illuminate\Database\Ardent\Collection|\Reserva[] $reservas_enviadas
 * @property-read \Illuminate\Database\Ardent\Collection|\Reserva[] $reservas_solicitada
 * @method static \Illuminate\Database\Query\Builder|\Corretor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Corretor whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Corretor wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\Corretor whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Corretor whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Corretor whereUpdatedAt($value)
 * @property string $name
 * @property string $cpf
 * @method static \Illuminate\Database\Query\Builder|\Corretor whereName($value) 
 * @method static \Illuminate\Database\Query\Builder|\Corretor whereCpf($value) 
 * @method static \User corretores() 
 */


class Corretor extends Ardent implements UserInterface, RemindableInterface,ModelInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
 	public $autoHydrateEntityFromInput = true;
   	public $autoPurgeRedundantAttributes = true;

	public static $rules 		  = 
	[
		'name' => 'required|between:4,200',
		'cpf'    => 'required',
		'email'  =>' email|required'		
	];
		
	public static $customMessages = 
	[
		'name.required'                    => '- Nome do corretor é obrigatorio.',
		'cpf.required'                    => '- CPF do corretor é obrigatorio e tambem deve ser um cpf valido.',
		'email.required'                    => '- Email do corretor é obrigatorio.',
		'email.email'                    => '- Preencha um email valido',
		'between'                     => '- Preencha o campo nome do corretor com mais de 4 caracteres e menos de 200',    	
	];     
		
		
	public static $relationsData  = 
	[
		'role'                 => array(self::BELONGS_TO, 'Role'),
		'imoveis'     		   => array(self::BELONGS_TO_MANY, 'Imovel','pivotKeys'=>['user_id','imovel_id'],'table' => 'imovel_corretor'),	
		'propostas_enviadas'   => array(self::HAS_MANY, 'Proposta', 'corretor_id'),
		'reservas_enviadas'    => array(self::HAS_MANY, 'Reserva', 'corretor_id'),
  	];

  	public  $tableColumns  = ['id','name','email','cpf'];

  	public  $editColumns   = 
  	[
  		'Corretor' => 
  		[
	 		['id'=>'name','name'=>'name','campo'=>'Nome do Corretor','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'Sr. Jose Almeida'],
			['id'=>'email','name'=>'email','campo'=>'Email do Corretor','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'corretor@corretor.com.br'],
			['id'=>'cpf','name'=>'cpf','campo'=>'CPF do Corretor','minlength'=>'','type'=>'text','required'=>'','class'=>'mask_cpf cpf','placeholder'=>'000.000.000-00'], 		
  		]

  	];


  	public  $settings      = 
  	[
  		'Corretor' => 
  		[  	
  			'model'=>'Corretor',
  			'title'=>'Lista de Corretores cadastrados',
			'thead'=>[
				['class'=>'numeric','name'=>'id'],
				['class'=>'','name'=>'Corretor'],
				['class'=>'','name'=>'Email'],
				['class'=>'','name'=>'CPF'],								
				['class'=>'numeric','name'=>'']
			],
  			'modal'=>[
				'title'=>'Deseja Excluir o registro',
				'description'=>'<b>Tem certeza que deseja Excluir o registro?</b> </br>Caso este registro esteja atrelado a outros como empreendimentos,imoveis os mesmos tambem serao deletados.',
				'btns'=>[]
			],
			'formTitle'   =>'Formulario de Corretores',
			'index.url'   =>'corretores.index' ,
			'insert.url'  =>'corretores.create' ,
			'store.url'   =>'corretores.store',
			'edit.url'    =>'corretores.edit',
			'update.url'  =>'corretores.update',
			'crud.url'    =>'corretores.crud',
			'destroy.url' =>'corretores.destroy',
		]
  	]; 

  	public  $btnsColumns   = 
  	[
  		'Corretor' => 
  		[  	
			[
				'link'=>'corretores.edit',
				'text'=>'<i class="fa fa-pencil"></i>',
				'array'=>
				[
					'class'=>'btn btn-primary btn-xs tooltips',
					'data-original-title'=>'Tooltip on top',
					'data-placement'=>"top",
					'data-toggle'=>"tooltip"
				]
			],
			[
				'link'=>'#myModal',
				'modal'=>'true',
				'text'=>'<i class="fa fa-trash-o"></i>',
				'data-url'=>'corretores.destroy',
				'array'=>
				[
					'class'=>'btn btn-danger btn-xs tooltips delModal',
					'data-original-title'=>'Tooltip on top',
					'data-placement'=>"top",
					'data-toggle'=>"modal"
				]
			],
		]

	];
	public function scopeCorretores($query){
		$roles = Role::where('name','=','Corretor')->first();
		return $query->where('role_id','=',$roles->id);
	}



	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getRememberToken(){

	}

	public function setRememberToken($value){

	}

	public function getRememberTokenName(){

	}


}