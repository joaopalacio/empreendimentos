<?php
use LaravelBook\Ardent\Ardent;
/**
 * Cidade
 *
 * @property integer $id
 * @property string $cidade
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Ardent\Collection|\Empreendimento[] $empreendimentos
 * @method static \Illuminate\Database\Query\Builder|\Cidade whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cidade whereCidade($value)
 * @method static \Illuminate\Database\Query\Builder|\Cidade whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Cidade whereUpdatedAt($value)
 * @property string $url-clean
 * @method static \Illuminate\Database\Query\Builder|\Cidade whereUrlClean($value)
 */
class Cidade extends Ardent implements ModelInterface{
	protected $table = "cidades";
	protected $fillable = ['cidade'];


 	public $autoHydrateEntityFromInput = true;
   	public $autoPurgeRedundantAttributes = true;

	public static $rules 		  = 
	[
		'cidade'  					  => 'required|between:4,20',
	];
		
	public static $customMessages = 
	[
		'required'                    => '- Este campo é obrigatorio.',
		'between'                     => '- Preencha o campo Cidade com mais de 4 caracteres',    	
	];     
		
	public static $sluggable      = 
	[
		'build_from'                  => 'cidade',
		'save_to'                     => 'url-clean',
	];	
		
	public static $relationsData  = 
	[
		'bairros'                     => array(self::HAS_MANY, 'Bairro', 'cidade_id'),
		'empreendimentos'             => array(self::HAS_MANY, 'Empreendimento', 'foreignKey'=>'cidade_id'),
  	];

  	public  $tableColumns  = ['id','cidade'];

  	public  $editColumns   = 
  	[
  		['id'=>'cidade','name'=>'cidade','campo'=>'Nome da Cidade','minlength'=>'','type'=>'text','required'=>'','class'=>'','placeholder'=>'Nome da cidade']
  	];


  	public  $settings      = 
  	[
  			'model'=>'Cidade',
  			'title'=>'Lista de Cidades cadastradas',
			'thead'=>[
				['class'=>'numeric','name'=>'id'],
				['class'=>'','name'=>'Cidade'],
				['class'=>'numeric','name'=>'']
			],
  			'modal'=>[
				'title'=>'Deseja Excluir o registro',
				'description'=>'<b>Tem certeza que deseja Excluir o registro?</b> </br>Caso este registro esteja atrelado a outros como empreendimentos,imoveis os mesmos tambem serao deletados.',
				'btns'=>[]
			],
			'formTitle'   =>'Formulario de Cidades',
			'index.url'   =>'cidades.index' ,
			'insert.url'  =>'cidades.create' ,
			'children.url'=>'bairros.index',
			'store.url'   =>'cidades.store',
			'edit.url'    =>'cidades.edit',
			'update.url'  =>'cidades.update',
			'crud.url'    =>'cidades.crud',
			'destroy.url' =>'cidades.destroy',
  	]; 

  	public  $btnsColumns   = 
  	[
		[
			'link'=>'bairros.index',
			'text'=>'<i class="fa  fa-home"></i>',
			'array'=>
			[
				'class'=>'btn btn-warning btn-xs tooltips',
				'data-original-title'=>'Lista de Bairros desta Cidade',
				'data-placement'=>"top",
				'data-toggle'=>"tooltip"
			]
		],
		[
			'link'=>'cidades.edit',
			'text'=>'<i class="fa fa-pencil"></i>',
			'array'=>
			[
				'class'=>'btn btn-primary btn-xs tooltips',
				'data-original-title'=>'Editar registro',
				'data-placement'=>"top",
				'data-toggle'=>"tooltip"
			]
		],
		[
			'link'=>'#myModal',
			'modal'=>'true',
			'text'=>'<i class="fa fa-trash-o"></i>',
			'data-url'=>'cidades.destroy',
			'array'=>
			[
				'class'=>'btn btn-danger btn-xs tooltips delModal',
				'data-original-title'=>'Excluir registro',
				'data-placement'=>"top",
				'data-toggle'=>"modal"
			]
		],

	];


}