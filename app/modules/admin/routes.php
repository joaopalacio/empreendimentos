<?php

//EVENTS
\Event::subscribe(new \ClienteEventHandler);
\Event::subscribe(new \CorretorEventHandler);
\Event::subscribe(new \SistemaEventHandler);


Route::get('/login', function(){	
	return Redirect::route('site.index');
});
/*
Route::get('/login/Cliente', function(){
	$Role = Role::where('name','=','Cliente')->first();
	$user = User::where('role_id','=',$Role->id)->first();
	Auth::login($user);	
	return Redirect::route('cliente.index');
});

Route::get('/login/Admin', function(){
	$Role = Role::where('name','=','Administrador')->first();
	$user = User::where('role_id','=',$Role->id)->first();
	Auth::login($user);	
	return Redirect::route('admin.index');
});
Route::get('/login/Corretor', function(){
	$Role = Role::where('name','=','Corretor')->first();
	$user = User::where('role_id','=',$Role->id)->first();
	Auth::login($user);	
	return Redirect::route('admin.index');
});

*/



Route::post('/login', ['as'=>'login.made','uses' => 'LoginController@logar']);	
Route::group(array('before' => 'auth'), function()
{

	/*
	 * ==========================================================================================================================
	 * ========== > ROUTES ADMIN 
	 * ==========================================================================================================================
	 */
	Route::get('/admin/', array('as'                                                =>'admin.index','uses' => 'App\Modules\Admin\Controllers\Admin\DashboardController@index'));	
	Route::get('/admin/cidades', array('as'                                         =>'cidades.index','uses' => 'App\Modules\Admin\Controllers\Admin\CidadesController@index'));
	Route::get('/admin/cidades/create', array('as'                                  =>'cidades.create','uses' => 'App\Modules\Admin\Controllers\Admin\CidadesController@create'));	
	Route::get('/admin/cidades/edit/{id}', array('as'                               =>'cidades.edit','uses' => 'App\Modules\Admin\Controllers\Admin\CidadesController@edit'));
	Route::put('/admin/cidades/update/{id}', array('as'                             =>'cidades.update','uses' => 'App\Modules\Admin\Controllers\Admin\CidadesController@update'));
	Route::get('/admin/cidades/destroy/{id}', array('as'                            =>'cidades.destroy','uses' => 'App\Modules\Admin\Controllers\Admin\CidadesController@destroy'));				
	Route::post('/admin/cidades/store', array('as'                                  =>'cidades.store','uses' => 'App\Modules\Admin\Controllers\Admin\CidadesController@store'));		
	//Bairros
	Route::get('/admin/cidades/bairros/{id}', array('as'                        	=>'bairros.index','uses' => 'App\Modules\Admin\Controllers\Admin\BairrosController@index'));	
	Route::get('/admin/cidades/bairros/{id}/create_bairros/', array('as'        	=>'bairros.create','uses' => 'App\Modules\Admin\Controllers\Admin\BairrosController@create'));
	Route::post('/admin/cidades/bairros/{id}/store_bairros/', array('as'        	=>'bairros.store','uses' => 'App\Modules\Admin\Controllers\Admin\BairrosController@store'));	
	Route::get('/admin/cidades/{cidade_id}/bairros/destroy/{id}', 					array('as' 	=>'bairros.destroy','uses' => 'App\Modules\Admin\Controllers\Admin\BairrosController@destroy'));	
	//Bancos
	Route::get('/admin/bancos', array('as'                                         =>'bancos.index','uses' => 'App\Modules\Admin\Controllers\Admin\BancosController@index'));
	Route::get('/admin/bancos/create', array('as'                                  =>'bancos.create','uses' => 'App\Modules\Admin\Controllers\Admin\BancosController@create'));	
	Route::get('/admin/bancos/edit/{id}', array('as'                               =>'bancos.edit','uses' => 'App\Modules\Admin\Controllers\Admin\BancosController@edit'));
	Route::put('/admin/bancos/update/{id}', array('as'                             =>'bancos.update','uses' => 'App\Modules\Admin\Controllers\Admin\BancosController@update'));
	Route::get('/admin/bancos/destroy/{id}', array('as'                            =>'bancos.destroy','uses' => 'App\Modules\Admin\Controllers\Admin\BancosController@destroy'));				
	Route::post('/admin/bancos/store', array('as'                                  =>'bancos.store','uses' => 'App\Modules\Admin\Controllers\Admin\BancosController@store'));					
	//Indicadores
	Route::get('/admin/indicadores', array('as'                                     =>'indicadores.index','uses' => 'App\Modules\Admin\Controllers\Admin\IndicadoresController@index'));
	Route::get('/admin/indicadores/create', array('as'                              =>'indicadores.create','uses' => 'App\Modules\Admin\Controllers\Admin\IndicadoresController@create'));	
	Route::get('/admin/indicadores/edit/{id}', array('as'                           =>'indicadores.edit','uses' => 'App\Modules\Admin\Controllers\Admin\IndicadoresController@edit'));
	Route::put('/admin/indicadores/update/{id}', array('as'                         =>'indicadores.update','uses' => 'App\Modules\Admin\Controllers\Admin\IndicadoresController@update'));
	Route::get('/admin/indicadores/destroy/{id}', array('as'                        =>'indicadores.destroy','uses' => 'App\Modules\Admin\Controllers\Admin\IndicadoresController@destroy'));				
	Route::post('/admin/indicadores/store', array('as'                              =>'indicadores.store','uses' => 'App\Modules\Admin\Controllers\Admin\IndicadoresController@store'));		
	//IndicadoresValores
	Route::get('/admin/indicadores/valores/{id}', array('as'                        =>'valores.index','uses' => 'App\Modules\Admin\Controllers\Admin\ValoresController@index'));	
	Route::get('/admin/indicadores/valores/{id}/create_valores/', array('as'        =>'valores.create','uses' => 'App\Modules\Admin\Controllers\Admin\ValoresController@create'));
	Route::post('/admin/indicadores/valores/{id}/store_valores/', array('as'        =>'valores.store','uses' => 'App\Modules\Admin\Controllers\Admin\ValoresController@store'));	
	Route::get('/admin/indicadores/valores/{id}/destroy/{indicador_id}', array('as' =>'valores.destroy','uses' => 'App\Modules\Admin\Controllers\Admin\ValoresController@destroy'));			
	//FAQ
	Route::get('/admin/faqs', array('as'                                            =>'faq.index','uses' => 'App\Modules\Admin\Controllers\Admin\FaqController@index'));
	Route::get('/admin/faqs/create', array('as'                                     =>'faq.create','uses' => 'App\Modules\Admin\Controllers\Admin\FaqController@create'));	
	Route::get('/admin/faqs/edit/{id}', array('as'                                  =>'faq.edit','uses' => 'App\Modules\Admin\Controllers\Admin\FaqController@edit'));
	Route::put('/admin/faqs/update/{id}', array('as'                                =>'faq.update','uses' => 'App\Modules\Admin\Controllers\Admin\FaqController@update'));
	Route::get('/admin/faqs/destroy/{id}', array('as'                               =>'faq.destroy','uses' => 'App\Modules\Admin\Controllers\Admin\FaqController@destroy'));				
	Route::post('/admin/faqs/store', array('as'                                     =>'faq.store','uses' => 'App\Modules\Admin\Controllers\Admin\FaqController@store'));
	

	Route::get('/cliente/', array('as'                                              =>'admin.cliente','uses' => 'App\Modules\Admin\Controllers\Admin\DashboardController@index'));

	
	//Corretores CRUD
	Route::get('/admin/corretores', array('as'                                      =>'corretores.index','uses' => 'App\Modules\Admin\Controllers\Admin\CorretoresController@index'));
	Route::get('/admin/corretores/create', array('as'                               =>'corretores.create','uses' => 'App\Modules\Admin\Controllers\Admin\CorretoresController@create'));	
	Route::get('/admin/corretores/edit/{id}', array('as'                            =>'corretores.edit','uses' => 'App\Modules\Admin\Controllers\Admin\CorretoresController@edit'));
	Route::put('/admin/corretores/update/{id}', array('as'                          =>'corretores.update','uses' => 'App\Modules\Admin\Controllers\Admin\CorretoresController@update'));
	Route::get('/admin/corretores/destroy/{id}', array('as'                         =>'corretores.destroy','uses' => 'App\Modules\Admin\Controllers\Admin\CorretoresController@destroy'));				
	Route::post('/admin/corretores/store', array('as'                               =>'corretores.store','uses' => 'App\Modules\Admin\Controllers\Admin\CorretoresController@store'));	

				
	//Sites
	Route::get('/admin/site', array('as'                                            =>'site.index','uses' => 'App\Modules\Admin\Controllers\Admin\SiteController@index'));
	Route::get('/admin/site/create', array('as'                                     =>'site.create','uses' => 'App\Modules\Admin\Controllers\Admin\SiteController@create'));	
	Route::get('/admin/site/edit/{id}', array('as'                                  =>'site.edit','uses' => 'App\Modules\Admin\Controllers\Admin\SiteController@edit'));
	Route::put('/admin/site/update/{id}', array('as'                                =>'site.update','uses' => 'App\Modules\Admin\Controllers\Admin\SiteController@update'));
	Route::get('/admin/site/destroy/{id}', array('as'                               =>'site.destroy','uses' => 'App\Modules\Admin\Controllers\Admin\SiteController@destroy'));				
	Route::post('/admin/site/store', array('as'                                     =>'site.store','uses' => 'App\Modules\Admin\Controllers\Admin\SiteController@store'));

	//Settings
	Route::get('/admin/settings', array('as'                                        =>'settings.index','uses' => 'App\Modules\Admin\Controllers\Admin\SettingsController@index'));
	Route::post('/admin/settings', array('as'                                       =>'settings.update','uses' => 'App\Modules\Admin\Controllers\Admin\SettingsController@update'));	

	//banners
	Route::get('/admin/banners', array('as'  										=>'settings.banners','uses' => 'App\Modules\Admin\Controllers\Admin\BannerController@photos'));
	//ajax upload banners
	Route::post('/admin/banners/upload/', array('as'  =>'banners.upload','uses' 	=> 'App\Modules\Admin\Controllers\Admin\BannerController@upload'));
	//ajax delete banners
	Route::post('/admin/banners/destroy/photo/', array('as'  =>'banners.images.delete','uses' => 'App\Modules\Admin\Controllers\Admin\BannerController@destroyImage'));
	


	//Empreendimentos 
	Route::get('/admin/empreendimentos', array('as'                                 =>'empreendimentos.index','uses' => 'App\Modules\Admin\Controllers\Admin\EmpreendimentosController@index'));
	Route::get('/admin/empreendimentos/create', array('as'                          =>'empreendimentos.create','uses' => 'App\Modules\Admin\Controllers\Admin\EmpreendimentosController@create'));	
	Route::get('/admin/empreendimentos/edit/{id}', array('as'                       =>'empreendimentos.edit','uses' => 'App\Modules\Admin\Controllers\Admin\EmpreendimentosController@edit'));
	Route::put('/admin/empreendimentos/update/{id}', array('as'                     =>'empreendimentos.update','uses' => 'App\Modules\Admin\Controllers\Admin\EmpreendimentosController@update'));
	Route::get('/admin/empreendimentos/destroy/{id}', array('as'                    =>'empreendimentos.destroy','uses' => 'App\Modules\Admin\Controllers\Admin\EmpreendimentosController@destroy'));				
	Route::get('/admin/empreendimentos/{id}/photos/', array('as'  					=>'empreendimentos.photos','uses' => 'App\Modules\Admin\Controllers\Admin\EmpreendimentosController@photos'));

	//ajax upload empreendimentos
	Route::post('/admin/empreendimentos/{id}/upload/', array('as'  =>'empreendimentos.upload','uses' => 'App\Modules\Admin\Controllers\Admin\EmpreendimentosController@upload'));
	//ajax delete empreendimentos
	Route::post('/admin/empreendimentos/{id}/destroy/photo/', array('as'  =>'empreendimentos.images.delete','uses' => 'App\Modules\Admin\Admin\Controllers\EmpreendimentosController@destroyImage'));
		

	Route::post('/admin/empreendimentos/store', array('as'                          =>'empreendimentos.store','uses' => 'App\Modules\Admin\Controllers\Admin\EmpreendimentosController@store'));
	Route::get('/admin/empreendimentos/{empreendimento_id}/estagios', array('as'    =>'estagio.index','uses' => 'App\Modules\Admin\Controllers\Admin\EstagioController@index'));
	//Imoveis 
	Route::get('/admin/empreendimentos/{empreendimento_id}/imoveis', array('as'              =>'imoveis.index','uses' => 'App\Modules\Admin\Controllers\Admin\ImoveisController@index'));
	Route::get('/admin/empreendimentos/{empreendimento_id}/imoveis/tipo/{id}', array('as'    =>'imoveis.tipo','uses' => 'App\Modules\Admin\Controllers\Admin\ImoveisController@lista'));
	Route::get('/admin/empreendimentos/{empreendimento_id}/imoveis/create', array('as'       =>'imoveis.create','uses' => 'App\Modules\Admin\Controllers\Admin\ImoveisController@create'));	
	Route::get('/admin/empreendimentos/{empreendimento_id}/imoveis/photos/{id}', array('as'  =>'imoveis.photos','uses' => 'App\Modules\Admin\Controllers\Admin\ImoveisController@photos'));
	
	//ajax upload imoveis
	Route::post('/admin/empreendimentos/imoveis/{id}/upload/', array('as'  =>'imoveis.upload','uses' => 'App\Modules\Admin\Controllers\Admin\ImoveisController@upload'));
	//ajax delete images
	Route::post('/admin/empreendimentos/imoveis/{id}/destroy/photo/', array('as'  =>'imoveis.images.delete','uses' => 'App\Modules\Admin\Controllers\Admin\ImoveisController@destroyImage'));


	Route::get('/admin/empreendimentos/{empreendimento_id}/imoveis/destroy/{id}', array('as' =>'imoveis.destroy','uses' => 'App\Modules\Admin\Controllers\Admin\ImoveisController@destroy'));				
	Route::post('/admin/empreendimentos/{empreendimento_id}/imoveis/store', array('as'       =>'imoveis.store','uses' => 'App\Modules\Admin\Controllers\Admin\ImoveisController@store'));

	Route::get('/admin/empreendimentos/{empreendimento_id}/imoveis/{id}/intermediarias', array('as' =>'imoveis.intermediarias','uses' => 'App\Modules\Admin\Controllers\Admin\IntermediariasController@index'));
	Route::get('/admin/imoveis', array('as'     =>'corretor.imoveis','uses' => 'App\Modules\Admin\Controllers\Corretor\ImoveisController@imoveisDisponiveis'));

	Route::get('/admin/reservas/', array('as'     =>'admin.lista.reserva','uses' => 'App\Modules\Admin\Controllers\Admin\ReservasController@admin'));
	Route::get('/admin/reservas/documentos/{id}', array('as'     =>'admin.reserva.documentos','uses' => 'App\Modules\Admin\Controllers\Admin\FormularioController@show'));
	
	Route::get('/admin/propostas/', array('as'    				=>'admin.lista.propostas','uses' => 'App\Modules\Admin\Controllers\Admin\PropostasController@admin'));	
	Route::get('/admin/propostas/documentos/{id}', array('as'   =>'admin.proposta.informacoes','uses' => 'App\Modules\Admin\Controllers\Admin\PropostasController@adminDocumento'));	

	Route::get('/admin/contratos/', array('as'    				=>'admin.lista.contratos','uses' => 'App\Modules\Admin\Controllers\Admin\ContratosController@admin'));	
	Route::get('/admin/contratos/baixa/{id}', array('as'		=>'admin.contratos.receber','uses' => 'App\Modules\Admin\Controllers\Admin\ContratosController@baixa'));	
	Route::get('/admin/contratos/darbaixa/{id}', array('as'		=>'admin.contratos.darbaixa','uses' => 'App\Modules\Admin\Controllers\Admin\ContratosController@darbaixa'));		
	
	Route::get('/admin/boletos', array('as'   =>'admin.boleto','uses' => 'App\Modules\Admin\Controllers\Admin\DashboardController@adminBoleto'));	

	Route::get('/admin/arquivo/retorno', array('as'   =>'admin.boleto','uses' => 'App\Modules\Admin\Controllers\Admin\FinanceiroController@retornoBoleto'));	
	Route::post('/admin/arquivo/retorno', array('as'   =>'admin.financeiro.upload','uses' => 'App\Modules\Admin\Controllers\Admin\FinanceiroController@gravaRetorno'));	



	/*
	 * =========================================================================================================
	 * ======================================+CORRETORES+=======================================================
	 * =========================================================================================================
	 */

	Route::get('/corretor/', array('as'                       =>'corretor.index','uses' => 'App\Modules\Admin\Controllers\Corretor\DashboardController@index'));	
	Route::get('/admin/corretor/reservas/', array('as'         =>'corretor.lista.reserva','uses' => 'App\Modules\Admin\Controllers\Corretor\ReservasController@corretor'));
	Route::post('/admin/store_reserva/', array('as'    		   =>'corretor.store.reserva','uses' => 'App\Modules\Admin\Controllers\Corretor\ReservasController@store_preReserva'));
	Route::get('/admin/corretor/proposta/{id}', array('as'     =>'corretor.make.proposta','uses' => 'App\Modules\Admin\Controllers\Corretor\PropostasController@make'));
	Route::post('/admin/corretor/proposta/{id}', array('as'     =>'corretor.store.proposta','uses' => 'App\Modules\Admin\Controllers\Corretor\PropostasController@store'));
	Route::get('/admin/minhas_propostas', array('as'     =>'corretor.lista.proposta','uses' => 'App\Modules\Admin\Controllers\Corretor\PropostasController@corretor'));
	Route::get('/admin/proposta/destroy/id/{id}', array('as'     =>'corretor.proposta.destroy','uses' => 'App\Modules\Admin\Controllers\Corretor\PropostasController@destroy'));

	//Clientes CRUD
	Route::get('/admin/clientes', array('as'                                      	=>'clientes.index','uses' => 'App\Modules\Admin\Controllers\Corretor\ClientController@index'));
	Route::get('/admin/clientes/create', array('as'                               	=>'clientes.create','uses' => 'App\Modules\Admin\Controllers\Corretor\ClientController@create'));	
	Route::get('/admin/clientes/edit/{id}', array('as'                            	=>'clientes.edit','uses' => 'App\Modules\Admin\Controllers\Corretor\ClientController@edit'));
	Route::put('/admin/clientes/update/{id}', array('as'                          	=>'clientes.update','uses' => 'App\Modules\Admin\Controllers\Corretor\ClientController@update'));
	Route::get('/admin/clientes/destroy/{id}', array('as'                         	=>'clientes.destroy','uses' => 'App\Modules\Admin\Controllers\Corretor\ClientController@destroy'));				
	Route::post('/admin/clientes/store', array('as'                               	=>'clientes.store','uses' => 'App\Modules\Admin\Controllers\Corretor\ClientController@store'));	
	//Fichas clientes
	Route::get('/admin/clientes/{id}/formulario/Proposta', array('as'               =>'clientes.ficha','uses' => 'App\Modules\Admin\Controllers\Corretor\FormularioController@ficha'));
	Route::get('/admin/clientes/{id}/formulario/Conjuge', 		array('as'        	=>'clientes.ficha.conjuge','uses' => 'App\Modules\Admin\Controllers\Corretor\FormularioController@ficha'));
	Route::post('/admin/clientes/{id}/formulario/', array('as'            	 		=>'formulario.store','uses' => 'App\Modules\Admin\Controllers\Corretor\FormularioController@store'));

	/*
	 * =========================================================================================================
	 * ======================================+CLIENTES+=========================================================
	 * =========================================================================================================
	 */

	//Route::get('/admin/', array('as'                                                =>'cliente.index','uses' => 'App\Modules\Admin\Controllers\Cliente\DashboardController@index'));	

	Route::get('/cliente/pagamentos', array('as'     =>'clientes.pagamentos','uses' => 'App\Modules\Admin\Controllers\Corretor\FinanceiroController@lista_boletos'));
	//seta a sessao com o imovel / proposta
	Route::get('/cliente/escolhe/proposta/{id}', array('as'     =>'clientes.escolhe.imovel','uses' => 'App\Modules\Admin\Controllers\Cliente\DashboardController@escolhe_proposta'));
	Route::get('/cliente/parcelas', array('as'     =>'clientes.parcelas','uses' => 'App\Modules\Admin\Controllers\Cliente\FinanceiroController@parcelas'));
	Route::get('/cliente/boleto/{chave}', array('as'     =>'clientes.boleto.make','uses' => 'App\Modules\Admin\Controllers\Cliente\FinanceiroController@boleto'));



	//DASHBOARD
	Route::get('/admin/ajax/dashboard/reserva/', array('as' =>'ajax.dashboard.reserva','uses' => 'App\Modules\Admin\Controllers\Admin\DashboardController@dash_reservas'));	



});


/*
 * =========================================================================================================
 * ======================================+AJAX+=============================================================
 * =========================================================================================================
 */
//AJAX set status of empreendimentos	
Route::post('/admin/ajax/set/status/empreendimento/', array('as' =>'ajax.mudaStatus','uses' => 'App\Modules\Admin\Controllers\Admin\AjaxController@setStatus'));

//Ajax set estagios de um empreendimento
Route::post('/admin/ajax/set/estagios', array('as'                                 =>'ajax.setEstagio','uses' => 'App\Modules\Admin\Controllers\Admin\AjaxController@setEstagio'));

//Ajax para pegar todos clientes deste corretor
Route::post('/admin/ajax/get/clientes', array('as'                                 =>'ajax.corretores.get.clientes','uses' => 'App\Modules\Admin\Controllers\Admin\AjaxController@getClientes'));	
//Ajax para pegar todos clientes deste corretor
Route::post('/admin/ajax/set/clientes', array('as'                                 =>'ajax.corretores.set.clientes','uses' => 'App\Modules\Admin\Controllers\Admin\AjaxController@setClientes'));	

Route::post('/admin/ajax/set/empreendimentos/destaque', array('as'=>'ajax.set.empreendimento.destaque','uses' => 'App\Modules\Admin\Controllers\Admin\AjaxController@setEmpreendimentoDestaque'));

//Ajax para setar o status de uma reserva
Route::post('/admin/ajax/set/reserva/status/', array('as'                          =>'ajax.reservas.status','uses' => 'App\Modules\Admin\Controllers\Admin\AjaxController@setReserva'));	

//Ajax para setar o status de uma reserva
Route::post('/admin/ajax/set/proposta/status/', array('as'                          =>'ajax.propostas.status','uses' => 'App\Modules\Admin\Controllers\Admin\AjaxController@setProposta'));	



//Ajax pegar os bairros de uma cidade
Route::get('/admin/ajax/get/bairros', array('as'                                 =>'ajax.getBairros','uses' => 'App\Modules\Admin\Controllers\Admin\AjaxController@getBairros'));	
	


	/*
	 * =========================================================================================================
	 * ======================================+VIEW COMPOSER+====================================================
	 * =========================================================================================================
	 */

//COMPOSER
View::creator('admin::includes.profile', function($view)
{
    $view->with('count', User::count());
});

//COMPOSER
View::creator('admin::layouts.email', function($view)
{
	$view->with('data', Configuracao::first());
});


//COMPOSER
View::creator('admin::includes.menu', function($view)
{

	$User = User::with('role')->find(Auth::user()->id);
	if ($User->role->name=='Administrador'){
		$menu = [
			['text'=>'Home','url'=>'/admin','icon'=>'fa-home'],
			['text'=>'Sistema','url'=>'','icon'=>'fa-desktop',
				'submenu'=>[
					['text'=>'Indicadores','url'=>'/admin/indicadores','icon'=>'fa-arrow-circle-o-up'],
					['text'=>'Corretores','url'=>'/admin/corretores','icon'=>' fa-users'],
					['text'=>'Cidades','url'=>'/admin/cidades','icon'=>'fa-map-marker'],
					['text'=>'FAQ','url'=>'/admin/faqs','icon'=>' fa-question'],
					['text'=>'Site SEO','url'=>'/admin/site','icon'=>'fa-keyboard-o'],
					['text'=>'Banners','url'=>'/admin/banners','icon'=>'fa-camera'],
					['text'=>'Configurações','url'=>'/admin/settings','icon'=>'fa-gear'],
					['text'=>'Bancos','url'=>'/admin/bancos','icon'=>'fa-dollar'],
				]
			],
			['text'=>'Empreendimentos','url'=>'/admin/empreendimentos','icon'=>'fa-building-o'],			
			['text'=>'Reservas','url'=>'/admin/reservas','icon'=>' fa-check-circle-o'],
			['text'=>'Propostas','url'=>'/admin/propostas','icon'=>' fa-files-o'],
			['text'=>'Contratos','url'=>'/admin/contratos','icon'=>'  fa-shopping-cart'],
			['text'=>'Financeiro','url'=>'','icon'=>'fa-money',
				'submenu'=>[
					['text'=>'Arquivo retorno','url'=>'/admin/arquivo/retorno','icon'=>'fa-check-square-o'],
					['text'=>'Gerar Boleto','url'=>'/admin/boletos','icon'=>'fa-barcode'],
				]
			],

			['text'=>'Relatorios','url'=>'/admin','icon'=>'fa-bar-chart-o'],
		];
	}elseif ($User->role->name=='Corretor'){
		$menu = [
			['text'=>'Home','url'=>'/admin','icon'=>'fa-home'],
			['text'=>'Imoveis','url'=>'/admin/imoveis','icon'=>' fa-building-o'],			
			['text'=>'Clientes','url'=>'/admin/clientes','icon'=>'fa-users'],
			['text'=>'Reservas','url'=>'/admin/corretor/reservas/','icon'=>' fa-check-circle-o'],
			['text'=>'Propostas','url'=>'/admin/minhas_propostas','icon'=>' fa-files-o'],
			['text'=>'Relatorios','url'=>'/admin','icon'=>'fa-bar-chart-o'],
		];
	}else{
		$menu = [
			['text'=>'Home','url'=>'/cliente','icon'=>'fa-camera'],
			//['text'=>'Indicadores','url'=>'/cliente/indicadores','icon'=>'fa-bar-chart-o'],			
			//['text'=>'Seu Imovel','url'=>'/cliente/estagios','icon'=>' fa-building-o'],
			['text'=>'Financeiro','url'=>'','icon'=>'fa-money',
				'submenu'=>[
					['text'=>'Extrato Financeiro','url'=>'/cliente/pagamentos','icon'=>'fa-camera'],
					['text'=>'Parcelas e Boletos','url'=>'/cliente/parcelas','icon'=>'fa-camera'],
				]
			],
		];
	}
	$menu = json_encode($menu);
	$menu = json_decode($menu);
    $view->with('menu',$menu);
});