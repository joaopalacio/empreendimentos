<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                {{isset($data['title']) ?$data['title'] : 'Tabela de valores' }}
            </header>
            <input id="{{$data['classSub']| '' }}_id" type="hidden" value="{{$data['id'] | '' }}"/>
            <div class="panel-body">
                <table class="table sliders">
                    <tbody>
                        @foreach ($data['model']->estagios as $key => $value) 
                        <tr>
      
                            <td style="width:14%">{{ucfirst($value->estagio)}}</td>
                            <td>
                                <div id="slider-{{$value->id}}" class="slider"></div>
                                <input type="hidden" data-estagio="{{$value->id}}" id="val-slider-{{$value->id}}" value="{{$value->porcent}}">
                                <div class="slider-info">
                                  Total:
                                  <span class="text-slider">{{number_format($value->porcent,0)}}%</span>
                                </div>
                            </td>
                        </tr>

                        @endforeach
                    </tbody>
                </table>
            </div>                
        </section>
    </div>
</div>

