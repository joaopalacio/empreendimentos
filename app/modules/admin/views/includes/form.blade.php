<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                {{isset($data->formTitle) ? $data->formTitle : 'Formulario a ser preenchido' }}
            </header>
            <div class="panel-body">
                <div class=" form">
                    @if ( $errors->count() > 0 )
                      <div class="alert alert-block alert-danger fade in">
                          <button data-dismiss="alert" class="close close-sm" type="button">
                              <i class="fa fa-times"></i>
                          </button>
                          <strong>Erro</strong>
                          <ul>
                            @foreach( $errors->all() as $message )
                              <li>{{ $message }}</li>
                            @endforeach
                          </ul>                    
                      </div>              
                    @endif
                    @if(isset($data['model']))
                        {{Form::model($data['model'], array('url' => array($data['route'], $data['model']->id),'method'=>isset($data['method']) ? $data['method'] : 'post','class'=>'cmxform form-horizontal tasi-form'))}}
                    @else
                        {{Form::open(array('url' => array($data['route']),'method'=>isset($data->method) ? $data->method : 'post','class'=>'cmxform form-horizontal tasi-form'))}}
                    @endif

                        @foreach ($data['elements'] as $element)
                            @if(isset($element['fieldSet']))
                                {{--Gera Campos subsequentes--}}
                                @foreach ($element['fieldSet'] as $subelement)
                                    @include('admin::includes.inputs',['element'=>$subelement])  
                                @endforeach
                            @else
                                @include('admin::includes.inputs',['element'=>$element])
                            @endif  
                        @endforeach
                        
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-danger" type="submit">{{Lang::get('admin.defaults.save')}}</button>
                                <button class="btn btn-default" type="reset">{{Lang::get('admin.defaults.cancel')}}</button>
                            </div>
                        </div>
                    {{Form::close()}}
                </div>

            </div>
        </section>
    </div>
</div>