<li class="dropdown">
    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
        <img alt="" src="assets/back/img/avatar1_small.jpg">
        <span class="username">{{isset(Auth::user()->name) ? Auth::user()->name : 'Sem Nome' }}</span>
        <b class="caret"></b>
    </a>
    
    <ul class="dropdown-menu extended logout">
        <div class="log-arrow-up"></div>
        <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
        <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
        <li><a href="#"><i class="fa fa-bell-o"></i> Notification</a></li>
        <li><a href="login.html"><i class="fa fa-key"></i> Log Out</a></li>
    </ul>
</li>