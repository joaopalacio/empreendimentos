                              <!-- The file upload form used as target for the file upload widget -->
                              <form id="fileupload" action="{{$data['route']}}" method="POST" enctype="multipart/form-data">
                                  <!-- Redirect browsers with JavaScript disabled to the origin page -->
                                  <noscript>
                                      <input type="hidden" name="redirect" value="{{route('admin.index')}}">
                                  </noscript>
                                  <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                                  <div class="row fileupload-buttonbar">
                                      <div class="col-lg-7">
                                          <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        <span>Adicionar</span>
                                        <input type="file" name="files[]" multiple>
                                        </span>
                                          <button type="submit" class="btn btn-primary start">
                                              <i class="glyphicon glyphicon-upload"></i>
                                              <span>Enviar todos</span>
                                          </button>
                                          <button type="reset" class="btn btn-warning cancel">
                                              <i class="glyphicon glyphicon-ban-circle"></i>
                                              <span>Cancelar upload</span>
                                          </button>
                                          <!-- The global file processing state -->
                                          <span class="fileupload-process"></span>
                                      </div>
                                      <!-- The global progress state -->
                                      <div class="col-lg-5 fileupload-progress fade">
                                          <!-- The global progress bar -->
                                          <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                              <div class="progress-bar progress-bar-success" style="width:0%;">
                                              </div>
                                          </div>
                                          <!-- The extended global progress state -->
                                          <div class="progress-extended">
                                              &nbsp;
                                          </div>
                                      </div>
                                  </div>
                                  <!-- The table listing the files available for upload/download -->
                                  <table role="presentation" class="table table-striped">
                                      <tbody class="files">
                                      </tbody>
                                  </table>
                              </form>
                              <div class="panel panel-default">
                                  <div class="panel-heading">
                                      <h3 class="panel-title">Informações</h3>
                                  </div>
                                  <div class="panel-body">
                                      <ul>
                                          <li>O tamanho maximo para uploads é de <strong>5 MB</strong></li>
                                          <li>Apenas arquivos tipo (<strong>JPG, GIF, PNG</strong>) são autorizadas</li>
                                      </ul>
                                  </div>
                              </div>                              