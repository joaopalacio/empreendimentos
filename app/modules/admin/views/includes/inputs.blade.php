{{--Input type spinner--}}
@if($element['type']=="spinner")
    <div class="form-group ">
        <label for="cname" class="control-label col-lg-2">{{$element['campo']}}</label>
        <div class="col-lg-1">
            <div class="spinners">
                <div class="input-group input-small">                                    
                    {{Form::text($element['name'],@$data['model']->{$element['name']},['class'=>'spinner-input form-control '.$element['class'],'id'=>$element['id'],'minlength'=>$element['minlength'],'readonly'=>'','placeholder'=>$element['placeholder']])}}
                    <div class="spinner-buttons input-group-btn btn-group-vertical">
                        <button type="button" class="btn spinner-up btn-xs btn-default">
                            <i class="fa fa-angle-up"></i>
                        </button>
                        <button type="button" class="btn spinner-down btn-xs btn-default">
                            <i class="fa fa-angle-down"></i>
                        </button>
                    </div>
               </div>
             </div>                                               
        </div>
    </div>  
{{--Input type select number Range (0..n)--}}    
@elseif ($element['type']=="selectRange")
    <div class="form-group ">
        <label for="cname" class="control-label col-lg-2">{{$element['campo']}}</label>
        <div class="col-lg-10">
            {{  Form::{$element['type']}($element['name'],$element['lista'][0],$element['lista'][1],['class'=>'form-control '.$element['class']])  }}
        </div>
    </div> 
{{--Input type hidden--}}
@elseif ($element['type']=="hidden")
            {{  Form::hidden($element['name'],$element['value'])}}
@elseif ($element['type']=="div")
    <div class="form-group ">
        <label for="cname" class="control-label col-lg-2">{{$element['campo']}}</label>
        <div class="col-lg-10" id="{{$element['id']}}">
           
        </div>
    </div>  
{{--Input type select multiple values--}}
@elseif ($element['type']=="multi")
  <div class="form-group">
      <label class="control-label col-md-2">{{$element['campo']}}</label>
      <div class="col-md-10">
          <select multiple="multiple" class="multi-select" id="{{$element['name']}}" name="{{$element['name']}}[]">
                @foreach($element['values'] as $val)
                    <option value="{{$val->id}}">{{$val->name}}</option>
                @endforeach 
          </select>
      </div>
  </div>
{{--Input type select options--}}
 @elseif($element['type']=="select")               
        <div class="form-group ">
            <label for="cname" class="control-label col-lg-2">{{$element['campo']}}</label>
            <div class="col-lg-10">
                {{Form::{$element['type']}($element['name'],$element['lista'],@$data['model']->{$element['name']},['class'=>'form-control '.$element['class'],'id'=>$element['id'],'minlength'=>$element['minlength'],'placeholder'=>@$element['placeholder']])}}
            </div>
        </div>                             
@else
{{--Input type text--}}
    <div class="form-group ">
        <label for="cname" class="control-label col-lg-2">{{$element['campo']}}</label>
        <div class="col-lg-10">
            <?php $value = (isset($data['model'])) ? @$data['model']->{$element['name']} : @$element['value'];?>
            {{Form::text($element['name'],$value,['class'=>'form-control '.$element['class'],'id'=>$element['id'],'placeholder'=>$element['placeholder'],@implode(@$element['options'])])}}
            @if(isset($element['span']))
                {{$element['span']}}
            @endif
        </div>
    </div>    
@endif 