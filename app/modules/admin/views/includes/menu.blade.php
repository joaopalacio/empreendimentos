@foreach ($menu as $element)
   @if(isset($element->submenu))
      <li class="sub-menu @if($element->url==Session::get('url'))active @endif">
          <a href="#">
              <i class="fa {{$element->icon}}"></i>
              <span>{{$element->text}}</span>
          </a>
          <ul class="sub">
              @foreach ($element->submenu as $submenu)
                  <li class="@if($element->url==Session::get('url'))active @endif">
                      <a href="{{$submenu->url}}">
                        <i class="fa {{$submenu->icon}}"></i>
                        <span>{{$submenu->text}}</span>
                      </a>
                  </li>
              @endforeach
          </ul>
      </li>
   @else
      <li class="@if($element->url==Session::get('url'))active @endif">
        <a href="{{$element->url}}">
          <i class="fa {{$element->icon}}"></i>
          <span>
            {{$element->text}}
          </span>
        </a>
      </li>
   @endif
@endforeach


