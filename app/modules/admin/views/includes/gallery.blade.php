              <section class="panel">
                  <header class="panel-heading">
                      {{$data['title']}}
                  </header>
                  <div class="panel-body">
                  @include('admin::includes.upload')
                      <ul class="grid cs-style-3">

                            @foreach ($data['model']->uploads as $element)
                              <li>
                                  <figure>
                                      <img src="/{{$data['dir_upload'].$element->url.'_medium.jpg'}}" alt="img04">
                                      <figcaption>
                                           <a class="delImage" data-id="{{$element->id}}"  href="#">Excluir</a>
                                      </figcaption>

                                  </figure>
                              </li>                            
                            @endforeach
                      </ul>

                  </div>
              </section>