<!-- Modal -->
@if(isset($data['modal']))
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{$data['modal']['title']}}</h4>
                </div>
                <div class="modal-body">

                    {{$data['modal']['description']}}

                </div>
                <div class="modal-footer">
                    @foreach($data['modal']['btns'] as $btn)
                        {{HTML::decode(HTML::link($btn['url'],$btn['text'],$btn['array']))}}
                    @endforeach
                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                    <button class="btn btn-success delete" rel="" type="button">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    @if(isset($data['modal']['link']) && $data['modal']['link']=='#makeReserva')
    <div class="modal fade" id="makeReserva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{$data['modal']['title']}}</h4>
                </div>
                <div class="modal-body">

                    {{$data['modal']['description']}}

                </div>
                <div class="modal-footer">
                    @foreach($data['modal']['btns'] as $btn)
                        {{HTML::decode(HTML::link($btn['url'],$btn['text'],$btn['array']))}}
                    @endforeach
                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                    <button class="btn btn-success make-reserva" rel="" type="button">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    @endif
    
@endif