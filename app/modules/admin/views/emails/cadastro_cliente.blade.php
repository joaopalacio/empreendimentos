@extends('admin::layouts.email')

@section('content')
	<tr><td height="35"></td></tr>

<!--section 3-->
<tr>
    <td>
        <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560">
            <tbody>
            <tr >
                <td>
                    <table class="mainContent" align="center" border="0" cellpadding="0" cellspacing="0" width="528">
                        <tbody><tr><td height="20"></td></tr>
                        <tr>
                            <td>

                                <table class="section-item" align="left" border="0" cellpadding="0" cellspacing="0">
                                    <tbody><tr><td height="6"></td></tr>
                                    <tr>
                                        <td><a href="" style="width: 128px; display: block;"><img  style="display: block;" src="assets/back/img/email-img/image2.png" alt="image3" class="section-img" height="auto" width="128"></a></td>
                                    </tr>
                                    <tr><td height="10"></td></tr>
                                    </tbody>
                                </table>

                                <table align="left" border="0" cellpadding="0" cellspacing="0">
                                    <tbody><tr><td height="30" width="30"></td></tr>
                                    </tbody>
                                </table>

                                <table class="section-item" align="left" border="0" cellpadding="0" cellspacing="0" width="360">
                                    <tbody><tr>
                                        <td  style="color: #484848; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">

                                            Seja Bem {{$data['Cliente']->nome or ''}} vindo ao sistema da MaschioShin

                                        </td>
                                    </tr>
                                    <tr><td height="15"></td></tr>
                                    <tr>
                                        <td  style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">

                                            Para se logar no sistema pela primeira vez seguem abaixo o seu email de acesso e sua senha temporaria <b>(troque após o primeiro acesso)</b> ...

                                        </td>
                                    </tr>
                                    <tr><td height="15"></td></tr>

                                    </tbody>
                                </table>

                            </td>
                        </tr>

                        <tr><td height="20"></td></tr>

                        </tbody></table>
                </td>
            </tr>


            </tbody></table>
    </td>
</tr>
<!-- end section 3 -->


<tr><td height="35"></td></tr>

<!--section 4 -->
<tr>
    <td>
        <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560" bgcolor="F1F2F7">
            <tr >
                <td>
                    <table class="mainContent" align="center" border="0" cellpadding="0" cellspacing="0" width="528">
                        <tbody><tr><td height="20"></td></tr>
                        <tr>
                            <td>

                                <table class="section-item" align="left" border="0" cellpadding="0" cellspacing="0" width="360">
                                    <tbody><tr>
                                        <td  style="color: #484848; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">

                                            Seus dados de acesso:

                                        </td>
                                    </tr>
                                    <tr><td height="15"></td></tr>
                                    <tr>
                                        <td  style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                            Email:{{$data['Cliente']->email or ''}}
                                            <br/>
                                            Senha:{{$data['Cliente']->senha or ''}}
                                        </td>
                                    </tr>
                                    <tr><td height="15"></td></tr>
                                    <tr>
                                        <td>
                                            <a href="#" style="background-color: #7087A3; font-size: 12px; padding: 10px 15px; color: #fff; text-decoration: none"> Acessar o sistema</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>


                                <table align="left" border="0" cellpadding="0" cellspacing="0">
                                    <tbody><tr><td height="30" width="30"></td></tr>
                                    </tbody>
                                </table>


                                <table class="section-item" align="left" border="0" cellpadding="0" cellspacing="0">
                                    <tbody><tr><td height="6"></td></tr>
                                    <tr>
                                        <td><a href="" style="width: 128px; display: block;"><img  style="display: block;" src="assets/back/img/email-img/image1.png" alt="image4" class="section-img" height="auto" width="128"></a></td>
                                    </tr>
                                    <tr><td height="10"></td></tr>
                                    </tbody>
                                </table>


                            </td>
                        </tr>

                        <tr><td height="20"></td></tr>

                        </tbody></table>
                </td>
            </tr>


            </table>
    </td>
</tr>
<!-- end section 4 -->

<tr><td height="35"></td></tr>	
@stop


