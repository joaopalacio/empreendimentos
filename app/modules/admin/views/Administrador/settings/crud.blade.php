@extends('admin::layouts.master')

@section('content')
	@include('admin::includes.form',array('data'=>$data))
@stop


@section('documentReady')

	$(".cep").on('change',function(e){
		$.getJSON('http://cep.correiocontrol.com.br/'+$(this).val()+'.json',function(data){
			$("#endereco").val(data.logradouro+", "+data.bairro);
			$("#cidade").val(data.localidade);
			$("#estado").val(data.uf);

		})
	})
	
@stop