@extends('admin::layouts.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                {{'Formulario de Proposta' }}
            </header>
            <div class="panel-body cmxform form-horizontal tasi-form">
                <div class=" form">
					<?php setlocale(LC_MONETARY, 'pt_BR');?>                    
                       	{{--Nome do Cliente--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Cliente</label>
					  <div class="col-lg-10">
					      <p class="form-control-static">{{$data['Proposta']->reserva->cliente->name}}</p>
					  </div>
					</div>              
                	{{--CPF do Cliente--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">CPF</label>
					  <div class="col-lg-10">
					      <p class="form-control-static">{{$data['Proposta']->reserva->cliente->cpf}}</p>
					  </div>
					</div>    
                	{{--Empreendimento--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Empreendimento</label>
					  <div class="col-lg-10">
					      <p class="form-control-static">{{$data['Proposta']->reserva->imovel->empreendimento->nome}}</p>
					  </div>
					</div> 
                	{{--Imovel--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Imovel</label>
					  <div class="col-lg-10">
					      <p class="form-control-static">{{$data['Proposta']->reserva->imovel->tipo}} ({{$data['Proposta']->reserva->imovel->area_privativa}} m2)</p>
					  </div>
					</div>
                	{{--Detalhes--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Quartos</label>
					  <div class="col-lg-10">
					      <p class="form-control-static">{{$data['Proposta']->reserva->imovel->quartos}}</p>
					  </div>
					</div>   
                	{{--Valor do Imovel--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Valor total</label>
					  <div class="col-lg-10">
					      <p class="form-control-static">{{UtilApp::Money($data['RegrasProposta']->valor_imovel,false)}}</p>
					  </div>
					</div>   	
                	{{--Valor do Imovel--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Desconto % </label>
					  <div class="col-lg-10 ">
					      <p class="form-control-static com_desconto">{{UtilApp::Porcent($data['Proposta']->desconto,false)}}</p>
					  </div>
					</div> 											 
                	{{--Valor do Imovel--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Valor Venda</label>
						<div class="col-lg-10">
					      <p class="form-control-static">{{UtilApp::Money($data['RegrasProposta']->valor_imovel*(1-($data['Proposta']->desconto/100)),false)}}</p>
					  </div>
					</div>  				
		                 
				{{Form::open(['url' => '','method'=>isset($data->method) ? $data->method : 'post','class'=>'cmxform form-horizontal tasi-form','id'=>'default'])}}                       													
		        		{{--DETALHES DA PROPOSTA--}}   

		            	{{--Desconto padrão do Imovel--}}
 			
 			

		
							{{--
							- Preenchimento de proposta, definindo condições de pagamento, dentro das regras pré-

							estabelecidas pelo administrador para aquele determinado imóvel; 

							- Caso o administrador tenha permitido o desconto padrão, aparecerá um box para selecionar 

							o desconto oferecido; 

							- Box para seleção do desconto padrão, que concederá desconto para o pagamento "no ato" 

							- Quando o corretor for preencher a proposta, os valores das parcelas vão aparecer conforme 

							o default definido para cada empreendimento. O procedimento para cadastro de parcelas é: 

							Primeiro o corretor coloca o valor ou a porcentagem total que será financiada com a 

							construtora. 

							Depois define a entrada, depois o FGTS, e depois as chaves. 

							Depois as intermediarias.

							O restante será dividido em parcelas iguais e o sistema calculará sozinho. 

							Cada vez que o corretor alterar um dos valores acima, as parcelas serão recalculadas; 

							- Definir se a proposta está sendo cadastrada com a comissão do corretor ou sem (uma 

							marcação na proposta)
							--}}
							<?php 
								$valor_imovel= $data['RegrasProposta']->valor_imovel*(1-($data['Proposta']->desconto/100));
								$valor_a_financiar = ($valor_imovel*$data['Proposta']->porcent/100);
								$valor_pago        = $data['Proposta']->fgts+$data['Proposta']->chaves+$data['Proposta']->entrada;
							?>

		            	{{--Quantos % sera financiado com a construtora do Imovel--}}
		            	{{--Porcentagem Financiada com empresa--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">% Financiamento</label>
						  <div class="col-lg-10 porcent_final">
						      <p class="form-control-static com_desconto">
						      	{{UtilApp::Porcent($data['Proposta']->porcent,false)}}
						      </p>						      
						  </div>
						</div> 							
						<div class="form-group">
						  <label class="col-lg-2 control-label">Valor Financiado</label>
						  <div class="col-lg-10 porcent_final">
						      <p class="form-control-static com_desconto">
						      	{{UtilApp::Money($valor_a_financiar,false)}}
						      </p>						      
						  </div>
						</div> 													
				    	{{--Valor da FGTS do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">FGTS</label>
						  <div class="col-lg-10 fgts_final">
						      <p class="form-control-static com_desconto">
						      	{{UtilApp::Money($data['Proposta']->fgts,false)}}
						      </p>
						  </div>
						</div> 																
		            	{{--Valor da Entrada do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Entrada</label>
						  <div class="col-lg-10 entrada_final">
						      <p class="form-control-static com_desconto">
						      	{{UtilApp::Money($data['Proposta']->entrada,false)}}
						      </p>						      
						  </div>
						</div> 							
		            	{{--Valor da Chaves do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Chaves</label>
						  <div class="col-lg-10 chaves_final">
						      <p class="form-control-static com_desconto">
						      	{{UtilApp::Money($data['Proposta']->chaves,false)}}
						      </p>						      
						  </div>
						</div> 						
		            	{{--Valor da Intermediarias do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Intermediarias</label>
						  <div class="col-lg-10 intermediarias_final">
						      <p class="form-control-static com_desconto">						      
						      	@if(count($data['Proposta']->intermediarias))
						      		@foreach($data['Proposta']->intermediarias as $key =>$meio)
						      			<?php $valor_pago+=$meio->valor;?>
						      			<div class="col-lg-10" style="border-bottom:1px dashed #CCC">
						      			  <div class="col-lg-1 valor_final">
						      			  	{{$meio->numero}} mês 
						      			  </div>	
										  <div class="col-lg-9 valor_final">
											 {{UtilApp::Money($meio->valor,false)}}      
										  </div>						      								      		
										</div>  
						      		@endforeach
						      	@endif
						      </p>						      
						  </div>
						</div> 			
		            	{{--Valor da Prazo do Financiamento--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Prazo</label>
						  <div class="col-lg-10 prazo_final">
						      <p class="form-control-static com_desconto">
						      	{{$data['Proposta']->prazo}} meses
						      </p>						      
						  </div>
						</div> 							
		            	{{--Valor Total a ser financiado do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Valor a ser Parcelado</label>
						  <div class="col-lg-10 valor_final">
						      {{UtilApp::Money($valor_a_financiar-$valor_pago,false)}}      
						  </div>
						</div> 												
		            	{{--Valor da Parcelas do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Parcelas</label>
						  <div class="col-lg-10 parcelas_final">
					      		@for($index=0;$index<$data['Proposta']->prazo;$index++)
					      			<div class="col-lg-10" style="border-bottom:1px dashed #CCC">
					      			  <div class="col-lg-1 valor_final">
					      			  	{{$index+1}} parcela
					      			  </div>	
									  <div class="col-lg-9 valor_final">
										 {{UtilApp::Money(($valor_a_financiar-$valor_pago)/$data['Proposta']->prazo,false)}}      
									  </div>						      								      		
									</div>  
					      		@endfor						  
						  </div>
						</div> 								
                  	</fieldset>                   	          
              	{{Form::close()}}
		    </div>
		</section>     																																														                         
	</div>           
</div>
@stop

