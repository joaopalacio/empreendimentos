@extends('admin::layouts.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                {{'Tabela de Propostas' }}
            </header>
            @if (isset($data['insert']))
              <div class="panel-body" style="text-align: right;">
                  <a href="{{HTML::decode($data['insert'])}}" class="btn btn-primary"><i class="fa fa-cloud"></i> Novo Registro</a>
              </div> 
            @endif
           
            <div class="panel-body">
                <section id="unseen">
                  <table class="table table-bordered table-striped table-condensed">
                    <thead>
                    <tr>
                      {{--Foreach dos valores de data[thead]--}}
                      @foreach ($data['thead'] as $element)
                          <th class="{{$element['class']}}">{{$element['name']}}</th>
                      @endforeach                        
                    </tr>
                    </thead>
                    <tbody>
                    {{--Foreach dos valores do model passado em data[model]--}}
                    @foreach($data['model'] as $key=>$value)
                        <tr>         
                                <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($value->created_at))->diffForHumans()}}</td>
                                <td><a href="{{route('admin.proposta.informacoes',$value->reserva_id)}}">{{$value->reserva_id}}</a></td>
                                <td>{{$value->empreendimento}}</td>
                                <td>{{$value->imovel}}</td>
                                <td>{{$value->corretor}}</td>
                                <td>{{UtilApp::Money($value->valor_imovel,false)}}</td>
                                <td>{{UtilApp::Porcent($value->desconto,false)}}</td>
                                <td>{{UtilApp::Money($value->entrada,false)}}</td>
                                <td>{{UtilApp::Money($value->fgts,false)}}</td>
                                <td>
                                  
                                    <div class="btn-group">
                                    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle btn-sm" type="button">{{$value->status}} <span class="caret"></span></button>
                                    <ul role="menu" class="dropdown-menu">                                    

                                        <li><a href="#" data-id="{{$value->id}}">Aprovado</a></li>
                                        <li><a href="#" data-id="{{$value->id}}">Recusado</a></li>
                                    </ul>
                                    </div>
                                </td>
                                  {{--Se o TD for um array que a chave != dropdown--}}    
                            <td class="numeric">
                            <div class="pull-right hidden-phone">
                                @foreach ($data['buttons'] as $btn)

                                  @if(!isset($btn['modal']) && (!isset($btn['sub'])))
                                    {{HTML::decode(HTML::link(URL::route($btn['link'],$value->id),$btn['text'],$btn['array']))}}
                                  @elseif(!isset($btn['modal']))  
                                    {{HTML::decode(HTML::link(URL::route($btn['link'],[$value->{$btn['sub']},$value->id]),$btn['text'],$btn['array']))}}
                                  @else
                                    {{HTML::decode(HTML::link($btn['link'],$btn['text'],$btn['array']+['rel'=>$value->id]+['data-url'=>$data['destroy']]))}}
                                  @endif
                                @endforeach
                            </div>                        
                          </td>
                        </tr>
                    @endforeach                                            
                    </tbody>
                </table>
                  @if(!isset($data['not-paging']))
                    {{$data['model']->links()}}
                  @endif
                </section>
            </div>
        </section>
          @include('admin::includes.modal')        
    </div>
</div>


@stop

@section('documentReady')
	var id = "";
	var url = "";

	$(".delete").on("click",function(){
		window.location.href = url+'/'+id;
	});

	$(".dropdown-menu > li > a").on('click',function(e){
		var value = ($(this).html());
		var html = '<span class="caret"></span>';
		$(this).parent().parent().prev('button').empty().html(value+html);
		$.post("{{route('ajax.propostas.status')}}",{id:$(this).attr('data-id'),status:value},function(data){        top.location.href="{{route('admin.lista.propostas')}}";});
	})	
@stop