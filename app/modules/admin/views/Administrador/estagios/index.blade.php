@extends('admin::layouts.master')

@section('content')
	@include('admin::includes.porcent',array('data'=>$data))
@stop

@section('documentReady')

            $(".slider").slider({
                value: parseInt($("#val-"+$(this).attr('id')).val()),
                min: 0,
                max: 100,
                range: "min",
                create: function(event,ui){
                	var value = (parseInt($("#val-"+$(this).attr('id')).val()));
                	$(this).slider("value",value)

                },
                slide: function (event, ui) {
                    $(this).next().next(".slider-info").find(".text-slider").html(ui.value+"%");
                },
                stop: function( event, ui ) {
                	data = { 
                		porcent : (ui.value),
                		empreendimento_id : ($("#empreendimento_id").val()),
                		estagio_id : $("#val-"+$(this).attr('id')).attr('data-estagio')
                	};
				    $.post('{{URL::Route('ajax.setEstagio')}}', data);                	
                }
            });
@stop

@section('scriptsExtra')
    {{HTML::script('assets/back/assets/jquery-ui/jquery-ui-1.10.1.custom.min.js')}}
    {{HTML::script('assets/back/js/jquery.ui.touch-punch.min.js')}}
@stop