@extends('admin::layouts.master')



@section('content')
	<style>
	.menuDash li{
		border-bottom:1px solid #CCC;padding:5px 0;margin:0 0 5px 0;
		cursor: pointer;
	}
	</style>
	
	<div class="row">
	    <div class="col-lg-12">
	        <section class="panel">
	            <header class="panel-heading">
	                Bem vindo ao sistema de gerenciamento
	            </header>
	            <div class="panel-body">
	                <div class=" form">
		                <div class="col-lg-2" style="border:1px solid #CCCFFF;padding:20px">
		                	<h4>Menu</h4>
		                	<ul class="menuDash">		                		
								<li><a href="#">Imóveis Reservados</a></li>
								<li><a href="#">Reservados com Documentação</a></li>
								<li><a href="#">Clientes em atraso</a></li>
								<li><a href="#">Geração de 2° via</a></li>
								<li><a href="#">Geração de boleto extra</a></li>
								<li><a href="#">Indicadores financeiros</a></li>
		                	</ul>
		                </div>
		                <div class="col-lg-10 iframe" style="padding:20px 0">
		                
		                </div>
	                </div>
	            </div>
	        </section>
	    </div>
    </div>        

@stop


@section('documentReady')
	


@stop