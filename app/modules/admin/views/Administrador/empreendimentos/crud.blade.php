@extends('admin::layouts.master')

@section('content')

	@include('admin::includes.form',array('data'=>$data))

@stop


@section('documentReady')


	$(".change_bairros").on('change',function(e){
	    $("#bairro_id").find("option:gt(0)").remove();
	    $("#bairro_id").find("option:first").text("Loading...");
	    $.getJSON('{{URL::Route('ajax.getBairros')}}', {
	        cidade_id: $(this).val()
	    }, function (json) {
	        $("#bairro_id").find("option:first").text("Selecione o Bairro");
	        for (var i = 0; i < json.length; i++) {
	            $("<option/>").attr("value", json[i].id).text(json[i].bairros).appendTo($("#bairro_id"));
	        }
	    });
	})

@if(Request::is('admin/empreendimentos/edit/*'))
		var bairro_id = {{$data['model']->bairro_id}};
 	    $.getJSON('{{URL::Route('ajax.getBairros')}}', {
	        cidade_id: $("#cidade_id").val()
	    }, function (json) {
	        $("#bairro_id").find("option:first").text("Selecione o Bairro");
	        for (var i = 0; i < json.length; i++) {

	        	if(bairro_id==json[i].id)
	            	$("<option/>").attr({"value": json[i].id,'selected':'selected'}).text(json[i].bairros).appendTo($("#bairro_id"));
	            else{
	            	$("<option/>").attr({"value": json[i].id}).text(json[i].bairros).appendTo($("#bairro_id"));
	            }	
	        }
	    });   
@endif


@stop