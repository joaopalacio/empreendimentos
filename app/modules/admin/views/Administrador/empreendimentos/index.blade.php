@extends('admin::layouts.master')

@section('content')
	@include('admin::includes.table',array('data'=>$data))
@stop

@section('documentReady')

	var id = "";
	var url = "";
	$(".delModal").on('click',function(e){
		id = $(this).attr('rel');
		url = $(this).attr('data-url');
	});
	$(".delete").on("click",function(){
		window.location.href = url+'/'+id;
	});

	$(".dropdown-menu > li > a").on('click',function(e){
		var value = ($(this).html());
		var html = '<span class="caret"></span>';
		$(this).parent().parent().prev('button').empty().html(value+html);
		$.post("{{route('ajax.mudaStatus')}}",{empreendimento_id:$(this).attr('data-id'),status:value});

	})

  $(".destaque").on("click",function(){

    if($(this).is(':checked')) var valor = 'destaque'; else var valor = "";

    $.ajax({
      type: "POST",
      url: "{{route('ajax.set.empreendimento.destaque')}}",
      data: { 
        empreendimento_id:$(this).attr('data-id'),
        destaque:valor
      }
    })

  })

@stop