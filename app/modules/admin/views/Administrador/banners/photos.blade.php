@extends('admin::layouts.master')

@section('styles')
		{{HTML::style('assets/back/assets/fancybox/source/jquery.fancybox.css')}}
		{{HTML::style('assets/back/css/gallery.css')}}
		<!-- blueimp Gallery styles -->
		{{HTML::style('http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css')}}
		<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
		{{HTML::style('assets/back/assets/file-uploader/css/jquery.fileupload.css')}}
		{{HTML::style('assets/back/assets/file-uploader/css/jquery.fileupload-ui.css')}}
		<!-- CSS adjustments for browsers with JavaScript disabled -->
		<noscript>
			{{HTML::style('assets/back/assets/file-uploader/css/jquery.fileupload-noscript.css')}}
		</noscript>
		<noscript>
			{{HTML::style('assets/back/assets/file-uploader/css/jquery.fileupload-ui-noscript.css')}}
      	</noscript>	    
@stop

@section('content')
              <section class="panel">
                  <header class="panel-heading">
                      {{$data['title']}}
                  </header>
                  <div class="panel-body">
                  @include('admin::includes.upload')
                      <ul class="grid cs-style-3">

                            @foreach ($data['model'] as $element)
                              <li>
                                  <figure>
                                      <img src="/{{$data['dir_upload'].$element->image.'.jpg'}}" alt="img04">
                                      <figcaption>
                                           <a class="delImage" data-id="{{$element->id}}"  href="#">Excluir</a>
                                      </figcaption>

                                  </figure>
                              </li>                            
                            @endforeach
                      </ul>

                  </div>
              </section>
@stop


@section('scripts')
  <!--Multiuple File Uploader-->
  <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
  {{HTML::script('assets/back/assets/file-uploader/js/vendor/jquery.ui.widget.js')}}
  <!-- The Templates plugin is included to render the upload/download listings -->
  {{HTML::script('http://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js')}}
  <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
  {{HTML::script('http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js')}}
  <!-- The Canvas to Blob plugin is included for image resizing functionality -->
  {{HTML::script('http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js')}}

  <!-- blueimp Gallery script -->
  {{HTML::script('http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js')}}
  <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
  {{HTML::script('assets/back/assets/file-uploader/js/jquery.iframe-transport.js')}}
  <!-- The basic File Upload plugin -->
  {{HTML::script('assets/back/assets/file-uploader/js/jquery.fileupload.js')}}
  <!-- The File Upload processing plugin -->
  {{HTML::script('assets/back/assets/file-uploader/js/jquery.fileupload-process.js')}}
  <!-- The File Upload image preview & resize plugin -->
  {{HTML::script('assets/back/assets/file-uploader/js/jquery.fileupload-image.js')}}
  <!-- The File Upload audio preview plugin -->
  {{HTML::script('assets/back/assets/file-uploader/js/jquery.fileupload-audio.js')}}
  <!-- The File Upload video preview plugin -->
  {{HTML::script('assets/back/assets/file-uploader/js/jquery.fileupload-video.js')}}
  <!-- The File Upload validation plugin -->
  {{HTML::script('assets/back/assets/file-uploader/js/jquery.fileupload-validate.js')}}
  <!-- The File Upload user interface plugin -->
  {{HTML::script('assets/back/assets/file-uploader/js/jquery.fileupload-ui.js')}}
  <!-- The main application script -->
  {{HTML::script('assets/back/assets/file-uploader/js/main.js')}}
  <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
  <!--[if (gte IE 8)&(lt IE 10)]>
  {{HTML::script('assets/back/assets/file-uploader/js/cors/jquery.xdr-transport.js')}}
  <![endif]-->

  <!-- The template to display files available for upload -->
  <script id="template-upload" type="text/x-tmpl">
      {% for (var i=0, file; file=o.files[i]; i++) { %}
      <tr class="template-upload fade">
          <td>
              <span class="preview"></span>
          </td>
          <td>
              <p class="name">{%=file.name%}</p>
              <strong class="error text-danger"></strong>
          </td>
          <td class="title"><label>Link para o Banner: <input name="title[]" placeholder="http://www..." required></label></td>
          <td>
              <p class="size">Processing...</p>
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
          </td>
          <td>
              {% if (!i && !o.options.autoUpload) { %}
              <button class="btn btn-primary start" disabled>
                  <i class="glyphicon glyphicon-upload"></i>
                  <span>Start</span>
              </button>
              {% } %}
              {% if (!i) { %}
              <button class="btn btn-warning cancel">
                  <i class="glyphicon glyphicon-ban-circle"></i>
                  <span>Cancel</span>
              </button>
              {% } %}
          </td>
      </tr>
      {% } %}
  </script>
  <!-- The template to display files available for download -->
  <script id="template-download" type="text/x-tmpl">
      {% for (var i=0, file; file=o.files[i]; i++) { %}
      <tr class="template-download fade">
          <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
          </td>
          <td>
              <p class="name">
                  {% if (file.url) { %}
                  <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                  {% } else { %}
                  <span>{%=file.name%}</span>
                  {% } %}
              </p>
              {% if (file.error) { %}
              <div><span class="label label-danger">Error</span> {%=file.error%}</div>
              {% } %}
          </td>
          <td>
              <span class="size">{%=o.formatFileSize(file.size)%}</span>
          </td>
          <td>
              {% if (file.deleteUrl) { %}
              <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
              <i class="glyphicon glyphicon-trash"></i>
              <span>Delete</span>
              </button>
              <input type="checkbox" name="delete" value="1" class="toggle">
              {% } else { %}
              <button class="btn btn-warning cancel">
                  <i class="glyphicon glyphicon-ban-circle"></i>
                  <span>Cancel</span>
              </button>
              {% } %}
          </td>
      </tr>
      {% } %}
  </script>
@stop

@section('documentReady')

      $(function() {
        //    fancybox
          jQuery(".fancybox").fancybox();

        $('#fileupload').bind('fileuploadsubmit', function (e, data) {
            var inputs = data.context.find(':input');
            if (inputs.filter(function () {
                    return !this.value && $(this).prop('required');
                }).first().focus().length) {
                data.context.find('button').prop('disabled', false);
                return false;
            }
            data.formData = inputs.serializeArray();
        });


          $('.delImage').on('click',function(e){
          	e.preventDefault();
          	if(confirm('Deseja excluir o item?')){
          		$(this).parent().parent().parent().remove();
          		$.post('{{route('banners.images.delete')}}',{id:$(this).attr('data-id')});
          	}          	
          })

      });


@stop