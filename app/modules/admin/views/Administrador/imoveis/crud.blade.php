@extends('admin::layouts.master')

@section('content')
	@include('admin::includes.form',array('data'=>$data))
@stop


@section('documentReady')

	var count = 0;
	$("select[name=intermediarias]").addClass("form-control");

	mudaIntermediarias();
	function mudaIntermediarias(){
		if (count>parseInt($("select[name=intermediarias]").val())){
			$("#lista").empty().html('');
			count=0;						
			var percorre = parseInt($("select[name=intermediarias]").val());
		}else{
			var percorre = parseInt($("select[name=intermediarias]").val())-count;
		}		
		count = parseInt($("select[name=intermediarias]").val());
		for (var i=0;i<percorre;i++){						
			$('#lista').append('<div class="form-group"><div class="col-lg-2"><input type="text" name="intermediarias['+i+'][mes]" class="form-control"  id="intermediaria_mes[]" placeholder="Mês Intermediaria"></div><div class="col-lg-2"><input type="text" name="intermediarias['+i+'][valor]" class="form-control money"  id="intermediaria_valor[]" placeholder="Valor Intermediaria"></div></div>');
			$(".money").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'', decimal:'.', affixesStay: false});			
		}

	}

	$("select[name=intermediarias]").change(function(e){	
		mudaIntermediarias();
	})




	

@stop