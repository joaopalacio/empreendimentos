@extends('admin::layouts.master')

@section('content')
	@include('admin::includes.table',array('data'=>$data))
@stop

@section('documentReady')

	var id = "";
	var url = "";
	$(".delModal").on('click',function(e){
		id = $(this).attr('rel');
		url = $(this).attr('data-url');
	});
	$(".delete").on("click",function(){
		window.location.href = url+'/'+id;
	})


@stop