@extends('admin::layouts.master')

@section('content')
	@include('admin::includes.table',array('data'=>$data))
@stop

@section('documentReady')

	var id = "";
	var url = "";
	$(".btn-reserva").on('click',function(e){
		id = $(this).attr('rel');
		url = $(this).attr('url.ajax');
	});
	$(".make-reserva").on("click",function(){
		$.ajax({
		  type: "POST",
		  url: url,
		  data: { id:id }
		})
	  	.done(function( msg ) {
	    	if(msg==0)
	    	{
				alert('Imovel já reservado');    		
	    	}
	    	else
	    	{
	    		window.location.href = '{{route("corretor.imoveis")}}'
	    	}
	  	});		
	})


@stop