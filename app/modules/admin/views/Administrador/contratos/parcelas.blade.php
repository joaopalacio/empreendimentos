@extends('admin::layouts.master')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Lista de parcelas e emissão de boletos
            </header>
           
            <div class="panel-body">
                <section id="unseen">
                  <table class="table table-bordered table-striped table-condensed">
                    <thead>
                    <tr>
                       <th class="">Parcela</th>
                       <th class="">Valor</th>
                       <th class="">Vencimento</th>
                       <th class="">Status</th>
                       <th class="">Nosso Numero</th>
                       <th class="">Multa</th>
                       <th class="">Mora</th>
                       <th class="">Valor Pago</th>
                       <th class="numeric"></th>
                    </tr>
                    </thead>
                    <tbody>
                    {{--Foreach dos valores do model passado em data[model]--}}
						@if(isset($Contrato))
							@foreach ($Contrato[0]['parcelas'] as $element)
              <tr>
								<td>{{$element['description']}}</td>
								<td>{{UtilApp::Money($element['valor'],false)}}</td>
								<td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($element['vencimento']))->format('d/m/Y');}}</td>
                <td>
                  @if($element['pago']!='')
                      <span class="label label-success pull-right r-activity">Pago</span>   
                  @elseif(\Carbon\Carbon::now()->gte(\Carbon\Carbon::createFromTimeStamp(strtotime($element['vencimento']))->addDay(1))) 
                      <span class="label label-danger pull-right r-activity">Atrasada</span> 
                  @else
                      <span class="label label-info pull-right r-activity">Aguardando</span> 
                  @endif
                 
                </td>
                <td>
                  @if(isset($element['boletos']))
                    @foreach($element['boletos'] as $value)
                      {{$value['nosso_numero']}}
                    @endforeach
                  @endif
                </td>
                <td>{{$element['multa']}}</td>
                <td>{{$element['mora']}}</td>
                <td>{{UtilApp::Money($element['pago'],false)}}</td>
								<td>
									@if($element['pago']=='')
                    @if(count($element['boletos']))
                    <a href="{{route('admin.contratos.darbaixa',($element['id']))}}" class="btn btn-white">
                      <i class="fa fa-money text-info"></i>
                       Dar Baixa
                    </a>
                    @endif
                  @endif
										
								</td>
               </tr> 
							@endforeach
						@endif
                    </tbody>
                </table>
                </section>
            </div>
        </section>
          @include('admin::includes.modal')        
    </div>
</div>
@stop