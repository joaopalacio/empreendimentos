@extends('admin::layouts.master')

@section('content')
              <div class="row">
                  <div class="col-md-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Envio do Arquivo de Retorno (Padrão CNAB240)
                                  <span class="tools pull-right">
                                    <a href="javascript:;" class="fa fa-chevron-down"></a>
        
                                </span>
                          </header>
                          <div class="panel-body">
                              <form action="{{route('admin.financeiro.upload')}}" method="POST" class="form-horizontal tasi-form" enctype="multipart/form-data">
                                      <div class="form-group">
                                          <label class="control-label col-md-3">Selecione o Arquivo</label>
                                          <div class="col-md-4">
                                              <input type="file" name="retorno" class="default" />
                                          </div>
                                      </div>
                                     <button type="submit" class="btn btn-info">Enviar</button>

                              </form>
                          </div>
                      </section>
                  </div>
              </div>
@stop


