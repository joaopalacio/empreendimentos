@extends('admin::layouts.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                {{isset($data['title']) ?$data['title'] : 'Tabela de valores' }}
            </header>
            @if (isset($data['insert']))
              <div class="panel-body" style="text-align: right;">
                  <a href="{{HTML::decode($data['insert'])}}" class="btn btn-primary"><i class="fa fa-cloud"></i> Novo Registro</a>
              </div> 
            @endif
           
            <div class="panel-body">
                <section id="unseen">
                  <table class="table table-bordered table-striped table-condensed">
                    <thead>
                    <tr>
                      {{--Foreach dos valores de data[thead]--}}
                      @foreach ($data['thead'] as $element)
                          <th class="{{$element['class']}}">{{$element['name']}}</th>
                      @endforeach                        
                    </tr>
                    </thead>
                    <tbody>
                    {{--Foreach dos valores do model passado em data[model]--}}
                    @foreach($data['model'] as $key=>$value)
                        <?php 
                          $proposta_aguardando = Proposta::whereReservaId($value->id)->where('status','=','Aguardando')->count();                                               
                        ?>
                        @if(!$proposta_aguardando)
                            <tr>         
                            {{--Foreach dos valores de $data[td]--}}            
                              @foreach ($data['td'] as $key => $td)
                                  {{--Funcoes de dropdown (uso de array no value)--}}
                                  @if(is_array($td))
                                      @if($key=="dropdown")
                                      <td>
                                      
                                          <div class="btn-group">
                                          <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle btn-sm" type="button">{{$value->status_venda}} <span class="caret"></span></button>
                                          <ul role="menu" class="dropdown-menu">
                                            @if(isset($td['values']))
                                               @foreach ($td['values'] as $element)
                                                 <li><a href="#" data-id="{{$value->id}}">{{$element}}</a></li>
                                               @endforeach
                                            @endif   
                                          </ul>
                                          </div>
                                          </td>
                                      {{--Se o TD for um array que a chave != dropdown--}}    
                                      @else

                                      @endif
                                  @else
                                  <td>
                                        {{--Funcoes de Strings--}}
                                        @if(is_string($key))
                                            {{call_user_func_array($key,[$value->{$td}])}}        
                                        @else 
                                            {{$value->{$td}; }}
                                        @endif  
                                  </td>
                                  @endif
                              @endforeach 
                              {{--Foreach dos Botoes de açoes--}}
                              <td class="numeric">
                                <div class="pull-right hidden-phone">
                                	<?php                                                                 
                                  if($value->status=='Aprovado')
                                  {
                										$btns[0] = 
                											[
                												'link'=>'corretor.make.proposta',
                												'text'=>'<i class="fa fa-gavel"></i>',
                												'array'=>
                												[
                													'class'=>'btn btn-info btn-xs tooltips',
                													'data-original-title'=>'Reserva aceita, preencha a proposta',
                													'data-placement'=>"top"
                												]
                											];
                                	}else{
                                		$btns = $data['buttons'];
                                	}
                                	?>
                                    @foreach ($btns as $btn)

                                      @if(!isset($btn['modal']) && (!isset($btn['sub'])))
                                        {{HTML::decode(HTML::link(URL::route($btn['link'],$value->id),$btn['text'],$btn['array']))}}
                                      @elseif(!isset($btn['modal']))  
                                        {{HTML::decode(HTML::link(URL::route($btn['link'],[$value->{$btn['sub']},$value->id]),$btn['text'],$btn['array']))}}
                                      @else
                                        {{HTML::decode(HTML::link($btn['link'],$btn['text'],$btn['array']+['rel'=>$value->id]+['data-url'=>$data['destroy']]))}}
                                      @endif
                                    @endforeach
                                </div>                        
                              </td>
                            </tr>
                        @endif    
                    @endforeach                                            
                    </tbody>
                </table>
                  @if(!isset($data['not-paging']))
                    {{$data['model']->links()}}
                  @endif
                </section>
            </div>
        </section>
          @include('admin::includes.modal')        
    </div>
</div>


@stop

@section('documentReady')


	$.ajax({
	  type: "POST",
	  url: '{{route("ajax.corretores.get.clientes")}}',
	})
  	.done(function( msg ) {
    	$("#cliente").html(msg)
  	});		

	var id = "";
	var url = "";
	$(".delModal").on('click',function(e){
		id = $(this).attr('rel');
		url = $(this).attr('data-url');
		console.log(url);
	});
	$(".delete").on("click",function(){
		$.ajax({
		  type: "POST",
		  url: url,
		  data: { 
		  	id:id,
		  	cliente_id:$("#cliente").val()
		  }
		})
	  	.done(function( msg ) {
	    	window.location.href = '{{route("corretor.lista.reserva")}}'
	  	});		
	})


@stop