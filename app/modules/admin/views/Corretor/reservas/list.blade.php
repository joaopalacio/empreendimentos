@extends('admin::layouts.master')

@section('content')
	<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Ficha cadastral do cliente
            </header>
            <div class="panel-body">
                <div class=" form">
                      <!--tab nav start-->
                      <section class="panel">
                          <header class="panel-heading tab-bg-dark-navy-blue ">
                              <ul class="nav nav-tabs">
                                  <li class="active">
                                      <a data-toggle="tab" href="#Cliente">Cliente</a>
                                  </li>
                                  <li class="">
                                      <a data-toggle="tab" href="#Conjuge">Conjuge</a>
                                  </li>
                              </ul>
                          </header>
                          <div class="panel-body">
                              <div class="tab-content">
                                  <div id="Cliente" class="tab-pane active">

                			               @foreach ($data['elements'] as $element)

                			                               {{--Input type text--}}
                										    <div class="form-group ">
                										        <label for="cname" class="control-label col-lg-2">
                										        	{{$element['campo']}}
                										        </label>
                										        <div class="col-lg-10">
                										            
                										            <p class="form-control">{{ @$data['model']['cliente']->{$element['name']} }}</p>
                										        </div>
                										    </div> 
                			               @endforeach
                                  </div>
                                  <div id="Conjuge" class="tab-pane">
			                        @foreach ($data['elements'] as $element)

			                               {{--Input type text--}}
            										    <div class="form-group ">
            										        <label for="cname" class="control-label col-lg-2">
            										        	{{$element['campo']}}
            										        </label>
            										        <div class="col-lg-10">
            										            
            										            <p class="form-control">{{ @$data['model']['conjuge']->{$element['name']} }}</p>
            										        </div>
            										    </div> 
			                        @endforeach
                                  </div>
                                  <div id="profile" class="tab-pane">Profile</div>
                                  <div id="contact" class="tab-pane">Contact</div>
                              </div>
                          </div>

                        
                </div>

            </div>
        </section>
    </div>
</div>
@stop


@section('documentReady')



@stop