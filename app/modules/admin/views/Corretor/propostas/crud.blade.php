@extends('admin::layouts.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                {{isset($data->formTitle) ? $data->formTitle : 'Formulario de Proposta a ser preenchido' }}
            </header>
            <div class="panel-body cmxform form-horizontal tasi-form">
                <div class=" form">
                    @if ( $errors->count() > 0 )
                      <div class="alert alert-block alert-danger fade in">
                          <button data-dismiss="alert" class="close close-sm" type="button">
                              <i class="fa fa-times"></i>
                          </button>
                          <strong>Erro</strong>
                          <ul>
                            @foreach( $errors->all() as $message )
                              <li>{{ $message }}</li>
                            @endforeach
                          </ul>                    
                      </div>              
                    @endif
					<?php setlocale(LC_MONETARY, 'pt_BR');?>                    
                       	{{--Nome do Cliente--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Cliente</label>
					  <div class="col-lg-10">
					      <p class="form-control-static">{{$data['Reserva']->cliente->name}}</p>
					  </div>
					</div>              
                	{{--CPF do Cliente--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">CPF</label>
					  <div class="col-lg-10">
					      <p class="form-control-static">{{$data['Reserva']->cliente->cpf}}</p>
					  </div>
					</div>    
                	{{--Empreendimento--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Empreendimento</label>
					  <div class="col-lg-10">
					      <p class="form-control-static">{{$data['Reserva']->imovel->empreendimento->nome}}</p>
					  </div>
					</div> 
                	{{--Imovel--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Imovel</label>
					  <div class="col-lg-10">
					      <p class="form-control-static">{{$data['Reserva']->imovel->tipo}} ({{$data['Reserva']->imovel->area_privativa}} m2)</p>
					  </div>
					</div>
                	{{--Detalhes--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Quartos</label>
					  <div class="col-lg-10">
					      <p class="form-control-static">{{$data['Reserva']->imovel->quartos}}</p>
					  </div>
					</div>   
                	{{--Valor do Imovel--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Valor total</label>
					  <div class="col-lg-10">
					      <p class="form-control-static">{{UtilApp::Money($data['RegrasProposta']->valor_imovel,false)}}</p>
					  </div>
					</div>   	
                	{{--Valor do Imovel--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Valor Desconto</label>
					  <div class="col-lg-10 ">
					      <p class="form-control-static com_desconto">{{UtilApp::Money($data['RegrasProposta']->valor_imovel,false)}}</p>
					  </div>
					</div> 		
	            	{{--Valor Total a ser financiado do Imovel--}}
					<div class="form-group">
					  <label class="col-lg-2 control-label">Valor Financiado</label>
					  <div class="col-lg-10 valor_final">
					      
					  </div>
					</div> 															 
				</div>	
			</div>
		    <div class="panel-body">
		    	<div class="stepy-tab">
                  	<ul id="default-titles" class="stepy-titles clearfix">
                    	<li id="default-title-0" class="current-step">
                          <div>Desconto</div>
                      	</li>
                      	<li id="default-title-1" class="">
                          <div>Step 2</div>
                      	</li>
                      	<li id="default-title-2" class="">
                       	   <div>Step 3</div>
                      	</li>
                 	</ul>
		    	</div>
		                 
				{{Form::open(['url' => '','method'=>isset($data->method) ? $data->method : 'post','class'=>'cmxform form-horizontal tasi-form','id'=>'default'])}}                       													
		        	<fieldset title="Desconto" class="step" id="default-step-0">
		            	<legend> </legend>
						{{--DETALHES DA PROPOSTA--}}   

		            	{{--Desconto padrão do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Desconto Imovel <small>(max: {{UtilApp::Porcent($data['RegrasProposta']->desconto_padrao,false)}})</small> </label>
						  <div class="col-lg-10">
						      {{Form::text('desconto_padrao','',['class'=>'form-control percent', 'data-v-min'=>"0", 'data-v-max'=>$data['RegrasProposta']->desconto_padrao,'data-a-sep'=>"",'data-p-sign'=>"s",'data-a-sign'=>" %",'data-m-dec'=>"2"])}}
						  </div>
						</div>   
 			
 			

		
							{{--
							- Preenchimento de proposta, definindo condições de pagamento, dentro das regras pré-

							estabelecidas pelo administrador para aquele determinado imóvel; 

							- Caso o administrador tenha permitido o desconto padrão, aparecerá um box para selecionar 

							o desconto oferecido; 

							- Box para seleção do desconto padrão, que concederá desconto para o pagamento "no ato" 

							- Quando o corretor for preencher a proposta, os valores das parcelas vão aparecer conforme 

							o default definido para cada empreendimento. O procedimento para cadastro de parcelas é: 

							Primeiro o corretor coloca o valor ou a porcentagem total que será financiada com a 

							construtora. 

							Depois define a entrada, depois o FGTS, e depois as chaves. 

							Depois as intermediarias.

							O restante será dividido em parcelas iguais e o sistema calculará sozinho. 

							Cada vez que o corretor alterar um dos valores acima, as parcelas serão recalculadas; 

							- Definir se a proposta está sendo cadastrada com a comissão do corretor ou sem (uma 

							marcação na proposta)
							--}}
		           	</fieldset> 
                  	<fieldset title="Financia" class="step" id="default-step-1" >
                      <legend> </legend>
		            	{{--Quantos % sera financiado com a construtora do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">% Financiamento <small>(max: {{UtilApp::Porcent($data['RegrasProposta']->porcent_financiamento,false)}})</small> </label>
						  <div class="col-lg-10">
						      {{Form::text('porcent_financiamento',$data['RegrasProposta']->porcent_financiamento,['class'=>'form-control percent', 'data-v-min'=>"0", 'data-v-max'=>$data['RegrasProposta']->porcent_financiamento,'data-a-sep'=>"",'data-p-sign'=>"s",'data-a-sign'=>" %",'data-m-dec'=>"2"])}}
						  </div>
						</div> 	  
						{{--Prazo Maximo de financiamento--}}			
						<div class="form-group">
						  <label class="col-lg-2 control-label">Prazo <small>(max: {{$data['RegrasProposta']->prazo}} meses)</small></label>
						  <div class="col-lg-10">
						      {{Form::text('prazo',1,['class'=>'form-control prazo','data-v-max'=>$data['RegrasProposta']->prazo])}}
						  </div>
						</div> 						                    
                  	</fieldset>   			           	                 	
                  	<fieldset title="FGTS" class="step" id="default-step-2" >
                      <legend> </legend>
		            	{{--Quantos % sera financiado com a construtora do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Valor FGTS <small>(usará quanto do FGTS para pagar)</small> </label>
						  <div class="col-lg-10">
						      {{Form::text('fgts','',['class'=>'form-control money', 'data-v-min'=>"0",'data-a-sep'=>"",'data-p-sign'=>"s",'data-a-sign'=>" %",'data-m-dec'=>"2"])}}
						  </div>
						</div> 	  						                    
                  	</fieldset> 	        
                  	<fieldset title="Entrada" class="step" id="default-step-3" >
                  		<legend> </legend>
		            	{{--Valor da Entrada do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Valor da Entrada <small>(min: {{UtilApp::Money($data['RegrasProposta']->entrada_minima,false)}})</small></label>
						  <div class="col-lg-10">
						      {{Form::text('entrada_minima',$data['RegrasProposta']->entrada_minima,['class'=>'form-control money',/*'data-v-min'=>$data['RegrasProposta']->entrada_minima*/])}}
						  </div>
						</div>                     	
                  	</fieldset>
                  	<fieldset title="Chaves" class="step" id="default-step-4" >
                  		<legend> </legend>
		            	{{--Valor das Chaves do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Valor da Chaves <small>(max: {{UtilApp::Money($data['RegrasProposta']->chaves_maxima,false) }})</small></label>
						  <div class="col-lg-10">
						      {{Form::text('chaves_maxima','',['class'=>'form-control money','data-v-max'=>$data['RegrasProposta']->chaves_maxima])}}
						  </div>
						</div>                     		
                  	</fieldset>     
                  	<fieldset title="Intermediarias" class="step" id="default-step-5" >
                      <legend> </legend>
						{{--Listar as Intermediarias--}}			
						<div class="form-group">
						  <label class="col-lg-2 control-label">Intermediarias 
						  		<button data-toggle="button" class="btn btn-plus">
                                  <i class="fa fa-plus text-info"></i>                                   
                              	</button></label>
						  <div class="col-lg-10">
						      
						  </div>
						</div> 	                      
						<?php $soma = 0; $count=0;?>
						{{--Listar as Intermediarias--}}
						@if(count($data['RegrasProposta']->intermediarias))
							@foreach ($data['RegrasProposta']->intermediarias as $element)
								<div class="form-group listaIntermediarias">
								  <label class="col-lg-2 control-label">Mês / Valor</label>
							        <div class="col-lg-1">
							            <div class="spinners">
							                <div class="input-group input-small">                                    
							                    {{Form::text('intermediarias['.$count.'][mes]',$element->mes,['class'=>'spinner-input form-control intermediariasMes_'.$count.' intermediarias'])}}
							                    <div class="spinner-buttons input-group-btn btn-group-vertical">
							                        <button type="button" class="btn spinner-up btn-xs btn-default">
							                            <i class="fa fa-angle-up"></i>
							                        </button>
							                        <button type="button" class="btn spinner-down btn-xs btn-default">
							                            <i class="fa fa-angle-down"></i>
							                        </button>
							                    </div>
							               </div>
							             </div>                                               
							        </div>
								  <div class="col-lg-2">
									  <div class="input-group m-bot12">
		                                  {{Form::text('intermediarias['.$count.'][description]','',['class'=>'form-control descriptionIntermediaria ','placeholder'=>'Descrição (opcional)','data-count'=>$count])}}		                                  
		                               </div>
								  </div>								        
								  <div class="col-lg-3">
									  <div class="input-group m-bot15">
		                                  <span class="input-group-addon">$</span>
		                                  {{Form::text('intermediarias['.$count.'][valor]',$element->valor,['class'=>'form-control intermediarias intermediariasValor money ','data-count'=>$count])}}
		                                  <span class="input-group-addon"></span>
		                                  <?php 
		                                  	$soma += $element->valor;
		                                  	$count++;
		                                  ?>
		                               </div>
								  </div>									  
								</div> 																					
							@endforeach	
						@endif		     
						{{--Total do valor a ser pago nas intermediarias--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Valor Total Intermediarias</label>
						  <div class="col-lg-10 total-intermediarias">
						      {{UtilApp::Money($soma,false)}}
						  </div>
						</div> 							                 
                  	</fieldset>   
                  	<fieldset title="Parcelas" class="step" id="default-step-6" >
                  		<legend> </legend>
		            	{{--Valor da FGTS do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">FGTS</label>
						  <div class="col-lg-10 fgts_final">
						      
						  </div>
						</div> 		
		            	{{--Porcentagem Financiada com empresa--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">% Financiamento</label>
						  <div class="col-lg-10 porcent_final">
						      
						  </div>
						</div> 						
		            	{{--Valor da Prazo do Financiamento--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Prazo</label>
						  <div class="col-lg-10 prazo_final">
						      
						  </div>
						</div> 		
		            	{{--Desconto dado com empresa--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">% Desconto</label>
						  <div class="col-lg-10 desconto_final">
						      
						  </div>
						</div> 																	
		            	{{--Valor da Entrada do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Entrada</label>
						  <div class="col-lg-10 entrada_final">
						      
						  </div>
						</div> 							
		            	{{--Valor da Chaves do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Chaves</label>
						  <div class="col-lg-10 chaves_final">
						      
						  </div>
						</div> 						
		            	{{--Valor da Intermediarias do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Intermediarias</label>
						  <div class="col-lg-10 intermediarias_final">
						      
						  </div>
						</div> 			
											
		            	{{--Valor da Parcelas do Imovel--}}
						<div class="form-group">
						  <label class="col-lg-2 control-label">Parcelas</label>
						  <div class="col-lg-10 parcelas_final">
						      
						  </div>
						</div> 								
                  	</fieldset>                   	          
                  	<input type="submit" class="finish btn btn-danger" value="{{Lang::get('admin.defaults.save')}}"/>
              	{{Form::close()}}
		    </div>
		</section>     																																														                         
	</div>           
</div>
@stop

@section('documentReady')
      $('#default').stepy({
          backLabel: 'Anterior',
          block: true,
          nextLabel: 'Proximo',
          titleClick: true,
          titleTarget: '.stepy-tab'
      });

      $("input").on('focusout',function(e){      	
      	recalcula();
      });
      
      recalcula();

	  /*Soma as Intermediarias*/
      function intermediarias(){
		var val,intermediarias =0;
      	$(".intermediariasValor").each(function(){      	
      		val = LimpaMoney($(this).val());      	
      		intermediarias+=parseFloat(val);					
		});    
		return intermediarias;  
      }

      $(".intermediarias").on('blur',function(e){
      	$(".total-intermediarias").empty().html("R$ "+intermediarias().toFixed(2));
      });

      $(".btn-plus").on('click',function(e){
      	e.preventDefault();
      	$(".listaIntermediarias").last().after('<div class="form-group listaIntermediarias"><label class="col-lg-2 control-label">Mês / Valor</label><div class="col-lg-1"><div class="spinners"><div class="input-group input-small">                                    {{Form::text('prazo[]',$element->mes,['class'=>'spinner-input form-control mes_'.$count])}}<div class="spinner-buttons input-group-btn btn-group-vertical"><button type="button" class="btn spinner-up btn-xs btn-default"><i class="fa fa-angle-up"></i></button><button type="button" class="btn spinner-down btn-xs btn-default"><i class="fa fa-angle-down"></i></button></div></div></div>                                               </div><div class="col-lg-3"><div class="input-group m-bot15"><span class="input-group-addon">$</span>{{Form::text('intermediaria[]',$element->valor,['class'=>'form-control intermediarias money ','data-count'=>$count])}}<span class="input-group-addon"></span></div></div></div> ');
      });

      function recalcula(){
      	$(".fgts_final").html($("input[name='fgts']").val());
      	$(".prazo_final").html($("input[name='prazo']").val());
      	$(".desconto_final").html($("input[name='desconto_padrao']").val());
      	$(".porcent_final").html($("input[name='porcent_financiamento']").val());
      	$(".entrada_final").html($("input[name='entrada_minima']").val());
      	$(".chaves_final").html($("input[name='chaves_maxima']").val());
      	
      	
		var intermediarias = 0;
		var totalPago      = 0;
		var html = '';

		/*Soma as Intermediarias*/
      	$(".intermediariasValor").each(function(){      	
      		val = LimpaMoney($(this).val());      	
      		intermediarias+=parseFloat(val);		
			html+='<div class="form-group"><label class="col-lg-2 control-label">Mês '+$('.intermediariasMes_'+$(this).attr('data-count')).val()+'</label><div class="col-lg-10">'+$(this).val()+'</div></div>';						
		});
		$(".intermediarias_final").empty().html(html);
		html='';      	

      	/*FGTS */
      	totalPago += LimpaMoney($("input[name='fgts']").val());

      	/*Entrada*/
      	totalPago += LimpaMoney($("input[name='entrada_minima']").val());

		/*Chaves*/
      	totalPago += LimpaMoney($("input[name='chaves_maxima']").val());  	
      	
      	/*Valor Imovel*/
      	var valor_imovel = parseFloat('{{$data['RegrasProposta']->valor_imovel}}');

      	/*DESCONTO*/
      	var desconto 	 = LimpaMoney($("input[name='desconto_padrao']").val());
      	var pagar = valor_imovel;
      	var total_desconto = 0
      	if(!isNaN(desconto)){
      		total_desconto = valor_imovel*desconto/100;
      		pagar = (valor_imovel-total_desconto);
      	}
      	$('.com_desconto').empty().html("R$ "+pagar.toFixed(2));
      	totalPago = totalPago+intermediarias;
      	
      	/*Valor Financiado */      	
      	var percent_financiado = LimpaMoney($("input[name='porcent_financiamento']").val());
      	var parcelado  = (pagar*percent_financiado/100)-totalPago;

		/*CADA PARCELA*/
		var prazo = parseInt($("input[name='prazo']").val());
		console.log('PRAZO'+prazo);
		$(".valor_final").empty().html("R$ "+parcelado.toFixed(2));
		html = '';

		if(parcelado<1000)
		{
			$(".finish").hide();
			$(".valor_final").empty().html('<b>Valor a parcelar está incorreto, abaixo de R$ 1.000,00</b>');
			return false;
		}
		else
		{
			$(".finish").show();
		} 


		for(i=0;i < prazo;i++){
			html+='<div class="form-group"><label class="col-lg-2 control-label">Parcela '+(i+1)+'</label><div class="col-lg-10 parcelas_final">R$ '+(parcelado/prazo).toFixed(2)+'</div></div>';
		}
		$(".parcelas_final").html(html);
      	console.log("FGTS = "+LimpaMoney($("input[name='fgts']").val()));      	
		console.log("ENTRADA = "+LimpaMoney($("input[name='entrada_minima']").val()));      	      	
		console.log("CHAVES = "+LimpaMoney($("input[name='chaves_maxima']").val()));      	          	
      	console.log("TOTAL = "+valor_imovel);      	      	      	
      	console.log("DESCONTO = "+desconto);      	      	
      	console.log("Valor Desconto= "+total_desconto);      	      	
      	console.log("TOTAL PAGO = "+((totalPago)+(intermediarias)));	      	
		console.log("A PARCELAR = "+parcelado);         			
		console.log("A PARCELAR = "+(parcelado/prazo).toFixed(2));   
     			
      }
      

      $(".finish").on('click',function(e){
      	e.preventDefault();


      	$.ajax({
      		type:'POST',
      		url :'{{route("corretor.store.proposta",$data['Reserva']->id)}}',
      		data:
      		{
      			'fgts':LimpaMoney($("input[name='fgts']").val()),
      			'desconto':LimpaMoney($("input[name='desconto_padrao']").val()),
      			'entrada':LimpaMoney($("input[name='entrada_minima']").val()),
      			'chaves':LimpaMoney($("input[name='chaves_maxima']").val()),
      			'porcent':LimpaMoney($("input[name='porcent_financiamento']").val()),
      			'prazo':LimpaMoney($("input[name='prazo']").val()),
      			'intermediarias':decodeURI($(".intermediarias").serialize()),
      			'meses':decodeURI($(".meses").serialize()),
      		},
      	}).done(function( data ) {
    		if(data=='1'){
      				window.location.href = "{{route('corretor.lista.reserva')}}";
      		}
      	
		});



      });

      {{--      	console.log("Intermediarias = "+intermediarias);
      	console.log("FGTS = "+LimpaMoney($("input[name='fgts']").val()));      	
		console.log("ENTRADA = "+LimpaMoney($("input[name='entrada_minima']").val()));      	      	
		console.log("CHAVES = "+LimpaMoney($("input[name='chaves_maxima']").val()));      	          	
      	console.log("TOTAL = "+valor_imovel);      	      	      	
      	console.log("DESCONTO = "+desconto);      	      	
      	console.log("Valor Desconto= "+total_desconto);      	      	
      	console.log("TOTAL PAGO = "+((totalPago)+(intermediarias)));	      	
		console.log("A PARCELAR = "+parcelado);         			
		console.log("A PARCELAR = "+(parcelado/prazo).toFixed(2));         			--}}

    function LimpaMoney(value){
		value = value.replace(".", ""); 
		value = value.replace(".", ""); 
		value = value.replace(".", ""); 
		value = value.replace(",", "."); 
		value = value.replace("R$", ""); 
		return parseFloat(value);
    }      

@stop
