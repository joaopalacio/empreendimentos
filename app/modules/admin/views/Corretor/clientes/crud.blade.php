@extends('admin::layouts.master')

@section('content')
	@include('admin::includes.form',array('data'=>$data))
@stop


@section('documentReady')
	$(".cep").on('change',function(e){
		$.getJSON('http://cep.correiocontrol.com.br/'+$(this).val().replace('-','')+'.json',function(data){
			$("#endereco").val(data.logradouro);
			$("#bairro").val(data.bairro);
			$("#municipio").val(data.localidade);
			$("#uf").val(data.uf);
		})
	});
	$(".cep_com").mask('99999-999');
	$(".cep_com").on('change',function(e){
		$.getJSON('http://cep.correiocontrol.com.br/'+$(this).val().replace('-','')+'.json',function(data){
			$("#endereco_com").val(data.logradouro);
			$("#bairro_com").val(data.bairro);
			$("#municipio_com").val(data.localidade);
			$("#uf_com").val(data.uf);
		})
	})	
@stop