@extends('admin::layouts.master')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Lista de imoveis adquiridos
            </header>
            @if (isset($data['insert']))
              <div class="panel-body" style="text-align: right;">
                  <a href="{{HTML::decode($data['insert'])}}" class="btn btn-primary"><i class="fa fa-cloud"></i> Novo Registro</a>
              </div> 
            @endif
           
            <div class="panel-body">
                <section id="unseen">
                  <table class="table table-bordered table-striped table-condensed">
                    <thead>
                    <tr>
                       <th class="">Empreendimento</th>
                       <th class="">Imovel</th>
                       <th class="">Codigo</th>
                       <th class="numeric"></th>
                    </tr>
                    </thead>
                    <tbody>
                    {{--Foreach dos valores do model passado em data[model]--}}
						@if(isset($model))
							@foreach ($model as $element)
								<td>{{$element->empreendimentos_nome}}</td>
								<td>{{$element->tipo}}</td>
								<td>{{$element->chave_unica}}</td>
								<td>
									<div class="pull-right hidden-phone">
										<a href="{{route('clientes.escolhe.imovel',$element->proposta_id)}}" class="btn btn-success"><i class="fa fa-eye"></i> Ver </a>
									</div>
								</td>
							@endforeach
						@endif
                    </tbody>
                </table>
                </section>
            </div>
        </section>
          @include('admin::includes.modal')        
    </div>
</div>
@stop