
  <div class="text-center">
      2014 &copy; {{HTML::link('http://andrelembo.com.br', 'Desenvolvimento Andre Lembo',['style'=>'color:#F77B6F'])}}
      <a href="#" class="go-top">
          <i class="fa fa-angle-up"></i>
      </a>
  </div>
  
<!-- js placed at the end of the document so the pages load faster -->
    {{HTML::script('assets/back/js/jquery.js')}}
    {{HTML::script('assets/back/js/bootstrap.min.js')}}
    {{HTML::script('assets/back/js/jquery.dcjqaccordion.2.7.js')}}
    {{HTML::script('assets/back/assets/fancybox/source/jquery.fancybox.js')}}
    {{HTML::script('assets/back/js/jquery.scrollTo.min.js')}}
    {{HTML::script('assets/back/js/jquery.nicescroll.js')}}
    {{HTML::script('assets/back/js/jquery.sparkline.js')}}
    {{HTML::script('assets/back/assets/fuelux/js/spinner.min.js')}}    

    {{HTML::script('assets/back/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js')}}
    {{HTML::script('assets/back/js/owl.carousel.js')}}
    <!--common script for all pages-->
    {{HTML::script('assets/back/js/jquery.maskMoney.min.js')}} 
    {{HTML::script('assets/back/js/jquery.numeric.js')}} 
    {{HTML::script('assets/back/assets/jquery-multi-select/js/jquery.multi-select.js')}}     
    <!--script for this page only-->
    {{HTML::script('assets/back/js/sparkline-chart.js')}}
    {{HTML::script('assets/back/js/easy-pie-chart.js')}}
   <!--script for this page only-->
   {{HTML::script('assets/back/assets/bootstrap-datepicker/js/bootstrap-datepicker.js')}}
    {{HTML::script('assets/back/js/modernizr.custom.js')}}    
    {{HTML::script('assets/back/js/toucheffects.js')}}        
    {{HTML::script('assets/jquery-maskedinput/src/jquery.maskedinput.js')}}
    {{HTML::script('assets/back/js/respond.min.js')}}
    {{HTML::script('assets/back/js/jquery.stepy.js')}}
    {{HTML::script('assets/back/js/bootstrap-switch.js')}}
    {{HTML::script('assets/back/js/autoNumeric.js')}}
    {{HTML::script('assets/back/js/common-scripts.js')}}

    @yield('scripts')