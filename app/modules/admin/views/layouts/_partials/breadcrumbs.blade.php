              <div class="row">                               
                  <div class="col-lg-12">
                      <!--breadcrumbs start -->
                      <ul class="breadcrumb">
                      @if (isset($breadcrumb))
                          @foreach ($breadcrumb as $element)                          
                            <li class="{{$element['active']}}">
                                <a href="{{$element['url']}}">
                                    <i class="fa {{$element['class']}}"></i> {{$element['texto']}}
                                </a>
                            </li>                            
                          @endforeach                      
                      @endif
                      </ul>
                      <!--breadcrumbs end -->
                  </div>
              </div>