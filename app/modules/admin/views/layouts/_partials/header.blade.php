<!--header start-->
    <div class="sidebar-toggle-box">
        <div data-original-title="Menu de navegação" data-placement="right" class="fa fa-bars tooltips"></div>
    </div>
    <!--logo start-->
    <a href="#" class="logo">Maschio<span>Shin</span></a>
    <!--logo end-->

              <div class="top-nav ">
                  <ul class="nav pull-right top-menu">
                      <li>
                          <input type="text" class="form-control search" placeholder="Buscar">
                      </li>
                      <!-- user login dropdown start-->
                      @include('admin::includes.profile')                      
                      <!-- user login dropdown end -->
                  </ul>
              </div>

<!--header end-->
 