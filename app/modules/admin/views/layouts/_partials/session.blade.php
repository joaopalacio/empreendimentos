@if (Session::has('message'))
	<div class="alert alert-success alert-block fade in">
		  <button data-dismiss="alert" class="close close-sm" type="button">
		      <i class="fa fa-times"></i>
		  </button>
		  <h4>
		      <i class="fa fa-ok-sign"></i>
		      Success!
		  </h4>
		  <p>{{Session::get('message')}}</p>
	</div>
@endif

@if (Session::has('info'))
	<div class="alert alert-warning alert-block fade in">
		  <button data-dismiss="alert" class="close close-sm" type="button">
		      <i class="fa fa-times"></i>
		  </button>
		  <h4><i class="fa fa-ok-sign"></i>Informações!</h4>
		  <p>{{Session::get('info')}}</p>
	</div>
@endif