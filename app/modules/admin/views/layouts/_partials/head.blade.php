<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{{ isset($description) ? $description : 'Seja bem vindo ao sistema de gerenciamento de empreendimentos.' }}}">
    <meta name="author" content="Joao Lucas Palacio Soares">
    <meta name="keyword" content="{{{ isset($keywords) ? $keywords : 'sistema,erp' }}}">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>{{{ isset($title) ? $title : 'Sistema de Gerenciamento de Empreendimentos' }}}</title>
    <!-- Bootstrap core CSS -->
    {{HTML::style('assets/back/css/bootstrap.min.css')}}    
    {{HTML::style('assets/back/css/bootstrap-reset.css')}}

    <!--external css-->
    {{HTML::style('assets/back/css/owl.carousel.css')}}
    {{HTML::style('assets/back/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css')}}
    {{HTML::style('assets/back/assets/font-awesome/css/font-awesome.css')}}
    {{HTML::style('assets/back/assets/jquery-multi-select/css/multi-select.css')}}   
    {{HTML::style('assets/back/assets/jquery-ui/jquery-ui-1.10.1.custom.min.css')}}        
    {{HTML::style('assets/back/assets/bootstrap-datepicker/css/datepicker.css')}}    
    <!-- Custom styles for this template -->
    {{HTML::style('assets/back/css/style.css')}}
    {{HTML::style('assets/back/css/style-responsive.css')}}
    @yield('styles')
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      {{HTML::script('assets/back/js/html5shiv.js')}}
      {{HTML::script('assets/back/js/respond.min.js')}}      
    <![endif]-->
  </head>