<!-- Stored in app/modules/admin/views/layouts/master.blade.php -->
<html>
	@include('admin::layouts._partials.head')
  	<body>
	  	<section id="container" class="">
	  		<!--header start-->
      		<header class="header white-bg">
      			@include('admin::layouts._partials.header')	
      		</header>
      		<!--header end-->

      		<!--sidebar start-->
	    	<aside>
	    		@include('admin::layouts._partials.sidebar')
	    	</aside>	 
	    	<!--sidebar end-->

			<!--main content start-->
	        <section id="main-content">
	          <section class="wrapper site-min-height">
		      	@include('admin::layouts._partials.session')			      
				@include('admin::layouts._partials.breadcrumbs')			      
		        @yield('content')		         
		      </section>		      
		    </section>
			<!--main content end-->

	      	<!--footer start-->
	      	<footer class="site-footer">
	        	@include('admin::layouts._partials.footer')
	      	</footer>
	      	<!--footer end-->

		    <script>
		        $(document).ready(function(){
		            @yield('documentReady');
		            
		            function loadStart(){
		            	$("#loading").show();
		            	$(".modal-backdrop").show();
		            }
		            function loadStop(){
		            	$("#loading").hide();
		            	$(".modal-backdrop").hide();
		            }
		        })        
		    </script>
		 	 @yield('scriptsExtra')       
		</section>  


<div class="modal-backdrop fade in" style="display: none;"></div>
<div class="modal fade in" id="loading" style="display: none;top:30%">
  <div class="modal-dialog">
      <div class="modal-content">

          <div class="modal-body">

              Carregando...<br/>
              Aguarde so mais um pouco...
          </div>
      </div>
  </div>
</div>

    </body>
</html>
