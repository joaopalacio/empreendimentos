<?php

namespace App\Modules\Admin\Controllers\Admin;
use View,Formulario,User,UtilApp,Config,Request,Carbon,Role,Auth,Session,Hash,Validator,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Formulario');
class FormularioController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }


	/**
	 * Show the form for creating a new Clientees
	 *
	 * @return Response
	 */
	public function show($id)
	{
		Session::put('url', '/admin/Clientees/create');
		$user = User::find($id);		
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.lista.reserva'),'class'=>'active','active'=>'active','texto'=>'Reservas'],
			['url'=>URL::route($this->model->settings['Cliente']['index.url']),'class'=>'active','active'=>'active','texto'=>'Formulario de dados do Cliente '.$user->name],
		];				  
		$this->layout->with(compact('breadcrumb'));
		$data = [
			'title'=>$this->model->settings['Cliente']['title'],
			'formTitle'=>$this->model->settings['Cliente']['formTitle'],
			'route'=>URL::route($this->model->settings['Cliente']['update.url'],$id),
			'model'=>
			[
				'cliente' => Formulario::whereUserId($id)->where('tipo','=','Proposta')->first(),
				'conjuge' => Formulario::whereUserId($id)->where('tipo','=','Conjuge')->first()
			],
			'elements'=>$this->model->editColumns['Cliente']
		];	


		$this->layout->content = View::make(Config::get('views_admin.reservas.show'), compact('data'));
	}




}