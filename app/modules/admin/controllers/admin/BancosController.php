<?php

namespace App\Modules\Admin\Controllers\Admin;
use View,Banco,Session,Validator,Config,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Banco');

class BancosController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }

	/**
	 * Display a listing of bancos
	 *
	 * @return Response
	 */
	public function index()
	{
		Session::put('url', '/admin/bancos');

		$breadcrumb = [
			['url'=>'admin.index','class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>'admin.index','class'=>'','active'=>'','texto'=>'Sistema'],			
			['url'=>$this->model->settings['index.url'],'class'=>'active','active'=>'active','texto'=>$this->model->settings['model']],
		];					  
		$this->layout->with(compact('breadcrumb'));

		$model = $this->model->paginate(30);
		$data = [
			'title'=>$this->model->settings['title'],
			'thead'=>$this->model->settings['thead'],
			'insert'=>URL::route($this->model->settings['insert.url']), 
			'td'=>$this->model->tableColumns,
			'destroy'=>URL::route($this->model->settings['destroy.url'],''), 			
			'modal'=>$this->model->settings['modal'],
			'buttons'=>$this->model->btnsColumns,
			'model'=>$model
		];

		$this->layout->content = View::make(Config::get('views_admin.bancos.lista'), compact('data'));
	}

	/**
	 * Show the form for creating a new Banco
	 *
	 * @return Response
	 */
	public function create()
	{
		Session::put('url', '/admin/bancos/create');

		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],			
			['url'=>URL::route($this->model->settings['index.url']),'class'=>'active','active'=>'active','texto'=>$this->model->settings['model']],
			['url'=>URL::route($this->model->settings['store.url']),'class'=>'active','active'=>'active','texto'=>'Novo Registro'],
		];				  
		$this->layout->with(compact('breadcrumb'));

		$data = [
			'title'=>$this->model->settings['title'],
			'formTitle'=>$this->model->settings['formTitle'],
			'route'=>URL::route($this->model->settings['store.url']), 
			'elements'=>$this->model->editColumns
		];

		$this->layout->content = View::make(Config::get('views_admin.bancos.insert'), compact('data'));
	}

	/**
	 * Store a newly created Banco in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $model = new $this->model;
        if ($model->save()) {
            return Redirect::route($this->model->settings['index.url'])->with('message', 'Registro gravado com Sucesso!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }
	}

	/**
	 * Show the form for editing the specified Banco.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$modelFind = Banco::find($id);
		Session::put('url', '/admin/bancos/create');

		$breadcrumb = [
			['url'=>'admin.index','class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>'admin.index','class'=>'','active'=>'','texto'=>'Sistema'],			
			['url'=>$this->model->settings['index.url'],'class'=>'','active'=>'','texto'=>$this->model->settings['model']],
			['url'=>$this->model->settings['store.url'],'class'=>'active','active'=>'active','texto'=>'Editar Registro '.$this->model->settings['model']],
		];				  
		$this->layout->with(compact('breadcrumb'));

		$model = $this->model->paginate(30);
		$data = [
			'title'=>$this->model->settings['title'],
			'formTitle'=>$this->model->settings['formTitle'],
			'route'=>URL::route($this->model->settings['update.url'],$id),
			'method'=>'PUT',
			'model'=>$modelFind,
			'elements'=>$this->model->editColumns
		];

		$this->layout->content = View::make(Config::get('views_admin.bancos.insert'), compact('data'));
	}



	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$model = $this->model->findOrFail($id);
		if ($model->updateUniques()) {
            return Redirect::route($this->model->settings['index.url'])->with('message', 'Registro alterado com Sucesso!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->model->destroy($id);

		return Redirect::back();
	}


}