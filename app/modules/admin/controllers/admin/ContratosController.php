<?php

namespace App\Modules\Admin\Controllers\Admin;
use View,Proposta,Contrato,Event,Image,Parcela,Intermediaria,Carbon\Carbon,UtilApp,Imovel,Request,File,UploadImage,Config,User,RegrasProposta,DB,Auth,Hash,RegraIntermediaria,Empreendimento,Session,Validator,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Contrato');

class ContratosController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }


	

	/**
	 * Show the form for creating a new cidade
	 *
	 * @return Response
	 */
	public function admin()
	{

		//TODO REGRAS DE NEGOCIO
		Session::put('url', '/admin/cidades/create');
		$model = $this->model->get();
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.lista.contratos'),'class'=>'','active'=>'','texto'=>'Contratos'],				
		];				  
		$this->layout->with(compact('breadcrumb'));

		$this->model->btnsColumns   = 
	  	[	
			[
				'link'=>'admin.proposta.informacoes',
				'text'=>'<i class="fa  fa-dollar"></i>',
				'array'=>
				[
					'class'=>'btn btn-success btn-s tooltips',
					'data-original-title'=>'Ver parcelas',
					'data-placement'=>"top",
					'data-toggle'=>"modal"
				]
			],
			[
				'link'=>'admin.contratos.receber',
				'text'=>'<i class="fa  fa-check"></i>',
				'array'=>
				[
					'class'=>'btn btn-info btn-s tooltips',
					'data-original-title'=>'Dar baixa em recebimentos',
					'data-placement'=>"top",
					'data-toggle'=>"modal"
				]
			],			
		];

		//THEAD alterando a ordem e colocando corretor
		$this->model->settings['thead'] = 
										[
											['class'=>'','name'=>'Empreendimento'],
											['class'=>'','name'=>'Cliente'],
											
											['class'=>'numeric','name'=>'Imovel'],
											['class'=>'numeric','name'=>'Corretor'],
											['class'=>'numeric','name'=>'']
										];
		//Seleciona todas reservas com nome do cliente,imovel,empreendimento e corretor								
		 $model = DB::table('contratos')
				->leftJoin('propostas', 'contratos.proposta_id', '=', 'propostas.id')
				->leftJoin('reserva', 'reserva.id', '=', 'propostas.reserva_id')
			    ->leftJoin('imoveis', 'imoveis.id', '=', 'reserva.imovel_id')
			    ->leftJoin('regras_propostas', 'imoveis.id', '=', 'regras_propostas.imovel_id')
	            ->leftJoin('empreendimentos', 'empreendimentos.id', '=', 'imoveis.empreendimento_id')
	            ->leftJoin('users as ' . DB::getTablePrefix() . 'corretor', 'corretor.id','=','reserva.corretor_id')
	            ->leftJoin('users', 'users.id', '=', 'reserva.user_id')
	            ->orderBy('data','DESC')	            
	            ->select('empreendimentos.nome as empreendimento','imoveis.tipo as imovel','users.name as cliente','reserva.status as status_venda','corretor.name as corretor','reserva.user_id as id','reserva.id as reserva_id','valor_imovel','propostas.*','propostas.created_at as data')
	            ->paginate(20);
		$data = [
			'title'=>'',
			'thead'=>$this->model->settings['thead'],
			'td'=>$this->model->tableColumns,
			'buttons'=>$this->model->btnsColumns,
			'model'=>$model
		];
		$this->layout->content = View::make(Config::get('views_admin.contratos.lista'), compact('data'));
	}

	public function baixa($id)
	{
		$Contrato = Contrato::with('parcelas','parcelas.boletos')->where('proposta_id','=',$id)->get()->toArray();
		$this->layout->content = View::make(Config::get('views_admin.contratos.parcelas'))->with(compact('Contrato'));  
	}

	public function darbaixa($id){
		$parcelas = Parcela::whereId($id)->first();
		$parcelas->pago = $parcelas->valor;
		$parcelas->save();
		return Redirect::back();
	}

	/**
	 * Show the form for creating a new cidade
	 *
	 * @return Response
	 */
	public function adminDocumento($id)
	{

		//TODO REGRAS DE NEGOCIO
		Session::put('url', '/admin/cidades/create');
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],	
			['url'=>URL::route('corretor.lista.reserva'),'class'=>'','active'=>'','texto'=>'Reservas'],
			['url'=>'#','class'=>'','active'=>'','texto'=>'Realizar Proposta'],						
		];				  
		$this->layout->with(compact('breadcrumb'));
		$Proposta = Proposta::find($id);		
		$RegrasProposta = RegrasProposta::with('intermediarias')->whereImovelId($Proposta->reserva->imovel_id)->first();		

		$data =
		[
			'Proposta' => $Proposta,
			'RegrasProposta' => $RegrasProposta
		];

		$this->layout->content = View::make(Config::get('views_admin.propostas.documento'), compact('data'));
	}


	public function destroy($id){
		$model = Proposta::with(
				[
					'reserva' => function($query)
					{
				    	$query->where('reserva.corretor_id','=',Auth::user()->id);
					},
					'intermediarias'
				])->find($id);
		$model->intermediarias()->delete();
		$model->delete();
		return Redirect::back();
	}

	
}