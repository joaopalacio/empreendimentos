<?php

namespace App\Modules\Admin\Controllers\Admin;
use View,Imovel,UtilApp,Banner,Image,Hashids,Request,File,UploadImage,Config,User,RegrasProposta,DB,Auth,Hash,RegraIntermediaria,Empreendimento,Session,Validator,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Banner');

class BannerController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }

	/**
	 * Show the form for creating a new cidade
	 *
	 * @return Response
	 */
	public function photos()
	{
		//TODO REGRAS DE NEGOCIO
		Session::put('url', '/admin/banners/');
		//TODO REGRAS DE NEGOCIO
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],	
			['url'=>URL::route('empreendimentos.index'),'class'=>'','active'=>'','texto'=>'Empreendimento'],			
		];				  
		$this->layout->with(compact('breadcrumb'));
		//$this->model->editColumns[10]['fieldSet'][8]['values'] = User::corretores()->select('id','name')->get();
		$data = [
			'title'=>'Upload de imagens e Galeria de imagens',
			'formTitle'=>$this->model->settings['formTitle'],
			'route'=>URL::route($this->model->settings['upload.url']),
			'elements'=>$this->model->editColumns,
			'dir_upload'=>Config::get('params.get.banners'),
			'model'=>Banner::all()
		];
		$this->layout->content = View::make(Config::get('views_admin.banners.photos'), compact('data'));
	}	

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */


	/*
	*	Paramentro id do Imovel
	*	@param $id
	*	Paramentro $_FILES
	*	@param Input::file()
	*	return JSON //Verificar Jquery FileUpload retorno em json
	*/
	public function upload(){
		$index = 0;
		$file = [];
		foreach(Input::file('files') as $value){	
			try{
				$url = md5(uniqid());
				$model = new Banner;
				$model->link = Request::get('title')[$index];
				$model->image = $url;				
				if($model->save()){
					$index++;
					Image::make($value->getRealPath())->fit(1700,400)->save(public_path()."/".Config::get('params.get.banners').$url.'.jpg');			
					Image::make($value->getRealPath())->fit(150,100)->save(public_path()."/".Config::get('params.get.banners').$url.'_small.jpg');			
					
					$file[] =     
				        [
				        'thumbnailUrl'=>"/".Config::get('params.get.banners').$url.'_small.jpg',
				        'url'=>"/".Config::get('params.get.banners').$url.'.jpg',
				        'name'=>$url.'.jpg'
				        ];    
				}else{
					$file[] =     
				        [
				        'thumbnailUrl'=>'1',
				        'error'=>json_encode($model->errors()->all()),
				        'url'=>'1',
				        'name'=>$url.".jpg",
				        'size'=>'1',
				        ];
				}

			}catch(Exception $e){
				$file[] =     
			        [
			        'thumbnailUrl'=>'1',
			        'error'=>'Ocorreu um erro ao enviar esta imagem',
			        'url'=>'1',
			        'name'=>$url.".jpg",
			        'size'=>'1',
			        ];    
			}				
		}		
		return json_encode(['files'=>$file]);
	}

	public function destroyImage(){
		if(Request::ajax()){
			$Image = Banner::find(Input::get('id'));						
			File::delete(public_path()."/".Config::get('params.upload.banners').$Image->url.'.jpg');
			$Image->delete();
			return 'Excluido com succeso';
		}
	}

}