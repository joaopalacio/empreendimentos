<?php

namespace App\Modules\Admin\Controllers\Admin;
use View,Reserva,Image,Imovel,Request,File,Config,User,RegrasProposta,DB,Auth,Hash,RegraIntermediaria,Empreendimento,Session,Validator,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Reserva');

class ReservasController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }

	/**
	 * Show the form for creating a new cidade
	 *
	 * @return Response
	 */
	public function admin()
	{

		//TODO REGRAS DE NEGOCIO
		Session::put('url', '/admin/cidades/create');
		$model = $this->model->minhasReservas()->get();
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],	
			['url'=>URL::route('empreendimentos.index'),'class'=>'','active'=>'','texto'=>'Lista de Reservas'],			
		];				  
		$this->layout->with(compact('breadcrumb'));
		$this->model->tableColumns = ['id','empreendimento','imovel','cliente','corretor'];
		$this->model->tableColumns = array_add($this->model->tableColumns,'dropdown',[]);
		$this->model->tableColumns['dropdown']['values'] = ['Aprovado','Recusado'];

		$this->model->btnsColumns   = 
	  	[	
			[
				'link'=>'admin.reserva.documentos',
				'text'=>'<i class="fa fa-files-o"></i>',
				'array'=>
				[
					'class'=>'btn btn-warning btn-s tooltips',
					'data-original-title'=>'Ver documentos do Cliente',
					'data-placement'=>"top",
					'data-toggle'=>"modal"
				]
			],
		];

		//THEAD alterando a ordem e colocando corretor
		$this->model->settings['thead'] = 
										[
												['class'=>'numeric','name'=>'id'],
												['class'=>'','name'=>'Empreendimento'],
												['class'=>'numeric','name'=>'Imovel'],
												['class'=>'numeric','name'=>'Cliente'],
												['class'=>'numeric','name'=>'Corretor'],
												['class'=>'numeric','name'=>'Status'],
												['class'=>'numeric','name'=>'']
										];
		//Seleciona todas reservas com nome do cliente,imovel,empreendimento e corretor								
		$model = DB::table('reserva')
			    ->join('imoveis', 'imoveis.id', '=', 'reserva.imovel_id')
	            ->join('empreendimentos', 'empreendimentos.id', '=', 'imoveis.empreendimento_id')
	            ->join('users as ' . DB::getTablePrefix() . 'corretor', 'corretor.id','=','reserva.corretor_id')
	            ->join('users', 'users.id', '=', 'reserva.user_id')
	            ->where('reserva.status',"!=",'Aprovado')
	            ->select('empreendimentos.nome as empreendimento','imoveis.tipo as imovel','users.name as cliente','reserva.*','reserva.status as status_venda','corretor.name as corretor','reserva.user_id as id','reserva.id as reserva_id')
	            ->paginate(20);
		$data = [
			'title'=>'Lista de imoveis pré reservados pelos corretores',
			'thead'=>$this->model->settings['thead'],
			'td'=>$this->model->tableColumns,
			'destroy'=>URL::route($this->model->settings['destroy.url'],''), 			
			'modal'=>$this->model->settings['modal'],
			'buttons'=>$this->model->btnsColumns,
			'model'=>$model
		];
		$this->layout->content = View::make(Config::get('views_admin.reservas.lista'), compact('data'));
	}


	

	
}