<?php

namespace App\Modules\Admin\Controllers\Admin;
use View,Cidade,User,Auth,Proposta,RegrasProposta,Parcela,Reserva,Bairro,Contrato,Estagio,Clockwork,Empreendimento,Session,Validator,Input,Redirect,Lang,URL,App,ModelInterface,Request;


class AjaxController extends \BaseController {



	/*Generate JSON of Bairros 
	  @param ($cidade_id)
	  @return JSON Bairro
	*/
	public function getBairros(){
		$bairros = Bairro::whereCidadeId(Request::get("cidade_id"))->get();
		if (isset($bairros)){ return $bairros;}
		return [];
	}


	/* Set estagio do empreendimento 
	 * @param porcent
	 * @param empreendimento_id
	 * @param estagio_id
	 */
	public function setEstagio(){
		$estagio = Estagio::find(Request::get('estagio_id'));
		$estagio->porcent = Request::get('porcent');
		$estagio->save();
	}

	/* Set status da reserva
	 * @param reserva_id
	 * @param status
	 */
	public function setReserva(){
		$Reserva = Reserva::find(Request::get('reserva_id'));
		if(isset($Reserva)){
			echo 1;
			$Reserva->status = Request::get('status');
			$Reserva->save();
		}
	}


	/* Set status da reserva
	 * @param reserva_id
	 * @param status
	 */
	public function setProposta(){
		$Proposta = Proposta::with('intermediarias')->find(Request::get('id'));
		$RegrasProposta = RegrasProposta::with('intermediarias')->whereImovelId($Proposta->reserva->imovel_id)->first();		
		if(Request::get('status')=='Aprovado'){
			$contrato = new Contrato;
			$contrato->proposta_id  = $Proposta->id;
			if($contrato->save()){
				//Entrada
				$parcelas = new Parcela;
				$parcelas->contrato_id = $contrato->id;
				$parcelas->vencimento  = date('Y-m-d');
				$parcelas->valor       = $Proposta->entrada;
				$parcelas->description = "Entrada";
				$parcelas->save();
				//Chaves
				$parcelas = new Parcela;
				if($Proposta->reserva->imovel->empreendimento->previsao_entrega)
				{
					$entrega = $Proposta->reserva->imovel->empreendimento->previsao_entrega;
				}
				else
				{
					$dt = \Carbon\Carbon::now();
					$entrega = $dt->addMonths($Proposta->prazo);
				}
				$parcelas->contrato_id = $contrato->id;
				$parcelas->vencimento  = $entrega;
				$parcelas->description = "Chaves";
				$parcelas->valor       = $Proposta->chaves;
				$parcelas->save();
				//INTERMEDIARIAS
				if(count($Proposta->intermediarias))
					foreach ($Proposta->intermediarias as $key => $value) {
						//Intermediarias
						$parcelas = new Parcela;
						$dt = \Carbon\Carbon::now();
						$parcelas->contrato_id = $contrato->id;
						$parcelas->vencimento  = $dt->addMonths($value->numero); 
						$parcelas->description = "Intermediarias = ".$value->numero;
						$parcelas->valor       = $value->valor;
						$parcelas->save();
					}
								

				$valor_imovel= $RegrasProposta->valor_imovel*(1-($Proposta->desconto/100));				
				$valor_a_financiar = ($valor_imovel*$Proposta->porcent/100);
				$valor_pago        = $Proposta->fgts+$Proposta->chaves+$Proposta->entrada;	
				
				for($index=0;$index<$Proposta->prazo;$index++)
				{
						//Parcelas
						$dt = \Carbon\Carbon::now();
						$parcelas = new Parcela;
						$parcelas->contrato_id = $contrato->id;
						$parcelas->vencimento  = $dt->addMonths($index+1);
						$parcelas->description = "Parcelas (".($index+1)."/".$Proposta->prazo.")";
						$parcelas->valor       = ($valor_a_financiar-$valor_pago)/$Proposta->prazo;
						$parcelas->save();
				}

			}
		}
		$Proposta->status = Request::get('status');
		$Proposta->save();
	}


	/* Get clientes do corretor
	 * @param Auth User
	 * return list User
	 */
	public function getClientes(){
		$model = User::with('formulario')->whereCorretorId(Auth::user()->id)->get();
		$html = "";
		$html .= "<option value=''>Selecione o Cliente</option>";
		if(isset($model))
			foreach ($model as $key => $value) {
				if (count($value->formulario)){
					$html .= "<option value='$value->id'>$value->name</option>";
				}
			}
		return $html;	
	}

	/* Set clientes do corretor na reserva
	 * @param id
	 * @param cliente_id
	 */
	public function setClientes(){
		$Reserva = Reserva::find(Request::get('id'));
		$Reserva->user_id = Request::get('cliente_id');
		$Reserva->status = 'Aguardando Aprovação'; 
		$Reserva->save();
	}

	/* Set status do empreendimento 
	 * @param empreendimento_id
	 * @param status
	 */
	public function setStatus(){
		$empreendimento               = Empreendimento::find(Request::get('empreendimento_id'));
		$empreendimento->status_venda = Request::get('status');
		if($empreendimento->save())
			echo 1;
		else			
		Clockwork::info($empreendimento->errors()); // 'Message text.' appears in Clockwork log tab		
	}

	/* Set banco financiador do empreendimento 
	 * @param empreendimento_id
	 * @param banco
	 */
	public function setBancoEmpreendimento(){
		$empreendimento               = Empreendimento::find(Request::get('empreendimento_id'));
		$empreendimento->banco_id = Request::get('banco');
		if($empreendimento->save())
			echo 1;
		else			
		Clockwork::info($empreendimento->errors()); // 'Message text.' appears in Clockwork log tab		
	}

	/* Set Destaque do empreendimento 
	 * @param empreendimento_id
	 * @param status
	 */
	public function setEmpreendimentoDestaque(){
		$empreendimento           = Empreendimento::find(Request::get('empreendimento_id'));
		$empreendimento->destaque = Request::get('destaque') ? Request::get('destaque') : 'normal';
		if($empreendimento->save())
			echo 1;
		else			
		Clockwork::info($empreendimento->errors()); // 'Message text.' appears in Clockwork log tab		

	}

}