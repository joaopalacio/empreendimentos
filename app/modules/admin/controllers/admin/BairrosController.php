<?php

namespace App\Modules\Admin\Controllers\Admin;
use View,Cidade,Bairro,Session,Config,Validator,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Bairro');

class BairrosController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }

	/**
	 * Display a listing of cidades
	 *
	 * @return Response
	 */
	public function index($id)
	{
		Session::put('url', '/admin/cidades/create');
		$model = Cidade::with('bairros')->find($id);
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],	
			['url'=>URL::route('cidades.index'),'class'=>'','active'=>'','texto'=>'Cidades'],			
			['url'=>URL::route('bairros.index',$id),'class'=>'active','active'=>'active','texto'=>'Bairros de '.$model->cidade],
		];					  
		$this->layout->with(compact('breadcrumb'));

		$model = $this->model->whereCidadeId($id)->paginate(30);
		$data = [
			'title'=>$this->model->settings['title'],
			'thead'=>$this->model->settings['thead'],
			'insert'=>URL::route($this->model->settings['insert.url'],$id), 
			'td'=>$this->model->tableColumns,
			'destroy'=>URL::route($this->model->settings['destroy.url'],$id,''), 
			'modal'=>$this->model->settings['modal'],
			'buttons'=>$this->model->btnsColumns,
			'model'=>$model
		];

		$this->layout->content = View::make(Config::get('views_admin.cidades.bairros.lista'), compact('data'));
	}
	/**
	 * Show the form for creating a new cidade
	 *
	 * @return Response
	 */
	public function create($id)
	{
		Session::put('url', '/admin/cidades/create');
		$model = Cidade::with('bairros')->find($id);
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],	
			['url'=>URL::route('cidades.index'),'class'=>'','active'=>'','texto'=>'Cidades'],			
			['url'=>URL::route('bairros.index',$id),'class'=>'active','active'=>'active','texto'=>'Bairros de '.$model->cidade],
		];				  
		$this->layout->with(compact('breadcrumb'));

		$data = [
			'title'=>'',
			'formTitle'=>$this->model->settings['formTitle'],
			'route'=>URL::route($this->model->settings['store.url'],$id),
			'elements'=>$this->model->editColumns
		];
		$this->layout->content = View::make(Config::get('views_admin.cidades.bairros.insert'), compact('data'));
	}
	/**
	 * Store a newly created cidade in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{
        $model = new $this->model;
        $model->cidade_id = $id;
        $model->bairros = Input::get('bairros');
        if ($model->save()) {
            return Redirect::route($this->model->settings['index.url'],$id)->with('message', 'Registro gravado com Sucesso!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($cidade_id,$id)
	{
		$this->model->destroy($id);

		return Redirect::back();
	}
}