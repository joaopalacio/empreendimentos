<?php

namespace App\Modules\Admin\Controllers\Admin;
use View,Imovel,User,Config,Estagio,Hash,RegraIntermediaria,Empreendimento,Session,Validator,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Estagio');

class EstagioController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }

	/**
	 * Display a listing of cidades
	 *
	 * @return Response
	 */
	public function index($id)
	{
		Session::put('url', '/admin/Empreendimento/create');
		$model = Empreendimento::with('estagios')->find($id);
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],	
			['url'=>URL::route('empreendimentos.index'),'class'=>'','active'=>'','texto'=>'Empreendimento'],			
			['url'=>URL::route('estagio.index',$id),'class'=>'active','active'=>'active','texto'=>'Estagios do '.$model->nome],
		];					  
		$this->layout->with(compact('breadcrumb'));

		$data = [
			'title'=>$this->model->settings['title'],
			'classSub'=>"empreendimento",
			'id'=>$id,
			'model'=>$model
		];

		$this->layout->content = View::make(Config::get('views_admin.estagios.lista'), compact('data'));
	}
	
	/**
	 * Store a newly created cidade in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{	
		$valida = 0;
		$chave_unica = Hash::make(uniqid());
		for($i=0; $i < Input::get('quantidade');$i++){
			$model                    = new $this->model;
			$model->empreendimento_id = $id;
			$model->tipo              = Input::get('tipo');	 
			$model->area_comum        = Input::get('area_comum');	
			$model->area_privativa    = Input::get('area_privativa');	
			$model->area_total        = Input::get('area_total');	       
			$model->descricao         = Input::get('descricao');
			$model->quartos           = Input::get('quartos');
			$model->suites            = Input::get('suites');
			$model->chave_unica		  = $chave_unica;
			$model->localizacao       = Input::get('localizacao');
			$model->vagas          	  = Input::get('vagas');	        	        	        	        
	        if ($model->save()) {
		        	foreach (Input::get('corretores') as $key => $value) {
		        		$model->corretores()->attach($value);
		        	}        	
				$regra_proposta                        = new RegrasProposta;
				$regra_proposta->imovel_id             = $model->id;
				$regra_proposta->desconto_padrao       = Input::get('desconto_padrao');
				$regra_proposta->valor_imovel          = Input::get('valor_imovel');
				$regra_proposta->entrada_minima        = Input::get('entrada_minima');
				$regra_proposta->chaves_maxima         = Input::get('chaves_maxima');
				$regra_proposta->porcent_financiamento = Input::get('porcent_financiamento');
				$regra_proposta->prazo                 = Input::get('prazo');
				$regra_proposta->save();
	        	foreach (Input::get('intermediarias') as $key => $value) {
	        		$intermediaria = new RegraIntermediaria;
	        		$intermediaria->mes = $value['mes'];
	        		$intermediaria->valor = $value['valor'];
	        		$intermediaria->regras_id = $regra_proposta->id;
	        		$intermediaria->save();
	        	}						
	        	$valida +=1;  
	        } 
    	}

        if ($model->save()) {
        	$valida +=1;  
            return Redirect::route($this->model->settings['index.url'],$id)->with('message', 'Registros gravado com Sucesso = '.$valida.'!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }


	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($cidade_id,$id)
	{
		$this->model->find($id)->corretores()->detach();		
		RegrasProposta::whereImovelId($id)->delete();
		$this->model->destroy($id);

		return Redirect::back();
	}
}