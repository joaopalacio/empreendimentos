<?php

namespace App\Modules\Admin\Controllers\Admin;
use View,Configuracao,Config,Session,Hash,Validator,Input,Redirect,Lang,URL,App,ModelInterface;;

App::bind('ModelInterface', 'Configuracao');;

class SettingsController extends \BaseController {

	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';
	protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }

	/**
	 * Display a listing of site
	 *
	 * @return Response
	 */
	public function index()
	{
		Session::put('url', '/admin/site');

		$breadcrumb = [
			['url'=>'admin.index','class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>'admin.index','class'=>'','active'=>'','texto'=>'Sistema'],			
			['url'=>$this->model->settings['index.url'],'class'=>'active','active'=>'active','texto'=>$this->model->settings['model']],
		];					  
		

		$data = [
			'title'=>$this->model->settings['title'],
			'formTitle'=>$this->model->settings['formTitle'],
			'route'=>URL::route($this->model->settings['update.url']), 
			'elements'=>$this->model->editColumns,
			'model'=>Configuracao::first(),
		];

		$this->layout->content = View::make(Config::get('views_admin.settings.index'), compact('data'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$model = $this->model->first();
		if (!isset($model))
		{	
			$model = new Configuracao;
			if ($model->save()) {
	            return Redirect::route($this->model->settings['index.url'])->with('message', 'Registro alterado com Sucesso!');
	        } else {
	            return Redirect::back()->withErrors($model->errors())->withInput();
	        }			
		}
		else				
        if ($model->update(Input::all())) {
            return Redirect::route($this->model->settings['index.url'])->with('message', 'Registro alterado com Sucesso!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }
	}

	


}