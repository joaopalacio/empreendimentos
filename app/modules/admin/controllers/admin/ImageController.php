<?php
 
class ImageController extends BaseController {
    public function getUploadForm() {    return View::make('image/upload-form');    }
 
    public function postUpload() {
        $file = Input::file('image');
        $input = array('image' => $file);
        $rules = array(    'image' => 'image');
        $validator = Validator::make($input, $rules);
        if ( $validator->fails() ){
            return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);
        }
        else {
            $imovel = Empreendimento::find(Request::get('id'));
            $filename = $file->getClientOriginalName();
            //Input::file('image')->move($destinationPath, $filename);
            Image::make($file->getClientOriginalName())->resize(null, 150,true)->crop(220,150)->save(public_path()."/".Config::get('params.get.empreendimentos').$url.'_small.jpg');         
            Image::make($file->getClientOriginalName())->resize(null, 370,true)->crop(460,370)->save(public_path()."/".Config::get('params.get.empreendimentos').$url.'_medium.jpg');    
            Image::make($file->getClientOriginalName())->resize(null, 420,true)->crop(460,420)->save(public_path()."/".Config::get('params.get.empreendimentos').$url.'_large.jpg');         
            Image::make($file->getClientOriginalName())->resize(1180, null,true)->crop(1180,420)->save(public_path()."/".Config::get('params.get.empreendimentos').$url.'_interna.jpg');                     
            Image::make($file->getClientOriginalName())->resize(null, 1400,true)->crop(1700,1400)->save(public_path()."/".Config::get('params.get.empreendimentos').$url.'_banner.jpg');                                         
            $url = md5(uniqid());                
            $model = new UploadImage;
            $model->url = $url;
            $model->media = '800x600';
            $imovel->uploads()->save($model);
            return Response::json(
                        [
                        'thumbnailUrl'=>'1',
                        'url'=>'1',
                        'name'=>'1',
                        'size'=>'1',
                        ]
                   )            
        }
    }
}