<?php

namespace App\Modules\Admin\Controllers\Admin;
use View,Imovel,UtilApp,Corretor,Image,Hashids,Request,File,UploadImage,Config,User,RegrasProposta,DB,Auth,Hash,RegraIntermediaria,Empreendimento,Session,Validator,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Imovel');

class ImoveisController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }

	/**
	 * Display a listing of cidades
	 *
	 * @return Response
	 */
	public function index($id)
	{
		Session::put('url', '/admin/Empreendimento/create');
		$model = Empreendimento::with('imoveis')->find($id);
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],	
			['url'=>URL::route('empreendimentos.index'),'class'=>'','active'=>'','texto'=>'Empreendimento'],			
			['url'=>URL::route('imoveis.index',$id),'class'=>'active','active'=>'active','texto'=>'Imoveis de '.$model->nome],
		];					  
		$this->layout->with(compact('breadcrumb'));
		$model = DB::table('imoveis')
		->select(array('imoveis.*', DB::raw('COUNT(nome) as quantidade')))
		->groupBy('nome')
		->where('empreendimento_id','=',$id)
		->paginate();
		//$model = $this->model->whereEmpreendimentoId($id)->groupBy('nome')->orderBy('id','ASC')->paginate(30);
		$data = [
			'title'=>$this->model->settings['title'],
			'thead'=>
			        [ 
			        	['class'=>'','name'=>'Nome'],
			        	['class'=>'','name'=>'Quartos'],
			        	['class'=>'','name'=>'Área'],
			        	['class'=>'','name'=>'Quantidade'],
			        	['class'=>'','name'=>''],
			        ] ,
			'insert'=>URL::route($this->model->settings['insert.url'],$id), 
			'td'=>['nome','quartos','area_privativa','quantidade'],
			'destroy'=>URL::route($this->model->settings['destroy.url'],$id,''), 
			'modal'=>$this->model->settings['modal'],
			'buttons'=>[
					[
						'link'=>'imoveis.tipo',
						'text'=>'<i class="fa   fa-list-alt"></i>',
						'sub'=>'empreendimento_id',
						'array'=>
						[
							'class'=>'btn btn-primary btn-xs tooltips',
							'data-original-title'=>'Listar Imoveis',
							'data-placement'=>"top",
							'data-toggle'=>"tooltip"
						]
					],
			],
			'model'=>$model
		];

		$this->layout->content = View::make(Config::get('views_admin.imoveis.lista'), compact('data'));
	}

	/**
	 * Display a listing of cidades
	 *
	 * @return Response
	 */
	public function lista($empreendimento_id,$id)
	{
		Session::put('url', '/admin/Empreendimento/create');
		$model = Empreendimento::with('imoveis')->find($empreendimento_id);
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],	
			['url'=>URL::route('empreendimentos.index'),'class'=>'','active'=>'','texto'=>'Empreendimento'],			
			['url'=>URL::route('imoveis.index',$id),'class'=>'active','active'=>'active','texto'=>'Imoveis de '.$model->nome],
		];					  
		$this->layout->with(compact('breadcrumb'));
		$tipo = Imovel::find($id);
		$model = $this->model->whereNome($tipo->nome)->orderBy('id','ASC')->paginate(30);
		$data = [
			'title'=>$this->model->settings['title'],
			'thead'=>$this->model->settings['thead'],
			'insert'=>URL::route($this->model->settings['insert.url'],$id), 
			'td'=>$this->model->tableColumns,
			'destroy'=>URL::route($this->model->settings['destroy.url'],$id,''), 
			'modal'=>$this->model->settings['modal'],
			'buttons'=>$this->model->btnsColumns,
			'model'=>$model
		];

		$this->layout->content = View::make(Config::get('views_admin.imoveis.lista'), compact('data'));
	}


	/**
	 * Show the form for creating a new cidade
	 *
	 * @return Response
	 */
	public function create($id)
	{

		//TODO REGRAS DE NEGOCIO
		Session::put('url', '/admin/cidades/create');
		$model = Empreendimento::with('imoveis')->find($id);
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],	
			['url'=>URL::route('empreendimentos.index'),'class'=>'','active'=>'','texto'=>'Empreendimento'],			
			['url'=>URL::route('imoveis.index',$id),'class'=>'active','active'=>'active','texto'=>'Imoveis de '.$model->nome],
		];				  
		$this->layout->with(compact('breadcrumb'));
		$this->model->editColumns[10]['fieldSet'][8]['values'] = Corretor::corretores()->select('id','name')->get();
		$data = [
			'title'=>'',
			'formTitle'=>$this->model->settings['formTitle'],
			'route'=>URL::route($this->model->settings['store.url'],$id),
			'elements'=>$this->model->editColumns
		];
		$this->layout->content = View::make(Config::get('views_admin.imoveis.insert'), compact('data'));
	}
	/**
	 * Store a newly created cidade in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{	
		$valida = 0;
		$chave_unica = Hash::make(uniqid());
		for($i=0; $i < Input::get('quantidade');$i++){
			$model                    = new $this->model;
			$model->empreendimento_id = $id;
			$model->tipo              = Input::get('tipo');	 
			$model->area_comum        = Input::get('area_comum');	
			$model->area_privativa    = Input::get('area_privativa');	
			$model->area_total        = Input::get('area_total');	       
			$model->descricao         = Input::get('descricao');
			$model->quartos           = Input::get('quartos');
			$model->suites            = Input::get('suites');
			$model->nome       		  = Input::get('nome');
			$model->vagas          	  = Input::get('vagas');	        	        	        	        
	        if ($model->save()) {
			        $hashids = new Hashids\Hashids(Config::get('app.key'));	
					$hash = $hashids->encrypt($model->id+Config::get('params.plusKey'));
					$model->chave_unica = $hash;
					$model->save();
					if(Input::get('corretores'))
		        	foreach (Input::get('corretores') as $key => $value) {
		        		$model->corretores()->attach($value);
		        	}        	
				$regra_proposta                        = new RegrasProposta;
				$regra_proposta->imovel_id             = $model->id;
				$regra_proposta->desconto_padrao       = UtilApp::Porcent(Input::get('desconto_padrao'),true);
				$regra_proposta->valor_imovel          = UtilApp::Money(Input::get('valor_imovel'),true);
				$regra_proposta->entrada_minima        = UtilApp::Money(Input::get('entrada_minima'));
				$regra_proposta->chaves_maxima         = UtilApp::Money(Input::get('chaves_maxima'));
				$regra_proposta->porcent_financiamento = UtilApp::Porcent(Input::get('porcent_financiamento'));
				$regra_proposta->prazo                 = Input::get('prazo');
				$regra_proposta->save();
				if(Input::get('intermediarias'))
	        	foreach (Input::get('intermediarias') as $key => $value) {
	        		if($value['mes']!='' && $value['valor']!='')
	        		{
		        		$intermediaria = new RegraIntermediaria;
		        		$intermediaria->mes = $value['mes'];
		        		$intermediaria->valor = UtilApp::Money($value['valor'],true);
		        		$intermediaria->regras_id = $regra_proposta->id;
		        		$intermediaria->save();
	        		}
	        	}						
	        	$valida +=1;  
	        } 
    	}

        if ($model->save()) {
        	$valida +=1;
        	return Redirect::route('imoveis.photos',[$id,$model->id])->with('message', 'Registro gravado com Sucesso! Agora adicione as imagens deste tipo de imovel');  
            //return Redirect::route($this->model->settings['index.url'],$id)->with('message', 'Registros gravado com Sucesso = '.$valida.'!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }


	}

	/**
	 * Show the form for creating a new cidade
	 *
	 * @return Response
	 */
	public function photos($empreendimento_id,$id)
	{
		//TODO REGRAS DE NEGOCIO
		Session::put('url', '/admin/empreendimentos/imoveis');
		//TODO REGRAS DE NEGOCIO
		$model = Imovel::find($id);
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],	
			['url'=>URL::route('empreendimentos.index'),'class'=>'','active'=>'','texto'=>'Empreendimento'],			
			['url'=>URL::route('imoveis.index',$empreendimento_id),'class'=>'active','active'=>'active','texto'=>'Imoveis de '.$model->empreendimento->nome],
			['url'=>'','class'=>'active','active'=>'active','texto'=>'Imagens de '.$model->tipo." cod  : ( ".($model->id)." )"],
		];				  
		$this->layout->with(compact('breadcrumb'));
		//$this->model->editColumns[10]['fieldSet'][8]['values'] = User::corretores()->select('id','name')->get();
		$data = [
			'title'=>'Upload de imagens e Galeria de imagens',
			'formTitle'=>$this->model->settings['formTitle'],
			'route'=>URL::route($this->model->settings['upload.url'],$id),
			'elements'=>$this->model->editColumns,
			'dir_upload'=>Config::get('params.get.imoveis'),
			'model'=>$model
		];
		$this->layout->content = View::make(Config::get('views_admin.imoveis.photos'), compact('data'));
	}	

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($cidade_id,$id)
	{
		$this->model->find($id)->corretores()->detach();				
		RegrasProposta::whereImovelId($id)->delete();
		//$this->model->destroy($id);

		//return Redirect::back();
	}

	/*
	*	Paramentro id do Imovel
	*	@param $id
	*	Paramentro $_FILES
	*	@param Input::file()
	*	return JSON //Verificar Jquery FileUpload retorno em json
	*/
	public function upload($id){
		$imovel = Imovel::find($id);
		foreach(Input::file('files') as $value){	
			$url = md5(uniqid());
			try{
				$img = Image::make($value->getRealPath());			
				$img->fit(220, 150)->save(public_path()."/".Config::get('params.get.imoveis').$url.'_small.jpg');				
				Image::make($value->getRealPath())->fit(460,370)->save(public_path()."/".Config::get('params.get.imoveis').$url.'_medium.jpg');	
				Image::make($value->getRealPath())->fit(800,600)->save(public_path()."/".Config::get('params.get.imoveis').$url.'_large.jpg');			
				$model = new UploadImage;
				$model->url = $url;
				$model->media = '800x600';
				$imovel->uploads()->save($model);
				$file[] =     
	                        [
	                        'thumbnailUrl'=>"/".Config::get('params.get.imoveis').$url.'_small.jpg',
	                        'url'=>"/".Config::get('params.get.imoveis').$url.'_small.jpg',
	                        'name'=>$url.'.jpg'
	                        ];    

			}catch(Exception $e){
				$file[] =     
	                        [
	                        'thumbnailUrl'=>'1',
	                        'error'=>'Ocorreu um erro ao enviar esta imagem',
	                        'url'=>'1',
	                        'name'=>$url.".jpg",
	                        'size'=>'1',
	                        ];    
			}
		}				
		return json_encode(['files'=>$file]);		
	}

	public function destroyImage($id){
		if(Request::ajax()){
			$Image = UploadImage::find(Input::get('id'));			
			File::delete(public_path()."/".Config::get('params.upload.imoveis').$Image->url.'.jpg');
			$Image->delete();
			return 'Excluido com succeso';
		}
	}

}