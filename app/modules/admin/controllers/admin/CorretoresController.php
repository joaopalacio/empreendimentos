<?php

namespace App\Modules\Admin\Controllers\Admin;
use View,Corretor,Role,Session,Event,Hash,Config,Validator,UtilApp,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Corretor');
class CorretoresController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }

	/**
	 * Display a listing of corretores
	 *
	 * @return Response
	 */
	public function index()
	{
		Session::put('url', '/admin/corretores');

		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],			
			['url'=>URL::route($this->model->settings['Corretor']['index.url']),'class'=>'active','active'=>'active','texto'=>$this->model->settings['Corretor']['model']],
		];					  
		$this->layout->with(compact('breadcrumb'));

		$model = $this->model->corretores()->paginate(30);
		$data = [
			'title'=>$this->model->settings['Corretor']['title'],
			'thead'=>$this->model->settings['Corretor']['thead'],
			'insert'=>URL::route($this->model->settings['Corretor']['insert.url']), 
			'td'=>$this->model->tableColumns,
			'destroy'=>URL::route($this->model->settings['Corretor']['destroy.url'],''), 			
			'modal'=>$this->model->settings['Corretor']['modal'],
			'buttons'=>$this->model->btnsColumns['Corretor'],
			'model'=>$model
		];

		$this->layout->content = View::make(Config::get('views_admin.corretores.lista'), compact('data'));
	}

	/**
	 * Show the form for creating a new corretores
	 *
	 * @return Response
	 */
	public function create()
	{
		Session::put('url', '/admin/corretores/create');

		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],			
			['url'=>URL::route($this->model->settings['Corretor']['index.url']),'class'=>'active','active'=>'active','texto'=>$this->model->settings['Corretor']['model']],
			['url'=>URL::route($this->model->settings['Corretor']['store.url']),'class'=>'active','active'=>'active','texto'=>'Novo Registro'],
		];				  
		$this->layout->with(compact('breadcrumb'));

		$data = [
			'title'=>$this->model->settings['Corretor']['title'],
			'formTitle'=>$this->model->settings['Corretor']['formTitle'],
			'route'=>URL::route($this->model->settings['Corretor']['store.url']), 
			'elements'=>$this->model->editColumns['Corretor']
		];

		$this->layout->content = View::make(Config::get('views_admin.indicadores.insert'), compact('data'));
	}

	/**
	 * Store a newly created corretores in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $model = new $this->model;        
		$roles           = Role::where('name','=','Corretor')->first();
		$model->name     = Input::get('name');
		$model->cpf      = Input::get('cpf');
		$model->password = Hash::make(microtime());
		//TODO Enviar Email com Senha
		$model->email    = Input::get('email');
		$model->role_id  = $roles->id;        
		if(!UtilApp::validaCPF(Input::get('cpf'))) $model->cpf="";
        if ($model->save()) {
        	Event::fire('corretor.gerar.acesso', [$model->id]);
            return Redirect::route($this->model->settings['Corretor']['index.url'])->with('message', 'Registro gravado com Sucesso!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }
	}

	/**
	 * Show the form for editing the specified corretores.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$modelFind = $this->model->find($id);
		Session::put('url', '/admin/corretores/create');

		$breadcrumb = [
			['url'=>'admin.index','class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>'admin.index','class'=>'','active'=>'','texto'=>'Sistema'],			
			['url'=>$this->model->settings['Corretor']['index.url'],'class'=>'','active'=>'','texto'=>$this->model->settings['Corretor']['model']],
			['url'=>$this->model->settings['Corretor']['store.url'],'class'=>'active','active'=>'active','texto'=>'Editar Registro '.$this->model->settings['Corretor']['model']],
		];				  
		$this->layout->with(compact('breadcrumb'));

		$model = $this->model->paginate(30);
		$data = [
			'title'=>$this->model->settings['Corretor']['title'],
			'formTitle'=>$this->model->settings['Corretor']['formTitle'],
			'route'=>URL::route($this->model->settings['Corretor']['update.url'],$id),
			'method'=>'PUT',
			'model'=>$modelFind,
			'elements'=>$this->model->editColumns['Corretor']
		];

		$this->layout->content = View::make(Config::get('views_admin.indicadores.insert'), compact('data'));
	}



	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$model = $this->model->findOrFail($id);
		$model->name     = Input::get('name');
		if($model->cpf != Input::get('cpf')) {$model->cpf = Input::get('cpf');}
		if($model->email != Input::get('email')) {$model->email = Input::get('email');}		
		//TODO Enviar Email com Senha
	
        if ($model->updateUniques()) {
            return Redirect::route($this->model->settings['Corretor']['index.url'])->with('message', 'Registro alterado com Sucesso!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->model->destroy($id);

		return Redirect::back();
	}



}