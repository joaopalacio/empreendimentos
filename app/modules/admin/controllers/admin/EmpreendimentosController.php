<?php

namespace App\Modules\Admin\Controllers\Admin;
use View,Empreendimento,Cidade,Estagio,Image,Banco,UploadImage,Auth,Config,Session,Hash,Validator,Input,Redirect,Lang,URL,App,ModelInterface,DB,Carbon\Carbon;

App::bind('ModelInterface', 'Empreendimento');

class EmpreendimentosController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }

	/**
	 * Display a listing of cidades
	 *
	 * @return Response
	 */
	public function index()
	{
		Session::put('url', '/admin/cidades');

		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],			
			['url'=>URL::route($this->model->settings['index.url']),'class'=>'active','active'=>'active','texto'=>$this->model->settings['model']],
		];					  
		$this->layout->with(compact('breadcrumb'));
		$this->model->tableColumns['dropdown']['values'] = ['Pré-venda','Lançamento','Em construção','Entregue','Ultimas unidades'];
		$model = $this->model->paginate(30);
		$data = [
			'title'=>$this->model->settings['title'],
			'thead'=>$this->model->settings['thead'],
			'insert'=>URL::route($this->model->settings['insert.url']), 
			'td'=>$this->model->tableColumns,
			'destroy'=>URL::route($this->model->settings['destroy.url'],''), 			
			'modal'=>$this->model->settings['modal'],
			'buttons'=>$this->model->btnsColumns,
			'model'=>$model
		];

		$this->layout->content = View::make(Config::get('views_admin.empreendimentos.lista'), compact('data'));
	}

	/**
	 * Show the form for creating a new cidade
	 *
	 * @return Response
	 */
	public function create()
	{
		Session::put('url', '/admin/cidades/create');

		if(Auth::user()->role->name=='Administrador'){
			$bancos = Banco::count();
			if(!$bancos){
				return Redirect::route('bancos.create')->with('info', 'Antes de cadastrar empreendimentos cadastre um banco que será o fiador desta obra');
			}
		}


		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],			
			['url'=>URL::route($this->model->settings['index.url']),'class'=>'active','active'=>'active','texto'=>$this->model->settings['model']],
			['url'=>URL::route($this->model->settings['store.url']),'class'=>'active','active'=>'active','texto'=>'Novo Registro'],
		];				  
		$this->layout->with(compact('breadcrumb'));
		$this->model->editColumns[1]['lista'] = array('' => 'Selecione o Banco Financiador') + Banco::select()->lists('banco', 'id');
		$this->model->editColumns[2]['lista'] = array('' => 'Selecione uma cidade') + Cidade::select()->lists('cidade', 'id');
		$this->model->editColumns[3]['lista'] = array('' => 'Selecione um Bairro');	
		$data = [
			'title'=>$this->model->settings['title'],
			'formTitle'=>$this->model->settings['formTitle'],
			'route'=>URL::route($this->model->settings['store.url']), 
			'elements'=>$this->model->editColumns
		];

		$this->layout->content = View::make(Config::get('views_admin.empreendimentos.insert'), compact('data'));
	}

	/**
	 * Store a newly created cidade in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $model = new $this->model;
        
        if ($model->save()) {
        	//Fundacoes estagios
        	foreach (['aprovações', 'preparações iniciais', 'fundações', 'estrutura', 'alvenaria', 'acabamento'] as $key => $value) {
        		$estagio = new Estagio;
        		$estagio->empreendimento_id = $model->id;
        		$estagio->porcent = 0.0;
        		$estagio->estagio = $value;
        		$estagio->save();
        	}
        	//empreendimentos.photos
        	return Redirect::route('empreendimentos.photos',$model->id)->with('message', 'Registro gravado com Sucesso!');
            // Redirect::route($this->model->settings['index.url'])->with('message', 'Registro gravado com Sucesso!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }
	}

	/**
	 * Show the form for editing the specified cidade.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$modelFind = Empreendimento::find($id);
		if(isset($modelFind->previsao_entrega))
		$modelFind->previsao_entrega = Carbon::createFromFormat('Y-m-d', @$modelFind->previsao_entrega)->format('d/m/Y');
		Session::put('url', '/admin/cidades/create');

		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],			
			['url'=>URL::route($this->model->settings['index.url']),'class'=>'','active'=>'','texto'=>$this->model->settings['model']],
			['url'=>URL::route($this->model->settings['edit.url'],$id),'class'=>'active','active'=>'active','texto'=>'Editar Registro '.$this->model->settings['model']],
		];				  

		$this->layout->with(compact('breadcrumb'));
		$this->model->editColumns[1]['lista'] = array('' => 'Selecione o Banco Financiador') + Banco::select()->lists('banco', 'id');
		$this->model->editColumns[2]['lista'] = array('' => 'Selecione uma cidade') + Cidade::select()->lists('cidade', 'id');
		$this->model->editColumns[3]['lista'] = array('' => 'Selecione um Bairro');	
		
		$data = [
			'title'=>$this->model->settings['title'],
			'formTitle'=>$this->model->settings['formTitle'],
			'route'=>URL::route($this->model->settings['update.url'],$id),
			'method'=>'PUT',
			'model'=>$modelFind,
			'elements'=>$this->model->editColumns
		];

		$this->layout->content = View::make(Config::get('views_admin.empreendimentos.insert'), compact('data'));
	}



	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$model = $this->model->findOrFail($id);
        if ($model->update(Input::all())) {
            return Redirect::route($this->model->settings['index.url'])->with('message', 'Registro alterado com Sucesso!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->model->find($id)->delete();	
		return Redirect::back();
	}

	/**
	 * Show the form for creating a new cidade
	 *
	 * @return Response
	 */
	public function photos($id)
	{
		//TODO REGRAS DE NEGOCIO
		Session::put('url', '/admin/empreendimentos/imoveis');
		//TODO REGRAS DE NEGOCIO
		$model = Empreendimento::find($id);
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],	
			['url'=>URL::route('empreendimentos.index'),'class'=>'','active'=>'','texto'=>'Empreendimento'],			
			['url'=>URL::route('empreendimentos.photos',$id),'class'=>'active','active'=>'active','texto'=>'Fotos de '.$model->nome],
		];				  
		$this->layout->with(compact('breadcrumb'));
		$data = [
			'title'=>'Upload de imagens e Galeria de imagens',
			'formTitle'=>$this->model->settings['formTitle'],
			'route'=>URL::route($this->model->settings['upload.url'],$id),
			'elements'=>$this->model->editColumns,
			'dir_upload'=>Config::get('params.get.empreendimentos'),
			'model'=>$model
		];
		$this->layout->content = View::make(Config::get('views_admin.empreendimentos.photos'), compact('data'));
	}	

	/*
	*	Paramentro id do Empreendimento
	*	@param $id
	*	Paramentro $_FILES
	*	@param Input::file()
	*	return JSON //Verificar Jquery FileUpload retorno em json
	*/
	public function upload($id){
		$imovel = Empreendimento::find($id);
		$file = [];
		foreach(Input::file('files') as $value){	
			$url = md5(uniqid());
			try{
				$img = Image::make($value->getRealPath());			
				$img->fit(220, 150)->save(public_path()."/".Config::get('params.get.empreendimentos').$url.'_small.jpg');				
				Image::make($value->getRealPath())->fit(460,370)->save(public_path()."/".Config::get('params.get.empreendimentos').$url.'_medium.jpg');	
				Image::make($value->getRealPath())->fit(460,420)->save(public_path()."/".Config::get('params.get.empreendimentos').$url.'_large.jpg');			
				Image::make($value->getRealPath())->fit(1180,420)->save(public_path()."/".Config::get('params.get.empreendimentos').$url.'_interna.jpg');						
				Image::make($value->getRealPath())->fit(1700,1400)->save(public_path()."/".Config::get('params.get.empreendimentos').$url.'_banner.jpg');			
				$model = new UploadImage;
				$model->url = $url;
				$model->media = '800x600';
				$imovel->uploads()->save($model);
				$file[] =     
	                        [
	                        'thumbnailUrl'=>"/".Config::get('params.get.empreendimentos').$url.'_small.jpg',
	                        'url'=>"/".Config::get('params.get.empreendimentos').$url.'_small.jpg',
	                        'name'=>$url.'.jpg'
	                        ];    

			}catch(Exception $e){
				$file[] =     
	                        [
	                        'thumbnailUrl'=>'1',
	                        'error'=>'Ocorreu um erro ao enviar esta imagem',
	                        'url'=>'1',
	                        'name'=>$url.".jpg",
	                        'size'=>'1',
	                        ];    
			}
		}				
		return json_encode(['files'=>$file]);
	}

	public function destroyImage($id){
		if(Request::ajax()){
			$Image = UploadImage::find(Input::get('id'));			
			File::delete(public_path()."/".Config::get('params.upload.empreendimentos').$Image->url.'.jpg');
			$Image->delete();
			return 'Excluido com succeso';
		}
	}

}