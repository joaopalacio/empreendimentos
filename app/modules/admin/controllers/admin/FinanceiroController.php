<?php

namespace App\Modules\Admin\Controllers\Admin;
use View,Corretor,Role,Session,Event,Hash,Validator,Boleto,Config,UtilApp,Input,Redirect,Lang,URL,App,ModelInterface;
use RetornoFactory,RetornoBanco,RetornoCNAB400;


class FinanceiroController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    public function retornoBoleto(){
		Session::put('url', '/admin/financeiro');

		$breadcrumb = [
			['url'=>'admin.index','class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>'admin.index','class'=>'','active'=>'','texto'=>'Financeiro'],			
			['url'=>'','class'=>'active','active'=>'active','texto'=>'Arquivo de Retorno'],
		];					  
		$this->layout->with(compact('breadcrumb'));
		$this->layout->content = View::make(Config::get('views_admin.financeiro.upload'));
		   	
		
    }


    public function gravaRetorno(){
    	$file = Input::file('retorno');
    	$retorno = file_get_contents($file->getRealPath());
    	//$cnab240 = RetornoFactory::getRetorno($file->getRealPath(), "linhaProcessada1");
		echo "<pre>";
		$lendo = @fopen($file->getRealPath(),"r");
		$i=0;
		while (!feof($lendo))
		{
			
			$i++;

			if($linha=fgets($lendo, 500)) {
	          //echo "<h1>Arquivo: $fileName. Linha: $linha</h1>";
	          $len = strlen($linha);
	      	}

			$linha = fgets($lendo,242);
			$linhas['t_u_segmento'] = substr($linha,13,1);//Segmento T ou U
			$linhas['t_tipo_reg'] = substr($linha,7,1);//Tipo de Registro
			
			if($linhas['t_u_segmento'] == 'T')
			{

				$linhas['t_cod_banco'] = substr($linha,0,3);//Código do banco na compensação
				$linhas['t_lote'] = substr($linha,3,4);//Lote de serviço - Número seqüencial para identificar um lote de serviço.
				$linhas['t_n_sequencial'] = substr($linha,8,5);//Nº Sequencial do registro no lote
				$linhas['t_cod_seg'] = substr($linha,15,2);//Cód. Segmento do registro detalhe
				$linhas['t_cod_conv_banco'] = substr($linha,23,6);//Código do convênio no banco - Código fornecido pela CAIXA, através da agência de relacionamento do cliente. Deve ser preenchido com o código do Cedente (6 posições).
				$linhas['t_n_banco_sac'] = substr($linha,32,3);//Numero do banco de sacados
				$linhas['t_mod_nosso_n'] = substr($linha,37,20);//Modalidade nosso número
				//$linhas['t_id_titulo_banco'] = substr($linha,41,15);//Identificação do titulo no banco - Número adotado pelo Banco Cedente para identificar o Título.
				$linhas['t_cod_carteira'] = substr($linha,57,1);//Código da carteira - Código adotado pela FEBRABAN, para identificar a característica dos títulos. 1=Cobrança Simples, 3=Cobrança Caucionada, 4=Cobrança Descontada
				$linhas['t_num_doc_cob'] = substr($linha,58,15);//Número do documento de cobrança - Número utilizado e controlado pelo Cliente, para identificar o título de cobrança.
				$linhas['t_dt_vencimento'] = substr($linha,73,8);//Data de vencimento do titulo - Data de vencimento do título de cobrança.
				$linhas['t_v_nominal'] = substr($linha,81,15);//Valor nominal do titulo - Valor original do Título. Quando o valor for expresso em moeda corrente, utilizar 2 casas decimais.
				$linhas['t_cod_banco2'] = substr($linha,96,3);//Código do banco
				$linhas['t_cod_ag_receb'] = substr($linha,99,5);//Codigo da agencia cobr/receb - Código adotado pelo Banco responsável pela cobrança, para identificar o estabelecimento bancário responsável pela cobrança do título.
				$linhas['t_dv_ag_receb'] = substr($linha,104,1);//Dígito verificador da agencia cobr/receb
				$linhas['t_id_titulo_empresa'] = substr($linha,105,25);//identificação do título na empresa - Campo destinado para uso da Empresa Cedente para identificação do Título. Informar o Número do Documento - Seu Número.
				$linhas['t_cod_moeda'] = substr($linha,130,2);//Código da moeda
				$linhas['t_tip_inscricao'] = substr($linha,132,1);//Tipo de inscrição - Código que identifica o tipo de inscrição da Empresa ou Pessoa Física perante uma Instituição governamental: 0=Não informado, 1=CPF, 2=CGC / CNPJ, 9=Outros.
				$linhas['t_num_inscricao'] = substr($linha,133,15);//Número de inscrição - Número de inscrição da Empresa (CNPJ) ou Pessoa Física (CPF).
				$linhas['t_nome'] = substr($linha,148,40);//Nome - Nome que identifica a entidade, pessoa física ou jurídica, Cedente original do título de cobrança.
				$linhas['t_v_tarifa_custas'] = substr($linha,198,13);//Valor da tarifa/custas
				$linhas['t_id_rejeicoes'] = substr($linha,213,10);//Identificação para rejeições, tarifas, custas, liquidação e baixas
			}
						
			if($linhas['t_u_segmento'] == 'U')
			{
				$linhas['u_cod_banco'] = substr($linha,0,3);//Código do banco na compensação
				$linhas['u_lote'] = substr($linha,3,4);//Lote de serviço - Número seqüencial para identificar um lote de serviço.
				$linhas['u_tipo_reg'] = substr($linha,7,1);//Tipo de Registro - Código adotado pela FEBRABAN para identificar o tipo de registro: 0=Header de Arquivo, 1=Header de Lote, 3=Detalhe, 5=Trailer de Lote, 9=Trailer de Arquivo.
				$linhas['u_n_sequencial'] = substr($linha,8,5);//Nº Sequencial do registro no lote
				$linhas['u_cod_seg'] = substr($linha,15,2);//Cód. Segmento do registro detalhe
				$linhas['u_juros_multa'] = substr($linha,17,15);//Jurus / Multa / Encargos - Valor dos acréscimos efetuados no título de cobrança, expresso em moeda corrente.
				$linhas['u_desconto'] = substr($linha,32,15);//Valor do desconto concedido - Valor dos descontos efetuados no título de cobrança, expresso em moeda corrente.
				$linhas['u_abatimento'] = substr($linha,47,15);//Valor do abat. concedido/cancel. - Valor dos abatimentos efetuados ou cancelados no título de cobrança, expresso em moeda corrente.
				$linhas['u_iof'] = substr($linha,62,15);//Valor do IOF recolhido - Valor do IOF - Imposto sobre Operações Financeiras - recolhido sobre o Título, expresso em moeda corrente.
				$linhas['u_v_pago'] = substr($linha,77,15);//Valor pago pelo sacado - Valor do pagamento efetuado pelo Sacado referente ao título de cobrança, expresso em moeda corrente.
				$linhas['u_v_liquido'] = substr($linha,92,15);//Valor liquido a ser creditado - Valor efetivo a ser creditado referente ao Título, expresso em moeda corrente.
				$linhas['u_v_despesas'] = substr($linha,107,15);//Valor de outras despesas - Valor de despesas referente a Custas Cartorárias, se houver.
				$linhas['u_v_creditos'] = substr($linha,122,15);//Valor de outros creditos - Valor efetivo de créditos referente ao título de cobrança, expresso em moeda corrente.
				$linhas['u_dt_ocorencia'] = substr(substr($linha,137,8),4,4).'-'.substr(substr($linha,137,8),2,2).'-'.substr(substr($linha,137,8),0,2);//Data da ocorrência - Data do evento que afeta o estado do título de cobrança.
				$linhas['u_dt_efetivacao'] = substr($linha,145,8);//Data da efetivação do credito - Data de efetivação do crédito referente ao pagamento do título de cobrança.
				$linhas['u_dt_debito'] = substr($linha,157,8);//Data do débito da tarifa
				$linhas['u_cod_sacado'] = substr($linha,167,15);//Código do sacado no banco
				$linhas['u_cod_banco_comp'] = substr($linha,210,3);//Cód. Banco Correspondente compens - Código fornecido pelo Banco Central para identificação na Câmara de Compensação, do Banco ao qual será repassada a Cobrança do Título.
				$linhas['u_nn_banco'] = substr($linha,213,20);//Nosso Nº banco correspondente - Código fornecido pelo Banco Correspondente para identificação do Título de Cobrança. Deixar branco (Somente para troca de arquivos entre Bancos).
				
				/*@$linhas['u_juros_multa'] = substr($linhas['$u_juros_multa'],0,13).'.'.substr($linhas['$u_juros_multa'],13,2);
				@$linhas['u_desconto'] = substr($linhas['$u_desconto'],0,13).'.'.substr($linhas['$u_desconto'],13,2);
				@$linhas['u_abatimento'] = substr($linhas['$u_abatimento'],0,13).'.'.substr($linhas['$u_abatimento'],13,2);
				@$linhas['u_iof'] = substr($linhas['$u_iof'],0,13).'.'.substr($linhas['$u_iof'],13,2);
				@$linhas['u_v_pago'] = substr($linhas['$u_v_pago'],0,13).'.'.substr($linhas['$u_v_pago'],13,2);
				@$linhas['u_v_liquido'] = substr($linhas['$u_v_liquido'],0,13).'.'.substr($linhas['$u_v_liquido'],13,2);
				@$linhas['u_v_despesas'] = substr($linhas['$u_v_despesas'],0,13).'.'.substr($linhas['$u_v_despesas'],13,2);
				@$linhas['u_v_creditos'] = substr($linhas['$u_v_creditos'],0,13).'.'.substr($linhas['$u_v_creditos'],13,2);*/
				
				$data_agora = date('Y-m-d');
				$hora_agora = date('H:i:s');
				
				$dados[] = $linhas;
			}

		}
    
    	foreach ($dados as $key => $vlinha) {
    		echo UtilApp::formatNumberTXT($vlinha['t_v_nominal'])."<br/>";
            echo "nosso_numero = ".$vlinha['t_mod_nosso_n']."<br/>";
            echo "valor_pagamento = ".UtilApp::formatNumberTXT($vlinha['u_v_pago'])."<br/>";
            echo "acrescimos = ".UtilApp::formatNumberTXT($vlinha['t_v_tarifa_custas'])."<br/>"; //multa e mora
            echo "data_pagamento = ".$vlinha['t_dt_vencimento']."<br/>";
            $boleto = Boleto::where('nosso_numero',"=",$vlinha['t_mod_nosso_n'])->first();
            if(isset($boleto)){
            	$boleto->status = "pago";
            	$boleto->save();
            }
    	}
	}

}

/*$fileName = "CNAB240.ret";



		//Use uma das duas instrucões abaixo (comente uma e descomente a outra)
		$cnab240 = RetornoFactory::getRetorno($fileName, "linhaProcessada1");
		//$cnab400 = new RetornoCNAB400("retorno.ret", "linhaProcessada1");
		$retorno = new RetornoBanco($cnab240);
		$retorno->processar();
		exit;