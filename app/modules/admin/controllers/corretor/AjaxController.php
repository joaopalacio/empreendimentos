<?php

namespace App\Modules\Admin\Controllers\Corretor;
use View,Cidade,User,Auth,Proposta,Reserva,Bairro,Estagio,Clockwork,Empreendimento,Session,Validator,Input,Redirect,Lang,URL,App,ModelInterface,Request;


class AjaxController extends \BaseController {



	/*Generate JSON of Bairros 
	  @param ($cidade_id)
	  @return JSON Bairro
	*/
	public function getBairros(){
		$bairros = Bairro::whereCidadeId(Request::get("cidade_id"))->get();
		if (isset($bairros)){ return $bairros;}
		return [];
	}


	/* Set estagio do empreendimento 
	 * @param porcent
	 * @param empreendimento_id
	 * @param estagio_id
	 */
	public function setEstagio(){
		$estagio = Estagio::find(Request::get('estagio_id'));
		$estagio->porcent = Request::get('porcent');
		$estagio->save();
	}

	/* Set status da reserva
	 * @param reserva_id
	 * @param status
	 */
	public function setReserva(){
		$Reserva = Reserva::find(Request::get('reserva_id'));
		$Reserva->status = Request::get('status');
		$Reserva->save();
	}


	/* Set status da reserva
	 * @param reserva_id
	 * @param status
	 */
	public function setProposta(){
		$Proposta = Proposta::find(Request::get('id'));
		$Proposta->status = Request::get('status');
		$Proposta->save();
	}


	/* Get clientes do corretor
	 * @param Auth User
	 * return list User
	 */
	public function getClientes(){
		$model = User::with('formulario')->whereCorretorId(Auth::user()->id)->get();
		$html = "";
		$html .= "<option value=''>Selecione o Cliente</option>";
		if(isset($model))
			foreach ($model as $key => $value) {
				if (count($value->formulario)){
					$html .= "<option value='$value->id'>$value->name</option>";
				}
			}
		return $html;	
	}

	/* Set clientes do corretor na reserva
	 * @param id
	 * @param cliente_id
	 */
	public function setClientes(){
		$Reserva = Reserva::find(Request::get('id'));
		$Reserva->user_id = Request::get('cliente_id');
		$Reserva->status = 'Aguardando Aprovação'; 
		$Reserva->save();
	}

	/* Set status do empreendimento 
	 * @param empreendimento_id
	 * @param status
	 */
	public function setStatus(){
		$empreendimento               = Empreendimento::find(Request::get('empreendimento_id'));
		$empreendimento->status_venda = Request::get('status');
		if($empreendimento->save())
			echo 1;
		else			
		Clockwork::info($empreendimento->errors()); // 'Message text.' appears in Clockwork log tab		
	}


	/* Set Destaque do empreendimento 
	 * @param empreendimento_id
	 * @param status
	 */
	public function setEmpreendimentoDestaque(){
		$empreendimento           = Empreendimento::find(Request::get('empreendimento_id'));
		$empreendimento->destaque = Request::get('destaque') ? Request::get('destaque') : 'normal';
		if($empreendimento->save())
			echo 1;
		else			
		Clockwork::info($empreendimento->errors()); // 'Message text.' appears in Clockwork log tab		

	}

}