<?php

namespace App\Modules\Admin\Controllers\Corretor;
use View,Auth,Cidade,Bairro,Proposta,RegrasProposta,Session,Validator,DB,Input,Event,Redirect,Lang,URL,App,ModelInterface,DateTime;

use OpenBoleto\Banco\Bradesco;
use OpenBoleto\Agente;


class DashboardController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

	/**
	 * Display a listing of cidades
	 *
	 * @return Response
	 */
	public function index()
	{
		Session::put('url', '/admin/index');

		$breadcrumb = [
			['url'=>'admin.index','class'=>'fa-home','active'=>'','texto'=>'Home'],
		];					  
		$this->layout->with(compact('breadcrumb'));	
		if(Auth::user()->role->name=='Cliente')
		{
			if(!Session::get('cliente.imovel'))
			{
				//Seleciona todas propostas aceitas deste cliente
				$model = DB::table('propostas')
						->join('reserva', 'reserva.id', '=', 'propostas.reserva_id')
					    ->join('imoveis', 'imoveis.id', '=', 'reserva.imovel_id')
					    ->join('regras_propostas', 'imoveis.id', '=', 'regras_propostas.imovel_id')
			            ->join('empreendimentos', 'empreendimentos.id', '=', 'imoveis.empreendimento_id')
			            ->join('users as ' . DB::getTablePrefix() . 'corretor', 'corretor.id','=','reserva.corretor_id')
			            ->leftJoin('users', 'users.id', '=', 'reserva.user_id')
			            ->select('empreendimentos.nome as empreendimentos_nome','imoveis.*','propostas.id as proposta_id')
			            ->where('reserva.user_id','=',Auth::user()->id)
			            ->where('propostas.status','=','Aprovado')
			            ->get();
				$this->layout->content = View::make('admin::dashboard.clientes.escolhe')->with(compact('model'));
			}
			else
			{
				$Proposta = Proposta::find(Session::get('cliente.imovel'));		
				$RegrasProposta = RegrasProposta::with('intermediarias')->whereImovelId($Proposta->reserva->imovel_id)->first();		

				$data =
				[
					'Proposta' => $Proposta,
					'RegrasProposta' => $RegrasProposta
				];			
			    $this->layout->content = View::make('admin::dashboard.clientes.proposta')->with(compact('data'));
			}
		}
		//Event::fire('test.email', array(\Auth::user()));
		//
	}

	public function adminBoleto(){
		
		$sacado = new Agente('Fernando Maia', '023.434.234-34', 'ABC 302 Bloco N', '72000-000', 'Brasília', 'DF');
		$cedente = new Agente('MaschioShin', '02.123.123/0001-11', 'CLS 403 Lj 23', '71000-000', 'Brasília', 'DF');

		$boleto = new Bradesco(array(
		    // Parâmetros obrigatórios
		    'dataVencimento' => new DateTime('2013-01-24'),
		    'valor' => 23.00,
		    'sequencial' => 75896452, // Até 11 dígitos
		    'sacado' => $sacado,
		    'cedente' => $cedente,
		    'agencia' => 1172, // Até 4 dígitos
		    'carteira' => 6, // 3, 6 ou 9
		    'conta' => 0403005, // Até 7 dígitos

		    // Parâmetros recomendáveis
		    //'logoPath' => 'http://empresa.com.br/logo.jpg', // Logo da sua empresa
		    'contaDv' => 2,
		    'agenciaDv' => 1,
		    'descricaoDemonstrativo' => array( // Até 5
		        'Compra de materiais cosméticos',
		        'Compra de alicate',
		    ),
		    'instrucoes' => array( // Até 8
		        'Após o dia 30/11 cobrar 2% de mora e 1% de juros ao dia.',
		        'Não receber após o vencimento.',
		    ),

		    // Parâmetros opcionais
		    //'resourcePath' => '../resources',
		    //'cip' => '000', // Apenas para o Bradesco
		    //'moeda' => Bradesco::MOEDA_REAL,
		    //'dataDocumento' => new DateTime(),
		    //'dataProcessamento' => new DateTime(),
		    //'contraApresentacao' => true,
		    //'pagamentoMinimo' => 23.00,
		    //'aceite' => 'N',
		    //'especieDoc' => 'ABC',
		    //'numeroDocumento' => '123.456.789',
		    //'usoBanco' => 'Uso banco',
		    //'layout' => 'layout.phtml',
		    //'logoPath' => 'http://boletophp.com.br/img/opensource-55x48-t.png',
		    //'sacadorAvalista' => new Agente('Antônio da Silva', '02.123.123/0001-11'),
		    //'descontosAbatimentos' => 123.12,
		    'moraMulta' => 123.12,
		    'outrasDeducoes' => 11123.12,
		    'outrosAcrescimos' => 1232.12,
		    'valorCobrado' => 13323.12,
		    'valorUnitario' => 13323.12,
		    'quantidade' => 10,
		));

	echo $boleto->getOutput();
	}
}