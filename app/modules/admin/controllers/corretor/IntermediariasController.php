<?php

namespace App\Modules\Admin\Controllers\Corretor;
use View,Imovel,RegrasProposta,Empreendimento,Session,Validator,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Imovel');

class IntermediariasController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }

	/**
	 * Display a listing of cidades
	 *
	 * @return Response
	 */
	public function index($id)
	{
		Session::put('url', '/admin/Empreendimento/create');
		$model = Empreendimento::with('imoveis')->find($id);
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],	
			['url'=>URL::route('empreendimentos.index'),'class'=>'','active'=>'','texto'=>'Empreendimento'],			
			['url'=>URL::route('imoveis.index',$id),'class'=>'active','active'=>'active','texto'=>'Imoveis de '.$model->nome],
		];					  
		$this->layout->with(compact('breadcrumb'));

		$model = $this->model->whereEmpreendimentoId($id)->paginate(30);
		$data = [
			'title'=>$this->model->settings['title'],
			'thead'=>$this->model->settings['thead'],
			'insert'=>URL::route($this->model->settings['insert.url'],$id), 
			'td'=>$this->model->tableColumns,
			'destroy'=>URL::route($this->model->settings['destroy.url'],$id,''), 
			'modal'=>$this->model->settings['modal'],
			'buttons'=>$this->model->btnsColumns,
			'model'=>$model
		];

		$this->layout->content = View::make('admin::'.$this->model->settings['index.url'], compact('data'));
	}
	/**
	 * Show the form for creating a new cidade
	 *
	 * @return Response
	 */
	public function create($id)
	{

		//TODO REGRAS DE NEGOCIO
		Session::put('url', '/admin/cidades/create');
		$model = Empreendimento::with('imoveis')->find($id);
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],	
			['url'=>URL::route('empreendimentos.index'),'class'=>'','active'=>'','texto'=>'Empreendimento'],			
			['url'=>URL::route('imoveis.index',$id),'class'=>'active','active'=>'active','texto'=>'Imoveis de '.$model->nome],
		];				  
		$this->layout->with(compact('breadcrumb'));

		$data = [
			'title'=>'',
			'formTitle'=>$this->model->settings['formTitle'],
			'route'=>URL::route($this->model->settings['store.url'],$id),
			'elements'=>$this->model->editColumns
		];
		$this->layout->content = View::make('admin::'.$this->model->settings['crud.url'], compact('data'));
	}
	/**
	 * Store a newly created cidade in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{	
		$valida = 0;
		for($i=0; $i < Input::get('quantidade');$i++){
			$model                    = new $this->model;
			$model->empreendimento_id = $id;
			$model->tipo              = Input::get('tipo');	 
			$model->area_comum        = Input::get('area_comum');	
			$model->area_privativa    = Input::get('area_privativa');	
			$model->area_total        = Input::get('area_total');	       
			$model->descricao         = Input::get('descricao');
			$model->quartos           = Input::get('quartos');
			$model->suites            = Input::get('suites');
			$model->localizacao       = Input::get('localizacao');
			$model->vagas          	  = Input::get('vagas');	        	        	        	        
	        if ($model->save()) {
				$regra_proposta                        = new RegrasProposta;
				$regra_proposta->imovel_id             = $model->id;
				$regra_proposta->desconto_padrao       = Input::get('desconto_padrao');
				$regra_proposta->valor_imovel          = Input::get('valor_imovel');
				$regra_proposta->entrada_minima        = Input::get('entrada_minima');
				$regra_proposta->chaves_maxima         = Input::get('chaves_maxima');
				$regra_proposta->porcent_financiamento = Input::get('porcent_financiamento');
				$regra_proposta->prazo                 = Input::get('prazo');
				$regra_proposta->save();
	        	$valida +=1;  
	        } 
    	}

        if ($model->save()) {
        	$valida +=1;  
            return Redirect::route($this->model->settings['index.url'],$id)->with('message', 'Registros gravado com Sucesso = '.$valida.'!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }


	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($cidade_id,$id)
	{
		$this->model->destroy($id);

		return Redirect::back();
	}
}