<?php

namespace App\Modules\Admin\Controllers\Corretor;
use View,Formulario,User,UtilApp,Request,Carbon\Carbon,Config,Role,Auth,Session,Hash,Validator,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Formulario');
class FormularioController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }


	/**
	 * Show the form for creating a new Clientees
	 *
	 * @return Response
	 */
	public function ficha($id)
	{
		Session::put('url', '/admin/Clientes/create');
		$user = User::find($id);		
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route($this->model->settings['Cliente']['index.url']),'class'=>'active','active'=>'active','texto'=>$this->model->settings['Cliente']['model']],
			['url'=>URL::route($this->model->settings['Cliente']['index.url']),'class'=>'active','active'=>'active','texto'=>'Formulario de dados do Cliente '.$user->name],
		];				  
		$this->layout->with(compact('breadcrumb'));

		//verifica a URL TIPO (Proposta, Conjuge)
		$tipo = Request::segment(5);

		if ($tipo=='Proposta'){
			//se for o cliente, popular valores do usuario no formulario
			$this->model->editColumns['Cliente'][0]['value'] = $user->name;
			$this->model->editColumns['Cliente'][1]['value'] = $user->cpf;
		}
		$formulario = Formulario::whereUserId($id)->where('tipo','=',$tipo)->first();
		if($formulario->casamento!='')
			$formulario->casamento = Carbon::createFromFormat('Y-m-d', $formulario->casamento)->format('d/m/Y');		
		if($formulario->nascimento!='')
			$formulario->nascimento = Carbon::createFromFormat('Y-m-d', $formulario->nascimento)->format('d/m/Y');		
		if($formulario->data_exp!='')
			$formulario->data_exp = Carbon::createFromFormat('Y-m-d', $formulario->data_exp)->format('d/m/Y');		




		$this->model->editColumns['Cliente'][] = ['id'=>'tipo','name'=>'tipo','campo'=>'tipo','minlength'=>'','type'=>'hidden','class'=>'','placeholder'=>'','value'=>$tipo];	


		if (!isset($formulario)){
			$data = [
				'title'=>$this->model->settings['Cliente']['title'],
				'formTitle'=>$this->model->settings['Cliente']['formTitle'],
				'route'=>URL::route($this->model->settings['Cliente']['store.url'],$id), 
				'elements'=>$this->model->editColumns['Cliente']
			];	
		}else{
			$data = [
				'title'=>$this->model->settings['Cliente']['title'],
				'formTitle'=>$this->model->settings['Cliente']['formTitle'],
				'route'=>URL::route($this->model->settings['Cliente']['update.url'],$id),
				'model'=>$formulario,
				'elements'=>$this->model->editColumns['Cliente']
			];	
		}


		$this->layout->content = View::make(Config::get('views_corretor.clientes.insert'), compact('data'));
	}



	/**
	 * Store a newly created Clientees in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{
		$User = User::find($id);
		$model = Formulario::whereUserId($id)->where('tipo','=',Input::get('tipo'))->first();
		if (!isset($model)){
			$model = new $this->model;
		}	
		$model->fill(Input::all());
		//formata datas antes de salvar de dd/mm/YYYY para YYYY-mm-dd
		if(Input::has('casamento'))
			$model->casamento  = Carbon::createFromTimeStamp(strtotime(Input::get('casamento')))->format('Y-m-d');
		else
			$model->casamento = null;
		if(Input::has('nascimento'))
			$model->nascimento = Carbon::createFromTimeStamp(strtotime(Input::get('nascimento')))->format('Y-m-d');
		else
			$model->nascimento = null;		
		if(Input::has('data_exp'))
			$model->data_exp   = Carbon::createFromTimeStamp(strtotime(Input::get('data_exp')))->format('Y-m-d');
		else
			$model->data_exp = null;		

		//formata valores $$
		$model->renda      = UtilApp::Money(Input::get('renda'),true);
		$model->user_id    = $id;
		$model->tipo       = Input::get('tipo');     
        if ($model->save()) {
            return Redirect::route($this->model->settings['Cliente']['index.url'])->with('message', 'Registro gravado com Sucesso!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }
	}


}