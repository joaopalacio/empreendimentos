<?php

namespace App\Modules\Admin\Controllers\Corretor;
use View,Proposta,Reserva,Event,Image,Intermediaria,Carbon\Carbon,UtilApp,Imovel,Request,File,UploadImage,Config,User,RegrasProposta,DB,Auth,Hash,RegraIntermediaria,Empreendimento,Session,Validator,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Proposta');

class PropostasController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }


	/**
	 * Show the form for creating a new cidade
	 *
	 * @return Response
	 */
	public function make($id)
	{

		//TODO REGRAS DE NEGOCIO
		Session::put('url', '/admin/cidades/create');
		$Reserva = Reserva::with('cliente','imovel')->find($id);
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],	
			['url'=>URL::route('corretor.lista.reserva'),'class'=>'','active'=>'','texto'=>'Reservas'],
			['url'=>'#','class'=>'','active'=>'','texto'=>'Realizar Proposta'],						
		];				  
		$this->layout->with(compact('breadcrumb'));
		$RegrasProposta = RegrasProposta::with('intermediarias')->whereImovelId($Reserva->imovel_id)->first();
		$data =
		[
			'Reserva'        => $Reserva,
			'RegrasProposta' => $RegrasProposta
		];

		$this->layout->content = View::make(Config::get('views_corretor.propostas.insert'), compact('data'));
	}

	public function store($id){
		if(Request::ajax()){
			$reserva = Reserva::find($id);
			$Proposta             = new Proposta;
			$Proposta->desconto   = Input::get('desconto');
			$Proposta->entrada    = Input::get('entrada');
			$Proposta->chaves     = Input::get('chaves');
			$Proposta->fgts       = Input::get('fgts');
			$Proposta->porcent    = Input::get('porcent');
			$Proposta->prazo      = Input::get('prazo');
			$Proposta->reserva_id = $id;
			$Proposta->save();
			parse_str(Input::get('intermediarias'), $intermediarias);
			if (count($intermediarias))
			foreach($intermediarias['intermediarias'] as $element){
				$model              = new Intermediaria;
				$model->proposta_id = $Proposta->id;
				$model->valor       = UtilApp::Money($element['valor']);
				$model->description = @$element['description'];
				$model->numero      = $element['mes'];
				$model->vencimento  = Carbon::now();
				$model->save();
			}
			return 1;
		}
	}



	/**
	 * Show the form for creating a new cidade
	 *
	 * @return Response
	 */
	public function corretor()
	{

		//TODO REGRAS DE NEGOCIO
		Session::put('url', '/admin/cidades/create');
		$model = $this->model->get();
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.lista.propostas'),'class'=>'','active'=>'','texto'=>'Propostas'],				
		];				  
		$this->layout->with(compact('breadcrumb'));

		$this->model->btnsColumns   = 
	  	[	
			[
				'link'=>'admin.proposta.informacoes',
				'text'=>'<i class="fa  fa-comments-o"></i>',
				'array'=>
				[
					'class'=>'btn btn-info btn-s tooltips',
					'data-original-title'=>'Ver proposta completa',
					'data-placement'=>"top",
					'data-toggle'=>"modal"
				]
			],
			[
				'link'=>'corretor.proposta.destroy',
				'text'=>'<i class="fa fa-trash-o"></i>',
				'array'=>
				[
					'class'=>'btn btn-danger btn-s tooltips',
					'data-original-title'=>'Excluir Proposta',
					'data-placement'=>"top",
					'data-toggle'=>"modal"
				]
			],			
		];

		//THEAD alterando a ordem e colocando corretor
		$this->model->settings['thead'] = 
										[
											['class'=>'numeric','name'=>'Data'],
											['class'=>'','name'=>'Reserva'],
											['class'=>'','name'=>'Empreendimento'],
											['class'=>'numeric','name'=>'Imovel'],
											['class'=>'numeric','name'=>'Corretor'],
											['class'=>'numeric','name'=>'Valor Imovel'],
											['class'=>'numeric','name'=>'Valor Venda'],
											['class'=>'numeric','name'=>'Entrada'],
											['class'=>'numeric','name'=>'FGST'],
											['class'=>'numeric','name'=>'Status'],
											['class'=>'numeric','name'=>'']
										];
		//Seleciona todas reservas com nome do cliente,imovel,empreendimento e corretor								
		$model = DB::table('propostas')
				->join('reserva', 'reserva.id', '=', 'propostas.reserva_id')
			    ->join('imoveis', 'imoveis.id', '=', 'reserva.imovel_id')
			    ->join('regras_propostas', 'imoveis.id', '=', 'regras_propostas.imovel_id')
	            ->join('empreendimentos', 'empreendimentos.id', '=', 'imoveis.empreendimento_id')
	            ->join('users as ' . DB::getTablePrefix() . 'corretor', 'corretor.id','=','reserva.corretor_id')
	            ->leftJoin('users', 'users.id', '=', 'reserva.user_id')
	            ->orderBy('data','DESC')
	            ->where('reserva.corretor_id','=',Auth::user()->id)
	            ->select('empreendimentos.nome as empreendimento','imoveis.tipo as imovel','users.name as cliente','reserva.status as status_venda','corretor.name as corretor','reserva.user_id as id','reserva.id as reserva_id','valor_imovel','propostas.*','propostas.created_at as data')
	            ->paginate(20);
		$data = [
			'title'=>'',
			'thead'=>$this->model->settings['thead'],
			'td'=>$this->model->tableColumns,
			'buttons'=>$this->model->btnsColumns,
			'model'=>$model
		];
		$this->layout->content = View::make(Config::get('views_corretor.propostas.lista'), compact('data'));
	}






	public function destroy($id){
		$model = Proposta::with(
				[
					'reserva' => function($query)
					{
				    	$query->where('reserva.corretor_id','=',Auth::user()->id);
					},
					'intermediarias'
				])->find($id);
		$model->intermediarias()->delete();
		$model->delete();
		return Redirect::back();
	}

	
}