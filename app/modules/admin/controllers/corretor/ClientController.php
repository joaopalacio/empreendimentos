<?php

namespace App\Modules\Admin\Controllers\Corretor;
use View,User,Role,Auth,Session,Hash,Config,Validator,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'User');
class ClientController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }

	/**
	 * Display a listing of Clientees
	 *
	 * @return Response
	 */
	public function index()
	{
		Session::put('url', '/admin/clientes');

		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],			
			['url'=>URL::route($this->model->settings['Cliente']['index.url']),'class'=>'active','active'=>'active','texto'=>$this->model->settings['Cliente']['model']],
		];					  
		$this->layout->with(compact('breadcrumb'));

		$model = $this->model->meusClientes()->paginate(30);
		$data = [
			'title'=>$this->model->settings['Cliente']['title'],
			'thead'=>$this->model->settings['Cliente']['thead'],
			'insert'=>URL::route($this->model->settings['Cliente']['insert.url']), 
			'td'=>$this->model->tableColumns,
			'destroy'=>URL::route($this->model->settings['Cliente']['destroy.url'],''), 			
			'modal'=>$this->model->settings['Cliente']['modal'],
			'buttons'=>$this->model->btnsColumns['Cliente'],
			'model'=>$model
		];

		$this->layout->content = View::make(Config::get('views_corretor.clientes.lista'), compact('data'));
	}

	/**
	 * Show the form for creating a new Clientees
	 *
	 * @return Response
	 */
	public function create()
	{
		Session::put('url', '/admin/Clientees/create');

		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],			
			['url'=>URL::route($this->model->settings['Cliente']['index.url']),'class'=>'active','active'=>'active','texto'=>$this->model->settings['Cliente']['model']],
			['url'=>URL::route($this->model->settings['Cliente']['store.url']),'class'=>'active','active'=>'active','texto'=>'Novo Registro'],
		];				  
		$this->layout->with(compact('breadcrumb'));

		$data = [
			'title'=>$this->model->settings['Cliente']['title'],
			'formTitle'=>$this->model->settings['Cliente']['formTitle'],
			'route'=>URL::route($this->model->settings['Cliente']['store.url']), 
			'elements'=>$this->model->editColumns['Cliente']			
		];	
		$this->layout->content = View::make(Config::get('views_corretor.clientes.insert'), compact('data'));
	}

	/**
	 * Store a newly created Clientees in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $model = new $this->model;
		$roles              = Role::where('name','=','Cliente')->first();
		$model->name        = Input::get('name');
		$model->cpf         = Input::get('cpf');
		$model->password    = Hash::make(microtime());
		//TODO Enviar Email com Senha
		$model->email       = Input::get('email');
		$model->corretor_id = Auth::user()->id;
		$model->role_id  = $roles->id;        
        if ($model->save()) {
            return Redirect::route($this->model->settings['Cliente']['index.url'])->with('message', 'Registro gravado com Sucesso!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }
	}

	/**
	 * Show the form for editing the specified Clientees.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$modelFind = $this->model->find($id);
		Session::put('url', '/admin/Clientees/create');

		$breadcrumb = [
			['url'=>'admin.index','class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>'admin.index','class'=>'','active'=>'','texto'=>'Sistema'],			
			['url'=>$this->model->settings['Cliente']['index.url'],'class'=>'','active'=>'','texto'=>$this->model->settings['Cliente']['model']],
			['url'=>$this->model->settings['Cliente']['store.url'],'class'=>'active','active'=>'active','texto'=>'Editar Registro '.$this->model->settings['Cliente']['model']],
		];				  
		$this->layout->with(compact('breadcrumb'));

		$model = $this->model->paginate(30);
		$data = [
			'title'=>$this->model->settings['Cliente']['title'],
			'formTitle'=>$this->model->settings['Cliente']['formTitle'],
			'route'=>URL::route($this->model->settings['Cliente']['update.url'],$id),
			'method'=>'PUT',
			'model'=>$modelFind,
			'elements'=>$this->model->editColumns['Cliente']
		];

		$this->layout->content = View::make(Config::get('views_corretor.clientes.insert'), compact('data'));
	}



	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$model = $this->model->findOrFail($id);
		$model->name     = Input::get('name');
		if($model->cpf != Input::get('cpf')) {$model->cpf = Input::get('cpf');}
		if($model->email != Input::get('email')) {$model->email = Input::get('email');}		
		//TODO Enviar Email com Senha
	
        if ($model->updateUniques()) {
            return Redirect::route($this->model->settings['Cliente']['index.url'])->with('message', 'Registro alterado com Sucesso!');
        } else {
            return Redirect::back()->withErrors($model->errors())->withInput();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->model->destroy($id);

		return Redirect::back();
	}



}