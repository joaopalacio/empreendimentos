<?php

namespace App\Modules\Admin\Controllers\Corretor;
use View,Imovel,UtilApp,Corretor,Image,Hashids,Request,File,UploadImage,Config,User,RegrasProposta,DB,Auth,Hash,RegraIntermediaria,Empreendimento,Session,Validator,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Imovel');

class ImoveisController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }

	/*
	*
	*  Listar Imoveis disponivel para o Corretor Logado (Auth::user())
	*
	*/
	public function imoveisDisponiveis(){

	

		Session::put('url', '/admin/Empreendimento/create');
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('admin.index'),'class'=>'','active'=>'','texto'=>'Sistema'],	
			['url'=>URL::route('empreendimentos.index'),'class'=>'','active'=>'','texto'=>'Empreendimento'],			
			['url'=>URL::route('corretor.imoveis'),'class'=>'active','active'=>'active','texto'=>'Imoveis disponivel para você'],
		];					  
		$this->layout->with(compact('breadcrumb'));

		$model = DB::table('imoveis')
	            ->join('empreendimentos', 'empreendimentos.id', '=', 'imoveis.empreendimento_id')
	            ->join('regras_propostas', 'imoveis.id', '=', 'regras_propostas.imovel_id') 
	            ->join('imovel_corretor', 'imovel_corretor.imovel_id', '=', 'imoveis.id')
	            ->select('empreendimentos.nome as empreendimento','imoveis.*','regras_propostas.valor_imovel as valor')
	            ->where('imovel_corretor.user_id',Auth::user()->id)
	            ->where('imoveis.status','=','Disponível')
	          //->groupBy('chave_unica')
	            ->paginate(20);
	
		$data = [
			'title'=>$this->model->settings['title'],
			'thead'=>[
				['class'=>'','name'=>'Empreendimento'],
				['class'=>'','name'=>'Descrição'],
				['class'=>'','name'=>'Tipo'],
				['class'=>'numeric','name'=>'Área Privativa'],
				['class'=>'numeric','name'=>'Valor do Imovel'],
				['class'=>'numeric','name'=>'Cod'],
				['class'=>'numeric','name'=>''],

			],
			'td'=>['empreendimento','nome','ucfirst'=>'tipo','area_comum','valor', 'chave_unica'],
			'destroy'=>'', 
  			'modal'=>
  			[
  				'link'     =>'#makeReserva',
				'title'=>'Cadastrar Pré-Reserva do Imovel',
				'description'=>'<b>Tem certeza que deseja Cadastrar a Pré-reserva deste imovel?</b> </br>
								Este Imovel ficará reservado para você por 5 dias. Vá em Reservas e selecione o pré-cliente após.<br/> Caso não seja enviado os dados do cliente para aprovação o mesmo sairá de reserva',
				'btns'=>[]
			],			
			'buttons'=>
					  	[
							[
								'link'     =>'#makeReserva',
								'modal'    =>'true',
								'data-url' =>'corretor.store.reserva',
								'text'     =>'<i class="fa  fa-check-circle-o"></i>',
								'array'    =>
								[
									'class'=>'btn btn-info btn-xs tooltips btn-reserva',
									'data-original-title'=>'Fazer reserva',
									'data-placement'=>"top",
									'data-toggle'=>"modal",
									'url.ajax'=>URL::route('corretor.store.reserva')
								]
							],							
						]
			,
			'model'=>$model
		];

		$this->layout->content = View::make(Config::get('views_corretor.imoveis.lista'), compact('data'));		
	}



}