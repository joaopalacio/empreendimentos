<?php

namespace App\Modules\Admin\Controllers\Corretor;
use View,Reserva,Image,Imovel,Request,File,UploadImage,Config,User,RegrasProposta,DB,Auth,Hash,RegraIntermediaria,Empreendimento,Session,Validator,Input,Redirect,Lang,URL,App,ModelInterface;


App::bind('ModelInterface', 'Reserva');

class ReservasController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    protected $model; 

    public function __construct(ModelInterface $model){
    	$this->model = $model;
    }



	/**
	 * Show the form for creating a new cidade
	 *
	 * @return Response
	 */
	public function corretor()
	{

		//TODO REGRAS DE NEGOCIO
		Session::put('url', '/admin/cidades/create');
		$model = $this->model->minhasReservas()->get();
		$breadcrumb = [
			['url'=>URL::route('admin.index'),'class'=>'fa-home','active'=>'','texto'=>'Home'],
			['url'=>URL::route('corretor.lista.reserva'),'class'=>'','active'=>'','texto'=>'Reservas'],			
		];				  
		$this->layout->with(compact('breadcrumb'));
		$model = DB::table('reserva')
			    ->join('imoveis', 'imoveis.id', '=', 'reserva.imovel_id')
	            ->join('empreendimentos', 'empreendimentos.id', '=', 'imoveis.empreendimento_id')	            
	            ->leftJoin('users', 'users.id', '=', 'reserva.user_id')
	            //->leftJoin('propostas','reserva.id', '=', 'propostas.reserva_id')  
	            ->select('empreendimentos.nome as empreendimento','imoveis.tipo as imovel','users.name as cliente','reserva.*')
	            ->where('reserva.corretor_id',Auth::user()->id)
	            ->paginate(20);
		$data = [
			'title'=>'Lista de reservas feitas pelo corretor',
			'thead'=>$this->model->settings['thead'],
			'td'=>$this->model->tableColumns,
			'destroy'=>URL::route($this->model->settings['destroy.url'],''), 			
			'modal'=>$this->model->settings['modal'],
			'buttons'=>$this->model->btnsColumns,
			'model'=>$model
		];
		$this->layout->content = View::make(Config::get('views_corretor.reservas.lista'), compact('data'));
	}
	/**
	 * Store a newly created cidade in storage.
	 *
	 * @return Response
	 */
	public function store_preReserva()
	{	
		if(Request::ajax()){
			$id = Input::get('id');
			$corretor = Auth::user()->id;
			$imovel = Imovel::find($id);
			if($imovel->status=="Disponível")
			{
				$imovel->status= 'Reserva';
				$imovel->save();
				$model = new Reserva;
				$model->imovel_id = $id;
				$model->corretor_id = $corretor;
				$model->save();
				return 1;
			}else
			{
				return 0;
			}
		}
	}

	
}