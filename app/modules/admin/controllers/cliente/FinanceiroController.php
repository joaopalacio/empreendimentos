<?php

namespace App\Modules\Admin\Controllers\Cliente;
use View,Corretor,Role,Session,Event,Hash,Validator,Crypt,Config,Input,Boleto,Empreendimento,Banco,Redirect,Lang,URL,App,ModelInterface,Contrato,Parcela,DateTime;
use Proposta,Reserva,User;

use OpenBoleto\Banco\Itau;
use OpenBoleto\Agente;

class FinanceiroController extends \BaseController {


	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'admin::layouts.master';

    public function parcelas(){
    	if(!Session::get('cliente.imovel')){
    		return false;	
    	}
		$Contrato = Contrato::with('parcelas')->where('proposta_id','=',Session::get('cliente.imovel'))->get()->toArray();

		/*if(isset($Contrato))
			foreach ($Contrato[0]['parcelas'] as $key => $value) {
				echo "Valor: ".$value['valor']." - Vencimento:".$value['vencimento'];
				echo "<br/>";
			}*/
			
	
	    $this->layout->content = View::make(Config::get('views_cliente.financeiro.parcelas'))->with(compact('Contrato'));    	

	}

	public function boleto($chave){
		$id = Crypt::decrypt($chave);
		$parcela = Parcela::with('contrato')->find($id);
		$proposta = Proposta::find($parcela->contrato->id);
		$reserva = Reserva::with('imovel')->find($proposta->reserva_id);
		$cliente = User::with('formulario')->find($reserva->user_id);
		$empreendimento = Empreendimento::find($reserva->imovel->empreendimento_id);
		$banco = Banco::find($empreendimento->banco_id);
		$sacado = new Agente($cliente->name, @$cliente->formulario[0]->CPF,@$cliente->formulario[0]->endereco,@$cliente->formulario[0]->cep,@$cliente->formulario[0]->cidade,@$cliente->formulario[0]->uf);
		$cedente = new Agente($banco->cedente, $banco->cnpj, @$banco->endereco, @$banco->cep,@$banco->cidade,@$banco->uf);

		$array = [
		    // Parâmetros obrigatórios
		    'dataVencimento' => new DateTime($parcela->vencimento),
		    'valor' => $parcela->valor,
		    'sequencial' => $parcela->id, // Até 11 dígitos
		    'sacado' => $sacado,
		    'cedente' => $cedente,
		    'agencia' => $banco->agencia, // Até 4 dígitos
		    'carteira' =>$banco->carteira, // 3, 6 ou 9
		    'conta' => $banco->conta, // Até 7 dígitos
		    'contaDv' => $banco->contaDV,
		    'agenciaDv' => $banco->agenciaDv,
		    'descricaoDemonstrativo' => array( // Até 5
		        $parcela->description
		    ),
		    'instrucoes' => array( // Até 8
		        'Não receber após o vencimento.',
		    ),
		    'moeda' => Itau::MOEDA_REAL,
		    'logoPath' => 'http://boletophp.com.br/img/opensource-55x48-t.png',
		    'moraMulta' => 0,
		    'outrasDeducoes' => 0,
		    'outrosAcrescimos' => 0,
		    'valorCobrado' => 0,
		    'valorUnitario' => 0,
		];
		
		$boleto = new Itau($array);

		//echo $boleto->getNossoNumero();
		$model = Boleto::where('nosso_numero','=',$boleto->getNossoNumero())->count();
		if(!$model){
			$model = new Boleto;
			$model->nosso_numero = $boleto->getNossoNumero();
			$model->parcela_id = $id;
	
			$model->boleto = $boleto->getLinhaDigitavel();
			$model->valor = $parcela->valor;
			$model->vencimento = ($parcela->vencimento);
			$model->status = "pendente";
			$model->save();
		}
		echo $boleto->getOutput();

		exit;
	}
}