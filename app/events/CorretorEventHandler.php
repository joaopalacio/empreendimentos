<?php

class CorretorEventHandler {

    /**
     * Handle user login events.
     */
    public function onGeraAcesso($event)
    {
        $model                  = Corretor::find($event);
        $uniqid                 = substr(md5(uniqid()),0,6);
        $model->password      = Hash::make($uniqid);
        $model->status_acesso = 'liberado';
        $model->save();    
        $model->password = $uniqid;
        $data['Corretor'] = $model;
        Mail::send('admin::emails.cadastro_corretor', compact('data') , function($msg) use ($model)
        {
            $msg->to($model->email, $model->name)->subject('Seja bem vindo! Seu cadastro foi feito.');
        });        
    }

    
    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     * @return array
     */
    public function subscribe($events)
    {
        $events->listen('corretor.gerar.acesso', 'CorretorEventHandler@onGeraAcesso');

    }

}

?>