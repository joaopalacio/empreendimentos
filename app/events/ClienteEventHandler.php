<?php

class ClienteEventHandler {

    /**
     * Handle user approves
     */
    public function onGerarAcesso($event)
    {
        $cliente                = User::find($event);
        $uniqid                 = substr(md5(uniqid()),0,6);
        $cliente->password      = Hash::make($uniqid);
        $cliente->status_acesso = 'liberado';
        $cliente->save();
        $data['Cliente'] = $cliente;
        $data['Cliente']->password = $uniqid;
        Mail::send('admin::emails.cadastro_cliente', $data , function($msg){
            $msg->to($cliente->email, $cliente->name)->subject('Seja bem vindo! Sua Proposta foi aceita.');
        });
    }


    /**
     * Handle user approves
     */
    public function onGeraBoleto($proposta_id)
    {
        $proposta  = Proposta::with('intermediarias')->find($proposta_id);
        
    }
    
    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     * @return array
     */
    public function subscribe($events)
    {
        $events->listen('criar.acesso', 'CorretorEventHandler@onGerarAcesso');

    }

}

?>