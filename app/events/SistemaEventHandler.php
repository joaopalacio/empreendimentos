<?php

class SistemaEventHandler {

    /**
     * Handle user login events.
     */
    public function onReservas()
    {
       
        $obj = new DateTime('-5 day');

        $reservas = Reserva::select(DB::raw("DATE_FORMAT(DATE(created_at), '%Y-%m-%d') as created_at"),'id')->where('user_id',"=",NULL)->where('created_at', '<=', $obj->format('Y-m-d'))->get();
        if(count($reservas)){
            foreach ($reservas as $key => $value) {
                $array[] = $value->id;        
            }
            Reserva::destroy($array);
        }
    }

    
    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     * @return array
     */
    public function subscribe($events)
    {
        $events->listen('sistema.valida.reservas', 'SistemaEventHandler@onReservas');
    }

}

?>