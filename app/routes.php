<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 										    ['as' =>'site.index','uses'=>'HomeController@index']);
Route::get('/contato', 										['as' =>'site.contato','uses'=>'HomeController@contato']);
Route::get('/busca', 										['as' =>'site.busca','uses'=>'HomeController@busca']);
Route::get('/sobre', 										['as' =>'site.sobre','uses'=>'HomeController@sobre']);
Route::get('/empreendimentos', 								['as' =>'site.empreendimentos','uses'=>'HomeController@empreendimentos']);
Route::get('/empreendimento/{cidade}/{empreendimento}/', 	['as' =>'site.detalhe','uses'=>'HomeController@empreendimentoDetalhe']);



Route::get('/photos/{arquivo}/{width}/{height}/', 									['as' =>'gera.imagens','uses'=>'HomeController@photos']);


//Ajax pegar os bairros de uma cidade
Route::get('/get/bairros/', array('as'		=>'site.getBairros','uses' => 'AjaxController@getBairros'));
//REMIND PASSWORD	
Route::post('form/contato', array('uses' => 'HomeController@PostSobre','as' => 'form.contato'));


//REMIND PASSWORD	
Route::post('password/reset', array(
  'uses' => 'RemindersController@request',
  'as' => 'password.request'
));
Route::get('password/reset/{token}', array(
  'uses' => 'RemindersController@reset',
  'as' => 'password.reset'
));
Route::post('password/reset/{token}', array(
  'uses' => 'RemindersController@update',
  'as' => 'password.update'
));

//COMPOSER
View::creator('includes.banner', function($view)
{
	$view->with('banners', Banner::all());
});