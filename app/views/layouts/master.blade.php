<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>{{isset($page) ? $page->title." | " : ""}}MaschioShin Construtora</title>

<meta name="description" content="{{isset($page) ? $page->description : 'Seja bem vindo ao site da MaschioShin Construtora, muitas ofertas e empreendimentos para você'}}" />
<meta name="keywords" content="{{isset($page) ? $page->keywords : 'Empreendimentos, ofertas'}}">	

@if(isset($page))
	<meta property="og:title" content="{{isset($page) ? $page->title." | " : ""}}MaschioShin Construtora" />
	<meta property="og:url" content="{{Request::url()}}" />
	<meta property="og:image" content="{{$page->og_image}}" />
	<meta property="og:description" content="{{$page->description}}" />		
@endif

{{HTML::style('assets/front/css/base.css')}}
{{HTML::script('assets/front/js/script.min.js')}}



@yield('head-include')
</head>

<body>
	<div id='mask' class='close_modal'></div>  
	<div id="geral">
		@include('includes.header')
		@yield('content')
		@include('includes.footer')
	</div>
	<script type="text/javascript">
		@yield('documentReady')
		@yield('extraReady')
	</script>
	
</body>
</html>	
