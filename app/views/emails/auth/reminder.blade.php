<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Esqueci minha senha</h2>

		<div>
			Para resetar sua senha, complete esse formulario: {{ URL::to('password/reset', array($token)) }}.
		</div>
	</body>
</html>