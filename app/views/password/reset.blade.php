@if (Session::has('error'))
  {{ trans(Session::get('reason')) }}
@endif
 
{{ Form::open(array('route' => array('password.update', $token))) }}
 
  <p>{{ Form::label('email', 'CPF') }}
  {{ Form::text('cpf') }}</p>
 
  <p>{{ Form::label('password', 'Senha') }}
  {{ Form::text('password') }}</p>
 
  <p>{{ Form::label('password_confirmation', 'Confirmar Senha') }}
  {{ Form::text('password_confirmation') }}</p>
 
  {{ Form::hidden('token', $token) }}
 
  <p>{{ Form::submit('Enviar') }}</p>
 
{{ Form::close() }}