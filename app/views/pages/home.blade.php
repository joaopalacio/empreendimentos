@extends('layouts.master')

@section('head-include')
	{{HTML::style('assets/front/css/home.css')}}
	{{HTML::style('assets/front/css/jquery.bxslider.css')}}
	{{HTML::script('assets/front/js/jquery.bxslider.js')}}
@stop

@section("content")
	@include('includes.banner')
		<div id="conteudo">
			<div class="alinha">
				@include('includes.form_busca')



				@if(isset($model))
					@foreach($model as $element)
						@if(count($element->uploads)>0)
							<div class="{{$element->tipo_destaque}}">
								<?php $cidade = Cidade::find($element->cidade_id);?>
								<div class="tarja"><span>{{$element->status_venda}}</span></div>
								<?php $url='';?>
								@foreach($element->uploads as $photo)
									<?php $url = $photo->url; ?>
								@endforeach								
								<a href="empreendimento/{{ @$cidade->{'url-clean'} }}/{{ @$element->{'url-clean'} }}">
									<?php 
										if ($element->tipo_destaque=='imovelP') $size = 'small';
										if ($element->tipo_destaque=='imovelG') $size = 'medium';
										if ($element->tipo_destaque=='imovelDestaque') $size = 'interna';
									?>
									<img style="" src="{{Config::get('params.get.empreendimentos').$url.'_'.$size.'.jpg'}}">
								</a>																	
								<p>
									<span>
											{{@$element->nome}} &bull; 
									</span> 
									{{@$cidade->cidade}} | {{@$element->bairro->bairros}} <br/>
									{{@$element->caption}}
								</p>
							</div> <!-- fim imovelG -->						
						@endif
					@endforeach
				@endif	
				


				<!--<a href="#" class="btnCarregarConteudo" title="CARREGAR MAIS CONTEÚDOS">CARREGAR MAIS CONTEÚDOS</a>-->

			</div>
		</div> <!-- fim conteudo -->	
@stop

@section('documentReady')

	$('#bannersUl').bxSlider();

	$(".ulCidades > li > a").on('click',function(e){
		e.preventDefault()
		var id = $(this).attr('data-id');
		$(".textCidade").empty().html($(this).html());	
		$('.scrollbarPadrao').hide();
		$(".textBairro").empty().html('Carregando os bairros...');
		$.ajax
		(
			{
				type: "GET",
				url: "{{route('site.getBairros')}}",
				data: { cidade_id:id }
			}
		)
		.done
		(function( data ) 
			{
				if(data){
					var html ="";
					$(".textBairro").empty().html('Selecione o Bairro');
					$.each(data,function(){
						html += "<li><a href='#' data-id='"+(this.id)+"'>"+this.bairros+"</a></li>";
					})

					$(".ulBairros").empty().html(html);
				}
			}
		);
	})

	$(document).on('click',".ulBairros > li > a",function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');
		$(".textBairro").empty().html($(this).html());	
		$('.scrollbarPadrao').hide();		
	});

	$(document).on('click',".ulTipos > li > a",function(e){
		e.preventDefault();
		$(".textTipo").empty().html($(this).html());	
		$('.scrollbarPadrao').hide();		
	});		

	$(document).on('click',".ulEstagios > li > a",function(e){
		e.preventDefault();
		$(".textEstagio").empty().html($(this).html());	
		$('.scrollbarPadrao').hide();		
	});		

@stop