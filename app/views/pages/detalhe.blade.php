@extends('layouts.master')
@section('head-include')
	{{HTML::style('assets/front/css/internas.css')}}
	<!--<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA3MF4oWp7-MKaWfJ2nZAl06JtDmB8S4J8&sensor=false"></script>-->
	<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
	
@stop
@section("content")
	
		<div id="conteudo">
			<div class="alinha">
				{{--TODO ADICIONAR BUSCA--}}

				
				<div class="destaqueObra">
					<div class="tarja"><span>{{$data->status_venda}}</span></div>
					<?php $count=0;?>
					@foreach($data->uploads as $photo)
						@if($count==0)
							<li><img style="width:1180px;height:420px" src="/{{Config::get('params.get.empreendimentos').$photo->url.'_interna.jpg'}}"></li>
						@endif	
						<?php $count++;?>
					@endforeach								
				</div> <!-- fim destaqueObra -->
				<!--MODAIS-->
				<div class="modalPlanta modais">
					<a href="#" class="btnFecharModal close_modal">fechar</a>
					<span class="nomeEmpreendimento">[nome do empreendimento] - Apartamento 01</span>

					<p class="preco">Preço: R$ 250.000,00</p>

					<span class="planta">
						<img src="imagens/img-planta.jpg">
					</span>
				</div>

				<div class="modalUnidades modais">
					<a href="#" class="btnFecharModal close_modal">fechar</a>
					<span class="nomeEmpreendimento">Unidades Disponíveis - {{$data->nome}}</span>

					<p class="unidades">Unidades</p>

					<br style="clear:both">

					<ul>
						<?php 
							$array = [];
							foreach(@$data->imoveis as $key =>$element):
								if(!array_key_exists($element->nome, $array)){
									$imovel                         = Imovel::with('regras','uploads')->find($element->id);
									$array[$element->nome]          = [];
									$array[$element->nome]['id']    = $element->id;
									$array[$element->nome]['nome']  = $element->nome;									
									$array[$element->nome]['preco'] = $imovel->regras[0]->valor_imovel;								
								}
							endforeach;

						?>
						@foreach($array as $key =>$element)
							<li><a href="#" class="infoImovel" data-id="{{$element['id']}}">{{$element['nome']}} - {{UtilApp::Money($element['preco'],false)}}</a></li>
						@endforeach
					</ul>


				</div>

				<div class="carrosel">
					<ul class="links">
						<li>
							<a href="#" class="links atv" rel="detalhes">detalhes</a>
						</li>
						<li>
							<a href="#" class="links" rel="unidades">unidades</a>
						</li>
						<li>
							<a href="#" class="links" rel="estagiosObra">ACOMPANHE A OBRA</a>
						</li>
					</ul>
					
					<div class="detalhes">
						<ul>
							<li>
								<h3>Detalhes</h3>
								<p>{{$data->description}}</p>
							</li> 

							<li>
								<h3>Unidades Disponíveis</h3>
								<p>Veja abaixo as plantas deste empreendimento</p>
								<a href="#" class="btn openModais" title="planta e valores">planta e valores</a>
							</li> 
							<li>
								<h3>Andamento da obra</h3>
								<ul class="estagiosObra">
									@foreach($data->estagios as $key =>$element)
									<li>
										<span class="nomeFase">{{ucfirst($element->estagio)}}</span>
										<span class="linhaPorcentagem">
											<span style="width:{{$element->porcent}}%"></span>
										</span>
										<span class="valor">{{number_format($element->porcent,0)}}%</span>
									</li>
									@endforeach
								</ul>
							</li>

						</ul>
					</div> <!-- fim detalhes -->

					<div class="imagens">
						<ul class="lista">
								@foreach($data->uploads as $photo)
									<li><img style="width:458px;height:420px" src="/{{Config::get('params.get.empreendimentos').$photo->url.'_small.jpg'}}"></li>
								@endforeach								
						</ul>

						<ul class="bullets">
							<!-- <li><a href="#" class="atv"></a></li> -->
							
						</ul>
					</div> <!-- fim imagens -->

				</div> <!-- fim carrosel -->

				<div class="informacoes">
					<div class="maps">
						<div class="tarja" style="z-index:8000"><span>Localização</span></div>
						<div id="map" style="width:460px;height:420px"></div>
					</div> <!-- fim maps -->

					<div class="itens">
						<h3>Lorem Ipsum</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
						tempor incididunt ut labore et dolore magna aliqua.</p>

						<ul>
							<li>Escolas</li>
							<li>Academias</li>
							<li>Supermercados</li>
							<li>Açougues</li>
							<li>Padarias</li>
							<li>Escolas</li>
							<li>Academias</li>
							<li>Supermercados</li>
							<li>Açougues</li>
							<li>Padarias</li>
						</ul>
						<ul class="aux">
							<li>Escolas</li>
							<li>Academias</li>
							<li>Supermercados</li>
							<li>Açougues</li>
							<li>Padarias</li>
							<li>Escolas</li>
							<li>Academias</li>
							<li>Supermercados</li>
							<li>Açougues</li>
							<li>Padarias</li>
							
						</ul>
					</div> <!-- fim itens -->

				</div> <!-- fim informacoes -->

				<div class="contato">
					<div class="cont1">
						<h4>Fale com um <br/>corretor.</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
							tempor incididunt ut labore et dolore </p>
					</div> <!-- fim telefone -->
					<div class="cont2">
						<h4>Ou entre em <br/>
							contato.</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
										tempor incididunt ut labore et dolore </p>
					</div> <!-- fim telefone -->
				</div> <!-- fim contato -->

			</div>
		</div> <!-- fim conteudo -->
@stop

@section('documentReady')
	
	$(document).on('click','.openModais',function(e){
		e.preventDefault();
		show_modal();
		$(".modalUnidades").show();		
	});
 	$(document).on('click','.close_modal',function(e){  
  		e.preventDefault();
        //use the function to close it  
        close_modal();  
  
    });  	

	var document_height = $(document).height();
	$('#mask').css({ height : document_height+"px" });  
	function close_modal(){  
  
	    //hide the mask  
	    $('#mask').fadeOut(500);  
	    $(".modais").fadeOut(500); 	   
	  
  	}
	function show_modal(){  
	  
		var window_width = $(window).width();
		var window_height = $(window).height();		
	    //set display to block and opacity to 0 so we can use fadeTo  
	    $('#mask').css({ 'display' : 'block', opacity : 0.8,height : document_height+"px" });  
	  
	    //fade in the mask to opacity 0.8  
	    $('#mask').show();  
	  
	     //show the modal window  
	  	$('#mask').css({ height : document_height+"px" });  	  	
	  	var document_height = $(document).height();
		$('#mask').css({ height : document_height+"px" });  
	}  

	var map;
	 
	function initialize() {
	    var latlng = new google.maps.LatLng(-18.8800397, -47.05878999999999);
	 
	    var options = {
	        zoom: 16,
	        center: latlng,
	        mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	 
	    map = new google.maps.Map(document.getElementById("map"), options);
	}
	 
	initialize();
@stop