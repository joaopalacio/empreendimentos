@extends('layouts.master')
@section('head-include')
	{{HTML::style('assets/front/css/internas.css')}}
@stop
@section("content")
	@include('includes.banner')
		<div id="conteudo">
			<div class="alinha">
				<div class="conteudoSobre">
					<div class="txt">
						<h3> @if(count($model)) {{$model->title1}} @endif </h3>
						<p> @if(count($model)) {{$model->descr1}} @endif </p>
					</div> <!-- fim txt -->

					<div class="img">
						<img src="assets/front/imagens/foto-sobre.jpg">
					</div> <!-- fim img -->

					<div class="img">
						<img src="assets/front/imagens/foto-sobre.jpg">
					</div> <!-- fim img -->

					<div class="txt">
						<h3>@if(count($model)) {{$model->title2}} @endif </h3>

						<p>@if(count($model)) {{$model->descr2}} @endif </p>
					</div> <!-- fim txt -->

					<div class="txt txtG">
						<h3>@if(count($model)) {{$model->title3}} @endif </h3>

						<p>@if(count($model)) {{$model->descr3}} @endif </p>
					</div> <!-- fim txt -->

				</div> <!-- fim conteudoSobre -->
			</div>
		</div> <!-- fim conteudo -->	
@stop

