@extends('layouts.master')

@section('head-include')
	{{HTML::style('assets/front/css/internas.css')}}
@stop

@section("content")
		<div id="conteudo">
			<div class="alinha">
				<div class="conteudoContato">
					<form>
						<h3>Contato</h3>
						<div>
							<label>*Nome:</label>
							<input type="text" class="cpoG nome" />
						</div>
						<div>
							<label>*E-mail:</label>
							<input type="text" class="cpoM emailcontato" />
						</div>
						<div>
							<label>Telefone:</label>
							<input type="text" class="cpoP1 telefone" style="width:168px" />
						</div>
						<div>
							<label>*Mensagem:</label>
							<textarea class="msg"></textarea>
						</div>

						<input type="buttom" class="btnEnviar enviaCont">

						<small>*Campos obrigatórios</small>
					</form>
					
				</div> <!-- fim conteudoContato -->

				<div class="mapa">
					<div class="tarja"><span>Localização</span></div>
					<iframe width="1180" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q={{$data->endereco}}&key=AIzaSyBzAllnPQxsZv3eqY57Mr7_3odRlSblld0&language=pt_BR"></iframe>

					<div class="endereco">
						<p>{{$data->endereco}} , {{$data->cep}} - {{$data->cidade}} / {{$data->estado}}</p>
					</div> <!-- fim endereco -->

				</div> <!-- fim mapa -->

			</div>
		</div> <!-- fim conteudo -->
@stop

@section('documentReady')

	$(".enviaCont").on('click',function(e){
		e.preventDefault()
		var valid = true;
		$('.error').removeClass('error');
		if ($('.nome').val()==''){
			$('.nome').addClass('error');
			valid =  false;
		}
		if ($('.msg').val()==''){
			$('.msg').addClass('error');
			valid = false;
		}
		if ($('.emailcontato').val()==''){
			$('.emailcontato').addClass('error');
			valid = false;
		}		
		if(!valid){return false;}		
		$.ajax
		(
			{
				type: "POST",
				url: "{{route('form.contato')}}",
				data: { 
					nome:$('.nome').val(),
					email:$('.emailcontato').val(),
					telefone:$('.telefone').val(),
					mensagem:$('.msg').val(), 
				}
			}
		)
		.done
		(function( data ) 
			{
				$('.msg').val('')	;
				$('.nome').val('')	;
				$('.emailcontato').val('')	;
				$('.telefone').val('')	;
			}
		);
	})
@stop