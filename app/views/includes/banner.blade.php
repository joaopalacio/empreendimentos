		<div id="banners">
			<!-- <a href="#" class="nav prev">prev</a>
			<a href="#" class="nav next">next</a> -->
			<ul id="bannersUl">
				@if(isset($banners))
				@foreach ($banners as $element)
					<li class="{{$element->id}}">
						<a href="{{$element->link or '#'}}">
							<img src="{{Config::get('params.get.banners').$element->image}}.jpg">
						</a>
					</li>
				@endforeach			
				@endif
			</ul>
		</div> <!-- fim banners -->