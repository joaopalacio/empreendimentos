				<div id="busca">
					<p class="descr">Busca rápida</p>

					<ul class="listaFiltro">
						<li>
							<div class="cposBusca">
								<a href="#" class="textCidade" title="Selecione uma Cidade">Selecione uma Cidade</a>
							</div> <!-- fim cposBusca -->
							<div id="scrollbar1" class="scrollbarPadrao">
								<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
								<div class="viewport">
									 <div class="overview">
										<ul class="ulCidades">
											@if(isset($cidades))
												@foreach ($cidades as $element)
													<li><a href="#" data-id="{{$element->id}}">{{$element->cidade}}</a>
												@endforeach
											@endif
										</ul>       
									</div>
								</div>
							</div>	
						</li>

						<li>
							<div class="cposBusca">
								<a href="#" class="textBairro" title="Selecione um Bairro">Selecione um Bairro</a>
							</div> <!-- fim cposBusca -->
							<div id="scrollbar2" class="scrollbarPadrao">
								<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
								<div class="viewport">
									 <div class="overview">
										<ul class="ulBairros">
											<li><a href="#">Selecione a cidade</a>
										</ul>       
									</div>
								</div>
							</div>	
						</li>

						<li>
							<div class="cposBusca">
								<a href="#" class="textTipo" title="Selecione um Tipo">Selecione um Tipo</a>
							</div> <!-- fim cposBusca -->
							<div id="scrollbar3" class="scrollbarPadrao">
								<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
								<div class="viewport">
									 <div class="overview">
										<ul class="ulTipos">
											@if(isset($tipos))
												@foreach ($tipos as $element)
													<li><a href="#">{{ucfirst($element)}}</a>
												@endforeach
											@endif
										</ul>       
									</div>
								</div>
							</div>	
						</li>

						<li>
							<div class="cposBusca">
								<a href="#" class="textEstagio" title="Estágio da obra">Estágio da obra</a>
							</div> <!-- fim cposBusca -->
							<div id="scrollbar4" class="scrollbarPadrao">
								<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
								<div class="viewport">
									 <div class="overview">
										<ul class="ulEstagios">
											@if(isset($estagios))
												@foreach ($estagios as $element)
													<li><a href="#">{{ucfirst($element)}}</a>
												@endforeach
											@endif
										</ul>       
									</div>
								</div>
							</div>	
						</li>

						<li>
							<div class="resultadoBusca">
								
							</div> <!-- fim cposBusca -->
						</li>

					</ul>

					<a href="#" class="lupa"></a>

					
				</div> <!-- fim busca -->

