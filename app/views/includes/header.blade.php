		<div id="header">
			<div class="alinha">
				<h1><a href="{{route('site.index')}}">Maschio shin construtora</a></h1>

				<div class="menu">
					<ul>
						<li><a href="{{route('site.index')}}" title="HOME" class="atv">HOME</a><span class="seta"></span></li>
						<li><a href="{{route('site.empreendimentos')}}" title="EMPREENDIMENTOS">EMPREENDIMENTOS</a></li>
						<li><a href="{{route('site.sobre')}}" title="SOBRE">SOBRE</a></li>
						<li><a href="{{route('site.contato')}}" title="CONTATO">CONTATO</a></li>
					</ul>
					<ul class="sub">

						<li><a href="#" title="LOGIN" class="btnLogin">LOGIN</a></li>
					</ul>

					<div class="boxLogin">
						<div class="cpos login">
							<form>
								<label>E-MAIL</label>
								<input type="text" class="email">

								<label>SENHA</label>
								<input type="password" class="senha">

								<input type="submit" style="border:0px" value="" class="btnEnviar btnEnviarLogar">
							</form>
						</div> <!-- fim cpos -->

						<div class="boxEsqueciSenha">
							<a href="#" class="EsqueciSenha" title="Esqueceu a senha?">Esqueceu a senha?</a>
						</div> <!-- fim boxEsqueciSenha -->
						<div class="cpos cpfBox" style="display:none;bottom:-63px">
							<form>
								<label>CPF</label>
								<input type="text" class="cpf" placeholder="Digite seu cpf">
								<input type="submit" value="" style="border:0px" class="btnEnviar btnCpf">
							</form>
						</div> <!-- fim cpos -->
					</div> <!-- fim boxLogin -->
				</div> <!-- fim menu -->
				
			</div>

			<br style="clear:both">
		</div> <!-- fim header -->


	@section('extraReady')
		
		$(function(){
			$(".EsqueciSenha").on('click',function(e){
				e.preventDefault()
				$(".boxLogin").css({'bottom':'-63px'});
				$(".boxLogin").css({'border':'1px solid #EEE;'});
				$(".login, .boxEsqueciSenha").hide();
				$(".cpfBox").show();
			});

			$(document).on('click','.btnEnviarLogar',function(e){
				e.preventDefault();
				$.ajax
				(
					{
						type: "POST",
						url: "{{route('login.made')}}",
						data: { email:$('.email').val(),senha:$('.senha').val()}
					}
				)
				.done
				(function( data ) 
					{
						if(data!='')
						{
							top.location.href = '{{route('admin.index')}}'
						}else
						{
							$('.email').addClass('error');
							$('.senha').val('');
							$('.senha').addClass('error');
						}
					}
				);				
			});

			$(document).on('click','.btnCpf',function(e){
				e.preventDefault();
				$.ajax
				(
					{
						type: "POST",
						url: "{{route('password.request')}}",
						data: { cpf:$('.cpf').val()}
					}
				)
				.done
				(function( data ) 
					{
						if (data=='reminders.user'){
							$(".cpf").addClass('error');
						}
					}
				);				
			});
		});

	@stop		