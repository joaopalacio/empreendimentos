<?php

use OpenBoleto\Banco\Bradesco;
use OpenBoleto\Agente;

 class MyListeners
 {
	 public function geraBoletos($id)
	 {			
			$sacado = new Agente('Fernando Maia', '023.434.234-34', 'ABC 302 Bloco N', '72000-000', 'Brasília', 'DF');
			$cedente = new Agente('Empresa de cosméticos LTDA', '02.123.123/0001-11', 'CLS 403 Lj 23', '71000-000', 'Brasília', 'DF');
			$new = new Cidade;
			$new->cidade = "teste1";
			$new->save();
			$boleto = new Bradesco(array(
			    // Parâmetros obrigatórios
			    'dataVencimento' => new DateTime('2013-01-24'),
			    'valor' => 23.00,
			    'sequencial' => 1234567, // Para gerar o nosso número
			    'sacado' => $sacado,
			    'cedente' => $cedente,
			    'agencia' => 1724, // Até 4 dígitos
			    'carteira' => 6,
			    'conta' => 1040305, // Até 8 dígitos
			    'convenio' => 1234, // 4, 6 ou 7 dígitos
			));

			echo $boleto->getOutput();	
	 }

	 public function subscribe($events)
	 {
		 $events->listen('event.geraBoletos', 'MyListeners@geraBoletos');
	 }
}
