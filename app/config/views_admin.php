<?php
return 
[
	'indicadores'=>	
	[
		'lista'=>'admin::Administrador.indicadores.index',
		'insert'=>'admin::Administrador.indicadores.crud'
	],
	'corretores'=>	
	[
		'lista'=>'admin::Administrador.corretores.index',
		'insert'=>'admin::Administrador.corretores.crud'
	],	
	'cidades'=>	
	[
		'lista'=>'admin::Administrador.cidades.index',
		'insert'=>'admin::Administrador.cidades.crud',
		'bairros'=>	
		[
			'lista'=>'admin::Administrador.bairros.index',
			'insert'=>'admin::Administrador.bairros.crud'
		],
	],	
	'faq'=>	
	[
		'lista'=>'admin::Administrador.faq.index',
		'insert'=>'admin::Administrador.faq.crud'
	],		
	'site'=>	
	[
		'lista'=>'admin::Administrador.site.index',
		'insert'=>'admin::Administrador.site.crud'
	],	
	'banners'=>	
	[
		'photos'=>'admin::Administrador.banners.photos',
	],	
	'settings'=>	
	[
		'index'=>'admin::Administrador.settings.crud',
	],	
	'bancos'=>	
	[
		'lista'=>'admin::Administrador.bancos.index',
		'insert'=>'admin::Administrador.bancos.crud'
	],
	'estagios'=>
	[
		'lista'=>'admin::Administrador.estagios.index'
	],
	'valores'=>
	[
		'lista'=>'admin::Administrador.valores.index',
		'insert'=>'admin::Administrador.valores.crud'
	],
	'empreendimentos'=>	
	[
		'lista'=>'admin::Administrador.empreendimentos.index',
		'insert'=>'admin::Administrador.empreendimentos.crud',
		'photos'=>'admin::Administrador.empreendimentos.photos',
	],		
	'imoveis'=>	
	[
		'lista'=>'admin::Administrador.imoveis.index',
		'insert'=>'admin::Administrador.imoveis.crud',
		'photos'=>'admin::Administrador.imoveis.photos',
	],	
	'reservas'=>
	[
		'show'=>'admin::Administrador.reservas.show',
		'lista'=>'admin::Administrador.reservas.index',
	],	
	'propostas'=>
	[
		'lista'=>'admin::Administrador.propostas.index',
		'documento'=>'admin::Administrador.propostas.documento',
	],		
	'contratos'=>
	[
		'lista'=>'admin::Administrador.contratos.index',
		'documento'=>'admin::Administrador.contratos.documento',
		'parcelas'=>'admin::Administrador.contratos.parcelas',		
	],	
	'financeiro'=>
	[
		'upload'=>'admin::Administrador.financeiro.upload',
		'documento'=>'admin::Administrador.contratos.documento',
	],							
	'dashboard'=>
	[
		'index'=>'admin::Administrador.dashboard.index',
		
	],			
];?>