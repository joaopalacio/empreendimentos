<?php
return [
	'plusKey'=>'90818192',
	'upload'=>
	[	
		'empreendimentos' => 'public/uploads/empreendimentos/',
		'imoveis'=>'public/uploads/imoveis/',
		'banners'=>'public/uploads/banners/'
	],
	'get'=>
	[
		'empreendimentos' => 'uploads/empreendimentos/',
		'imoveis'=>'uploads/imoveis/',
		'banners'=>'uploads/banners/'
	]
];?>